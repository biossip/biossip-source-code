/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip;

import java.io.File;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J_multiIndexes;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;
import uk.ac.ncl.ssip.parsers.Protein.GoParserXML;
import uk.ac.ncl.ssip.parsers.Protein.UniProtSwissProtXML;
import uk.ac.ncl.ssip.parsers.drug.DrugBankXML;
import uk.ac.ncl.ssip.parsers.protein.GOA;
import uk.ac.ncl.ssip.parsers.protein.KeggOrthologyParser;
import uk.ac.ncl.ssip.parsers.protein.STRING;
import uk.ac.ncl.ssip.queryframework.DReNInMining;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Protein_Graph {

    private String graphFile = "/Users/joemullen/Desktop/protein.gexf";
    private Neo4J backend = new Neo4J("/Users/joemullen/Documents/DReNIn-protein");
   

    public static void main(String[] args) {

        Protein_Graph j = new Protein_Graph();
        j.CreateProteinGraph();
        // j.merging();
        //j.testQuery();

    }

    public void CreateProteinGraph() {
        //>>>>>>>>>>>>>>>>>> STEP_1 FILES
        String UNIPROTxmlDB = new String("/Users/joemullen/Desktop/Datasets/UniProtSwissProt/uniprot_sprot.xml");
        //>>>>>>>>>>>>>>>>>> STEP_2 FILES
        String STRINGmappings = "/Users/joemullen/Desktop/Datasets/STRING/UNIPROTPROTEINMAPSrelease.2012_1.vs.human.string.v9.1.via_blast.v1.02172012.txt";
        String STRINGinteractionsWithTypes = "/Users/joemullen/Desktop/Datasets/STRING/9606.protein.actions.detailed.v9.1.txt";
        //>>>>>>>>>>>>>>>>>> STEP_3 FILES
        String GOntologyFile = "/Users/joemullen/Desktop/Datasets/GO/go_daily-termdb.obo-xml";
        //>>>>>>>>>>>>>>>>>> STEP_4 FILES
        String GOAFile = "/Users/joemullen/Desktop/Datasets/GOA/gene_association.goa_human";

        //>>>>>>>>>>>>>>>>>> STEP_1 START       
//        backend.setCommitSize(10000);
//        backend.initialiseDatabaseConnection();
//        /**
//         * TODO Need to index on the geneIDs
//         */
//        backend.createIndex();
//        backend.createIndex("UniProtID1");
//        backend.createIndex("UniProtID2");
//        backend.createIndex("UniProtID3");
//        backend.createIndex("UniProtID4");
//        backend.createIndex("UniProtID5");
//        backend.createIndex("STRING");
//        backend.syncIndexes(10);
//        UniProtSwissProtXML uni = new UniProtSwissProtXML(UNIPROTxmlDB, backend, true, true);
//        uni.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
//        ----------------------------------------------------------------------
//        //>>>>>>>>>>>>>>>>>> STEP_2 START
//        backend.initialiseDatabaseConnection();
//        backend.setCommitSize(10000);
//        backend.syncIndexes(10);
//        STRING interactionsadd = new STRING(600, STRINGmappings, STRINGinteractionsWithTypes, backend);
//        interactionsadd.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
        //----------------------------------------------------------------------
        //>>>>>>>>>>>>>>>>>> STEP_3 START
//        backend.initialiseDatabaseConnection();
//        backend.setCommitSize(10000);
//        backend.createIndex("GOID");
//        backend.syncIndexes(10);
//        GoParserXML test = new GoParserXML(GOntologyFile, backend);
//        test.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();

        //>>>>>>>>>>>>>>>>>> STEP_4 START
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(10000);
        backend.syncIndexes(10);
        GOA test = new GOA(GOAFile, backend, true, true, true);
        test.run();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();


    }

    public void testQuery() {

        //reinitialise connection 
        backend.initialiseDatabaseConnection();

        //resync the indexes
        backend.syncIndexes(10);


        System.out.println("export all code starts here...");
        GephiExporter gexfExportSubgraph = new GephiExporter();
        //ref node reference may be incorrect when reconnecting to database
        DReNInMining dm = new DReNInMining();
        StepDescription[] steps = dm.getAllNeighbours(1);
        gexfExportSubgraph.export(backend.traversal(new SSIPNode("Protein", "5HT3C_HUMAN"), steps), graphFile);

        //query database
        //gexfExportSubgraph.export(backend.returnAllNodesMap(), graphFile);

        //close database again 
        backend.finaliseDatabaseConnectionNoUPDATES();

    }
//    public void merging() {
//
//        //connect to database 
//        backendMerge.initialiseDatabaseConnection();
//        //create index
//        backendMerge.createIndex();
//        backendMerge.createIndex("altID1");
//        backendMerge.createIndex("altID2");
//        backendMerge.syncIndexes(10);
//
//        SSIPNode snod = new SSIPNode("Protein", "Idtest3");
//        snod.addProperty("altID1", "Id2");
//        snod.addProperty("altID2", "Id3");
//
//        SSIPNode snod2 = new SSIPNode("Protein", "Idtest2");
//        snod2.addProperty("altID1", "576");
//        snod2.addProperty("altID2", "563");
//
//
//        SSIPNode Str1 = new SSIPNode("Protein", "Id2");
//
//        SSIPNode Str2 = new SSIPNode("Protein", "563");
//
//        Str1.addRelation(Str2, new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
//        Str2.addRelation(Str1, new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
//
//        // snod.addProperty("Test1", "test1");
//        //snod.addRelation(new SSIPNode("altID1", "Id1"), new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
//        //snod.addRelation(new SSIPNode("Gene", "Id2"), new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
//        //snod.addRelation(new SSIPNode("Gene", "Id3"), new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
//        backendMerge.addNode(snod);
//        backendMerge.addNode(snod2);
//        backendMerge.addNode(Str1);
//        backendMerge.addNode(Str2);
//        backendMerge.commitTransaction();
//
////        backendMerge.addNode(new SSIPNode("altID1", "Id"));
////        SSIPNode s = new SSIPNode("altID1", "Id2");
////        s.addProperty("Test2", "test2");
////        backendMerge.addNode(s);
////        backendMerge.addNode(new SSIPNode("altID2", "Id3"));
////        populate databases and index 
////        KeggOrthologyParser keggParser = new KeggOrthologyParser(filepath, backend);
////        keggParser.parseFile();
////        backend.finaliseDatabaseConnection();
////
////        backend.initialiseDatabaseConnection();
////        backend.syncIndexes(5);
////        //query database
//        System.out.println("export all code starts here...");
//        GephiExporter gexfExportSubgraph = new GephiExporter();
//        gexfExportSubgraph.export(backendMerge.returnAllNodesMap(), graphFile);
//
//        //close database again 
//        backendMerge.finaliseDatabaseConnection();
//
//
//    }
}
