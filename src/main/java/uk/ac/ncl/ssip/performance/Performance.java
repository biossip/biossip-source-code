/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.performance;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matt2
 */
public class Performance {

    private long startTime;
    private long connectionTime;
    private long transactionTime;
    private long endTime;
    private PrintWriter writer = null;

    public Performance() {

        //log commit size, dependency versions, operating system, computer specification etc
        
    }

    public Performance(String performanceReportPath) {
        try {
            writer = new PrintWriter(performanceReportPath, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
            writer = null;
        }
    }

    public void generatePerformanceReport() {
    }

    public void setStartTime() {
        this.startTime = System.currentTimeMillis();
    }

    public void setConnectionTime() {
        this.connectionTime = System.currentTimeMillis() - startTime;
        this.transactionTime = connectionTime;
        output(connectionTime);
    }

    public void setTransactionTime() {
        this.transactionTime = System.currentTimeMillis() - transactionTime;
        output(transactionTime);

    }

    public void setEndTime() {
        this.endTime = System.currentTimeMillis()-startTime;
        output(endTime);
        if (writer != null) {
            writer.close();
        }
    }

    public void output(long time) {
        if (writer == null) {
            System.out.println(time);
        } else {
            writer.println(time);
        }
    }
}
