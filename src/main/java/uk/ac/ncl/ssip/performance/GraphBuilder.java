/*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.ac.ncl.ssip.performance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class GraphBuilder implements PerformanceTasks {

    private int startNodeNumber;
    private int endNodeNumber;
    private int ordersOfMagnitude;
    private List<Double> buildtimeset = new ArrayList<Double>();
    private List<Double> querytimeset = new ArrayList<Double>();
    private List<Integer> buildset = new ArrayList<Integer>();
    private boolean fullyconnected;
    private int replicates;

    public GraphBuilder(int replicates, int ordersOfMagnitude, boolean fullyconnected) {
        this.replicates = replicates;
        this.ordersOfMagnitude = ordersOfMagnitude;
        this.fullyconnected = fullyconnected;
    }

    public GraphBuilder() {
    }

    public List<Double> buildGraph() {

        //create dummy graph to preinitialise everything that will be used 
        //might want to consider doing this multiple times so java bytes are compiled to machine code 
        buildGraph(1, -2, true);

        for (int l = 0; l < replicates; l++) {
            //create graph sizes for each order of magnitude
            for (int i = 0; i < ordersOfMagnitude; i++) {
                for (int j = 1; j < 10; j++) {
                    buildset.add(((int) Math.pow(10, i)) * j);
                }
            }

            //randomise build order
            Collections.shuffle(buildset);

            for (int i = 0; i < buildset.size(); i++) {
                PrintToFile.addElement(String.valueOf(buildset.get(i)));

                buildtimeset.add(buildGraph(l, buildset.get(i), fullyconnected));
            }
            buildset.clear();
        }

        return buildtimeset;
    }

    public List<Double> queryGraph() {
        //insert code to query graph here

//        //shortest path query 
//        GraphHandlerInterface neogh = new Neo4J(1, 11, false);
//        neogh.createDB();
//
//        neogh.shortestPathQuery("no" + startNodeNumber, "no" + endNodeNumber);
//        //set opperation query 

        return querytimeset;
    }

    public void performanceCharts() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private double buildGraph(int rep, int graphsize, boolean fullyconnected) {

        //do garbage collection before each build so this is less likely to affect the build time
        System.gc();

        long starttime = System.currentTimeMillis();

        createDB(rep, graphsize, fullyconnected);

        long endtime = System.currentTimeMillis();

        System.out.println(endtime - starttime);
        PrintToFile.addElement(String.valueOf(endtime - starttime) + "\n");
        return (endtime - starttime);
    }

    public void createDB(int rep, int graphsize, boolean fullyconnected) {
        GraphHandlerInterface gh;
        graphsize = Math.abs(graphsize);
        String DB_PATH = "";
        if (fullyconnected) {

            gh = new Neo4J(DB_PATH = DB_PATH + "_" + rep + "_" + graphsize + "_fullyconnected");
            gh.initialiseDatabaseConnection();

        } else {

            gh = new Neo4J(DB_PATH = DB_PATH + "_" + rep + "_" + graphsize);
            gh.initialiseDatabaseConnection();

        }

        SSIPNode node = new SSIPNode("type", "no0");
        gh.addNode(node);

        for (int i = 1; i < graphsize; i++) {
            //create node
            node = new SSIPNode("type", "no" + i);

            if (fullyconnected) {
                //connect node to all other nodes
                for (int j = 0; j < i; j++) {
                    node.addRelation(new SSIPNode("type", "no" + j), new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
                }
            } else {
                int num =i-1;
                node.addRelation(new SSIPNode("type", "no" + num), new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
            }
            gh.addNode(node);

        }

    }

    public void setStartNodeNumber(int startNodeNumber) {
        this.startNodeNumber = startNodeNumber;
    }

    public void setEndNodeNumber(int endNodeNumber) {
        this.endNodeNumber = endNodeNumber;
    }
}
