/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.ncl.ssip.performance;

/**
 *
 * @author Matt2
 */
public class PerformanceTest {
    
    public static void main(String[] args){
        
        int replicates = 3;//Integer.parseInt(args[0]);
        int orders =5;//Integer.parseInt(args[1]);
        boolean connected = false;//Boolean.parseBoolean(args[2]);
        
        //initialise outputfile stream
        PrintToFile.initialiseOutputFile("buildtimes_" + replicates + "_" + 
                orders + "_" + connected + ".csv");
        
        //neo4J build performance tests with single relationship
        PrintToFile.addLine();
        PrintToFile.addElement("NEO4J");
        PrintToFile.addLine();
        GraphBuilder neoPerform = new GraphBuilder(replicates, orders, connected);
        neoPerform.buildGraph();


        //finalise outputfile stream
        PrintToFile.finaliseOutputFile();
    }
}
