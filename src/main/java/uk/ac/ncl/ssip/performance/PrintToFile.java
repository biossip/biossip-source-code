/*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.ac.ncl.ssip.performance;

import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class PrintToFile {

    static FileWriter fos;
    static PrintWriter dos;

    public static void initialiseOutputFile(String filePath) {
        try {
            fos = new FileWriter(filePath);
            dos = new PrintWriter(fos);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void addElement(String element) {
        dos.print(element + "\t" );
    }
    
    public static void addLine(){
        dos.println();
    }

    public static void finaliseOutputFile() {
        try {
            dos.close();
            fos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
