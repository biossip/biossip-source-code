///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package uk.ac.ncl.ssip.metadata.nodetypes;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Set;
//import org.neo4j.graphdb.RelationshipType;
//import uk.ac.ncl.ssip.metadata.MetaDataInterface;
//
///**
// *
// * @author Joseph Mullen
// */
//public class Protein implements MetaDataInterface {
//
//    private String sequence;
//    private Set geneIDs;
//    private Set refSeqProtIDs;
//    private String accession;
//    private Map<MetaDataInterface, RelationshipType> relations;
//
//    @Override
//    public RelationshipType getRelation(MetaDataInterface nodeObject) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public Set<String> getAllPropertyNames() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public Map<String, Object> getAttributes() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    public enum RelTypes implements RelationshipType {
//        hasGOTerm, hasKOTerm
//    }
//
//    public Protein(Set refSeqProtIds, Set geneIds, String acc, String seq) {
//        this.accession = acc;
//        this.geneIDs = geneIds;
//        this.refSeqProtIDs = refSeqProtIds;
//        this.sequence = seq;
//        this.relations = new HashMap<MetaDataInterface, RelationshipType>();
//
//    }
//
//    public String getType() {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//
//    public String getId() {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//
//    public void setId(String id) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//
//    public Map<MetaDataInterface, RelationshipType> getRelations() {
//        return relations;
//    }
//
//    public void addRelation(MetaDataInterface nodeObj, RelationshipType tr) {
//        relations.put(nodeObj, tr);
//    }
//}
