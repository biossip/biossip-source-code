/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.metadata;

import org.neo4j.graphdb.RelationshipType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class DReNInMetadata {

    public static void main(String[] args) {

        DReNInMetadata dm = new DReNInMetadata();



    }

    public enum RelTypes implements RelationshipType {

        KNOWS("KNOWS"), 
        BINDS_TO("BINDS_TO"), 
        INTERACTS_WITH_PROTEIN("INTERACTS_WITH_PROTEIN"),
        IS_ENCODED_BY("IS_ENCODED_BY"),
        HAS_GO_TERM("HAS_GO_TERM"), 
        PART_OF_PATHWAY("PART_OF_PATHWAY"), 
        INVOLVED_IN_DISEASE("INVOLVED_IN_DISEASE"), 
        IS_A("IS_A"), 
        HAS_SIM_STRUCTURE("HAS_SIM_STRUCTURE"),
        EXPRESSED_IN("EXPRESSED_IN"), 
        HAS_LIGAND_BINDING_SITE("HAS_LIGAND_BINDING_SITE"), 
        PART_OF_COMPLEX("PART_OF_COMPLEX"), 
        SIM_SEQUENCE("SIM_SEQUENCE"), 
        MEMBER_OF("MEMBER_OF"), 
        TRANSCRIBES("TRANSCRIBES"), 
        HAS_CHILD("HAS_CHILD"), 
        HAS_PARENT("HAS_PARENT"), 
        INTERACTS_WITH_DRUG("INTERACTS_WITH_DRUG"), 
        HAS_SIMILAR_CHEMICAL("HAS_SIMILAR_CHEMICAL"), 
        MAY_TREAT("MAY_TREAT"), 
        SIDE_EFFECT("SIDE_EFFECT"),
        LOCATED_IN("LOCATED_IN"),
        HAS_CONSERVED_PART("HAS_CONSERVED_PART"), 
        LOCATED_IN_CELLULAR_COMPONENT("LOCATED_IN_CELLULAR_COMPONENT"), 
        HAS_MOLECULAR_FUNCTION("HAS_MOLECULAR_FUNCTION"), 
        PART_OF_BIOLOGICAL_PROCESS("PART_OF_BIOLOGICAL_PROCESS"),
        IS_A_RARE_DISEASE("IS_A_RARE_DISEASE"), 
        PART_OF_RARE_DISEASE("PART_OF_RARE_DISEASE");
        
        private String text;

        RelTypes(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public static RelTypes fromString(String text) {
            if (text != null) {
                for (RelTypes b : RelTypes.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static RelTypes fromValue(String value) throws IllegalArgumentException {
            try {
                return RelTypes.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    public enum NodeType {

        DRUG("Drug"),
        MoA("MoA"),
        SMALL_MOLECULE("Small_Molecule"),
        DRUG_COMBINATION("Drug_Combination"),
        DISEASE("Disease"),
        RARE_DISEASE("Rare_Disease"),
        LIGAND("Ligand"),
        CELLULAR_COMPONENT("Cellular_Component"),
        MOLECULAR_FUNCTION("Moleular_Function"),
        BIOLOGICAL_PROCESS("Biological_Process"),
        PATHWAY("Pathway"),
        TRAIT("Trait"),
        GENE("Gene"),
        PROTEIN("Protein"),
        TARGET("Target"),
        PROTEIN_FAMILY("Protein_Family"),
        PROTEIN_COMPLEX("Protein_Complex"),
        PROTEIN_DOMAIN("Protein_Domain"),
        LIGAND_BINDING_SITE("Ligand_Binding_Site"),
        TISSUE("Tissue"),
        CELL_LINE("Cell_Line"),
        DNA("DNA");
        private String text;

        NodeType(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public static NodeType fromString(String text) {
            if (text != null) {
                for (NodeType b : NodeType.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static NodeType fromValue(String value) throws IllegalArgumentException {
            try {
                return NodeType.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }
}
