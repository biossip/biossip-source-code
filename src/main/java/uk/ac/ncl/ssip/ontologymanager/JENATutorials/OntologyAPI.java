/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.ontologymanager.JENATutorials;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

/**
 *
 * @author joemullen
 */
public class OntologyAPI {

    public static void main(String[] args) {

        OntologyAPI oapi = new OntologyAPI();
        //not working- need to sort this- line 34
        //   oapi.createOntology();

    }

    public void createOntology() throws IOException {

        // create the base model

        InputStream is = null;
        try {
            is = new FileInputStream("/Users/joemullen/Desktop/Ontologies/PLIO.owl");

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String SOURCE = "http://www.eswc2006.org/technologies/ontology";
        String NS = SOURCE + "#";
        OntModel base = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
        base.read(is, "RDF/XML");



// create the reasoning model using the base
        OntModel inf = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF, base);
        
        

//// create a dummy paper for this example
//        OntClass paper = base.getOntClass(NS + "Paper");
//        Individual p1 = base.createIndividual(NS + "paper1", paper);
//
//// list the asserted types
//        for (Iterator<Resource> i = p1.listRDFTypes(true); i.hasNext();) {
//            System.out.println(p1.getURI() + " is asserted in class " + i.next());
//        }
//
//// list the inferred types
//        p1 = inf.getIndividual(NS + "paper1");
//        for (Iterator<Resource> i = p1.listRDFTypes(true); i.hasNext();) {
//            System.out.println(p1.getURI() + " is inferred to be in class " + i.next());
//        }


    }
}
