/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.ontologymanager;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/ Class allows for
 * an Ontology to be queried. Methods are required for query expansion and
 * scoring ontological distances.
 */
public class QueryExpansion {

    private OntModel base;
    private String nameSpace;
    private String inputFileName;
    final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

    public static void main(String args[]) {

        String term = "Drug";

        String ontFile = "/Users/joemullen/Dropbox/Ontology/DReNIn_Ontology.owl";
        String nameSpace = "http://www.semanticweb.org/joemullen/ontologies/2014/6/untitled-ontology-12#";
        QueryExpansion qs = new QueryExpansion(ontFile, nameSpace);
        qs.createOntModel();
        System.out.println("TERM: " + nameSpace + term);
        System.out.println(qs.getAllClasses());
        System.out.println( qs.getAllSubClasses(term));
        System.out.println( qs.getAllSuperClasses(term));


    }

    /**
     * Constructor
     *
     * @param ontFile
     * @param namespace
     */
    public QueryExpansion(String ontFile, String namespace) {
        //create the reasoning model using the base
        this.base = ModelFactory.createOntologyModel();
        this.nameSpace = namespace;
        this.inputFileName = ontFile;

    }

    /**
     * Create the OntModel that will be queried.
     *
     * @param term
     */
    public void createOntModel() {
        try {
            // use the FileManager to find the input file
            InputStream in = FileManager.get().open(inputFileName);
            if (in == null) {
                throw new IllegalArgumentException("File: " + inputFileName + " not found");
            }
            base.read(in, "");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Returns all terms captured by the ontology.
     *
     * @return
     */
    public Set<String> getAllClasses() {
        String name = new Object() {
        }.getClass().getEnclosingMethod().getName();
        System.out.println(name);
        Set classSet = new HashSet<String>();
        ExtendedIterator classes = base.listClasses();
        while (classes.hasNext()) {
            OntClass thisClass = (OntClass) classes.next();
            classSet.add(thisClass.getLocalName());
            //list instances if desired
            ExtendedIterator instances = thisClass.listInstances();
            while (instances.hasNext()) {
                Individual thisInstance = (Individual) instances.next();
            }
        }

        return classSet;
    }

    /**
     * Get all sub classes for a given term.
     *
     * @param term
     * @return
     */
    public Set<String> getAllSubClasses(String term) {
        String name = new Object() {
        }.getClass().getEnclosingMethod().getName();
        System.out.println(name);
        String URI = nameSpace + term;
        Set terms = new HashSet<String>();
        OntClass event = base.getOntClass(URI);
        //get subclasses
        for (Iterator<OntClass> i = event.listSubClasses(); i.hasNext();) {
            OntClass c = (OntClass) i.next();
            terms.add(c.getLocalName());
        }
        return terms;
    }

    /**
     * Get all parent classes for a given term.
     *
     * @param term
     * @return
     */
    public Set<String> getAllSuperClasses(String term) {
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        System.out.println(name);
        String URI = nameSpace + term;
        Set terms = new HashSet<String>();
        OntClass event = base.getOntClass(URI);
        //get superclasses
        for (Iterator<OntClass> i = event.listSuperClasses(); i.hasNext();) {
            OntClass c = (OntClass) i.next();
            terms.add(c.getLocalName());
        }
        return terms;
    }
}
