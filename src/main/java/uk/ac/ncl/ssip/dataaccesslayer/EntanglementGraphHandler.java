/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.ncl.ssip.dataaccesslayer;

import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import java.util.List;
import java.util.Map;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;

/**
 *
 * @author Matt2
 */
public class EntanglementGraphHandler implements GraphHandlerInterface {

    private String DB_PATH = "";
    
    public EntanglementGraphHandler(String DB_PATH){
        this.DB_PATH=DB_PATH;
    }
    

    @Override
    public void addNode(MetaDataInterface node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void commitTransaction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public void clearDatabase(String dbname) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Map<String, List<String>> itterateAllTypeIdPairs() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Map<String, Map<String, MetaDataInterface>> traversal(String startType, String startId, int depth) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDatabaseName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Map<String, Map<String, MetaDataInterface>> iterateAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<MetaDataInterface> returnAllNodes() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, MetaDataInterface> returnAllNodesMap() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, MetaDataInterface> traversal(MetaDataInterface startNode, StepDescription... steps) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, MetaDataInterface> traversal(StepDescription... steps) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void initialiseDatabaseConnection() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void finaliseDatabaseConnection() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
