/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.dataaccesslayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.RelationshipType;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;

/**
 *
 * @author Joe Mullen
 */
public class SSSStepDescription {

    private int depth;
    private boolean depthFirst;
    //to do add ability to specify node type as well as relation type
    private List<String> relations;
    private Direction relDir;
    /**
     * These variables may be used to extend the traversal methods. If we provide
     * a list of node types we want to include and properties for which they must
     * contain then we can score during the traversal 
     */
    private List<String> nodes;
    private Map<String, Object> properties;
    
    
    public SSSStepDescription() {
        this.relations = new ArrayList<String>();
        
    }
    
  
    public SSSStepDescription(int depth, boolean depthFirst) {
        this.relations = new ArrayList<String>();
        this.depth = depth;
        this.depthFirst = depthFirst;
        this.relDir = Direction.BOTH;
    }

    public void addRelation(String rel){
        relations.add(rel);
    }
    
    public List<String> getRelations(){
        return relations;
    }
    
    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public boolean isDepthFirst() {
        return depthFirst;
    }

    public void setDepthFirst(boolean depthFirst) {
        this.depthFirst = depthFirst;
    }
    
    public void setDirection(Direction d) {
        this.relDir = d;
    }
    
    public Direction getDirection() {
        return relDir;
    }


}
