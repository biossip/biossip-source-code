/*
 * Copyright(c) 2014 Matthew Collison. 
 */

package uk.ac.ncl.ssip.dataaccesslayer;

import java.util.List;
import java.util.Map;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;

/**
 *
 * @author Matthew Collison 
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class Triplestore implements GraphHandlerInterface{
    
    public void intialiseTransaction() {
        throw new UnsupportedOperationException("Not supported yet.");        

    }

    @Override
    public void addNode(MetaDataInterface node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void commitTransaction() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void finaliseTransaction() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clearDatabase(String dbname) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getDatabaseName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, MetaDataInterface> returnAllNodesMap() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, MetaDataInterface> traversal(MetaDataInterface startNode, StepDescription... steps) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, MetaDataInterface> traversal(StepDescription... steps) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void initialiseDatabaseConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void finaliseDatabaseConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

}
