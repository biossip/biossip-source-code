/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.dataaccesslayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.graphdb.schema.Schema;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription.RelTypes;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author Matt and Joe
 */
public class Neo4J_multiIndexes implements GraphHandlerInterface {

    private String DB_PATH;
    private GraphDatabaseService graphDb;
    private int counter = -1;
    private int commitSize = 10;
    public static String refIndex = "reference_index";
    public static String refId = "reference_id";
    public static String typePrefix = "type: ";
    private Transaction tx;
    private Node refnode;

    public Neo4J_multiIndexes(String DB_PATH) {
        this.DB_PATH = DB_PATH;
    }

    @Override
    public void initialiseDatabaseConnection() {
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
        registerShutdownHook(graphDb);

        //synchronise indexes 
        try (Transaction tx = graphDb.beginTx()) {
            Schema schema = graphDb.schema();
            schema.awaitIndexesOnline(20, TimeUnit.SECONDS);
            System.out.println("Live indexes in database...");
            for (IndexDefinition index : graphDb.schema().getIndexes()) {
                System.out.print(index.getLabel());
            }
            tx.success();
            tx.close();
            System.out.println("Done");
        }

        tx = graphDb.beginTx();
        System.out.println("Sucesfully connected to graph database.");
        System.out.println("Graph location: " + DB_PATH);
    }

 
    @Override
    public void addNode(MetaDataInterface nodeObject) {

        //create/return equivalent neo4j node
        Node neoNode = checkNode(nodeObject.getType(), nodeObject.getId());

        //add relations to neo4j node
        for (MetaDataInterface toNodeObject : nodeObject.getRelations().keySet()) {
            //pre-fix getType() with "Type: " to identify this property as a type property

            //create/return neo4j node that will be connected 
            Node toNeoNode = checkNode(toNodeObject.getType(), toNodeObject.getId());

            //return relationship type connecting nodes
            RelationshipType relType = nodeObject.getRelations().get(toNodeObject);

            //create relationship in neo 
            neoNode.createRelationshipTo(toNeoNode, relType);
        }

        //when creating the first node connect this to a reference node
        if (counter == -1) {
            refnode = checkNode(refIndex, refId);
            neoNode.createRelationshipTo(refnode, SSIPRelationType.RelTypes.KNOWS);
            counter = 0;

            //if node reaches commit size threshold commit transaction to database server
        } else if (counter == commitSize) {
            commitTransaction();
        }
        counter++;

    }

    //create if it does not exists 
    public Node checkNode(String property, String value) {
        Node node = null;

        //allows identification of property as type id identification for interpretation 
        property = typePrefix + property;

        //change this for a different node
        for (Node n : graphDb.findNodesByLabelAndProperty(
                DynamicLabel.label(property), property, value)) {
            node = n;
        }
        if (node == null) {
            node = graphDb.createNode(DynamicLabel.label(property));
            node.setProperty(property, value);
            return node;
        } else {
            return node;
        }
    }

    @Override
    public void commitTransaction() {
        tx.success();
        tx = graphDb.beginTx();
    }

    @Override
    public void finaliseDatabaseConnection() {
        tx.success();
        tx.close();
        try (Transaction tx = graphDb.beginTx()) {
            Schema schema = graphDb.schema();
            schema.awaitIndexesOnline(20, TimeUnit.SECONDS);
            System.out.println("Live indexes in database...");
            for (IndexDefinition index : graphDb.schema().getIndexes()) {
                System.out.print(index.getLabel());
            }
            tx.success();
            tx.close();
            System.out.println("Done");
        }
        System.out.println("Transaction closed");
        graphDb.shutdown();
        System.out.println("Database connection shutdown");
    }

    /*
     * only the type and id properties of the start node have to be identical to seed the traversal
     * AIM: return all sub networks from seed node that follow the rules defined in the step descriptions 
     */
    @Override
    public Map<String, MetaDataInterface> traversal(MetaDataInterface seedNode, StepDescription... steps) {

        //stores neo4j node before conversion 
        List<Node> returnNeoNodes = new ArrayList<Node>();

        //starting nodes for traversal. The seed is from the method arguments initially but updated on subsequent steps
        List<Node> seedNodes = new ArrayList<Node>();
        seedNodes.add(findNode(seedNode.getType(), seedNode.getId()));

        //System.out.println(findNode(seedNode.getType(), seedNode.getId()));
        //System.out.println(seedNodes);

        //itterate through steps of the algorithm 
        for (int i = 0; i < steps.length; i++) {

            List<StepDescription.RelTypes> rels = steps[i].getRelations();
            System.out.println(rels.toString());

            //define the traversal rules for each step
            TraversalDescription td = graphDb.traversalDescription()
                    .breadthFirst()
                    .evaluator(
                    Evaluators.toDepth(steps[i].getDepth()));

            //add relationship rules to traversal if we have any
            if (!rels.isEmpty()) {
                for (StepDescription.RelTypes rel : rels) {
                    td = td.relationships(rel);
                }
            }

            /**
             * TODO this is where you could introduce traversal rules; scores
             * based on node types and attributes etc.
             */
            List<Node> newSeedNodes = new ArrayList<Node>();

            for (Node seed : seedNodes) {
                //meet Trevor the travelling traverser 
                Traverser trevor = td.traverse(seed);

                //convert step result into SSIP subgraph to seed next step and to return
                for (Node n : trevor.nodes()) {
                    returnNeoNodes.add(n);
                }
                //can be optimised as traversal includes paths to intermediate nodes in longer paths 
                for (Path p : trevor) {
                    newSeedNodes.add(p.endNode());
                }
            }
            seedNodes.clear();//not sure if this line can be deleted
            seedNodes.addAll(newSeedNodes);
            newSeedNodes.clear();

        }

        return convertNeoNodesToSSIPNodes(returnNeoNodes);
    }

    public Node findNode(String property, String value) {
        Node node = null;
        property = typePrefix + property;
        System.out.println("FINdNode: "+property);
        for (Node n : graphDb.findNodesByLabelAndProperty(
                DynamicLabel.label(property), property, value)) {
             System.out.println("FINdNode: n "+n);
            node = n;
        }
        return node;
    }

    public Map<String, MetaDataInterface> convertNeoNodesToSSIPNodes(List<Node> neoNodes) {
        Map<String, MetaDataInterface> returnNodes = new HashMap<String, MetaDataInterface>();

        for (Node neonode : neoNodes) {

            System.out.println("Neo4J node: " + neonode);
            System.out.println("type: " + neonode.getPropertyKeys());
            for (String property : neonode.getPropertyKeys()) {
                System.out.println("id: " + neonode.getProperty(property));
            }
            //convert step result into SSIP subgraph to seed next step and to return
            SSIPNode nodeObject = new SSIPNode(getTypeProperty(neonode),
                    (String) neonode.getProperty(typePrefix + getTypeProperty(neonode)));

            for (Relationship rel : neonode.getRelationships(Direction.OUTGOING)) {
                nodeObject.addRelation(
                        new SSIPNode(getTypeProperty(rel.getEndNode()), (String) rel.getEndNode()
                        .getProperty(typePrefix + getTypeProperty(rel.getEndNode()))), 
                        new SSIPRelationType(rel.getType().toString()));
            }
            returnNodes.put(nodeObject.getType() + nodeObject.getId(), nodeObject);
        }
        return returnNodes;
    }

    public String getTypeProperty(Node neonode) {
        for (String property : neonode.getPropertyKeys()) {
            if (property.startsWith(typePrefix)) {
                System.out.println(property);
                System.out.println(property.replaceFirst(typePrefix, ""));
                return property.replaceFirst(typePrefix, "");
            }
        }
        return null;
    }

    void shutDown() {
        System.out.println();
        System.out.println("Shutting down database ...");
        // START SNIPPET: shutdownServer
        graphDb.shutdown();
        // END SNIPPET: shutdownServer
    }

    // START SNIPPET: shutdownHook
    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    @Override
    public Map<String, MetaDataInterface> returnAllNodesMap() {

        //stores flattened graph at the end
        List<Node> returnNodes = new ArrayList<Node>();

        //neo4j traversal object
        TraversalDescription td = graphDb.traversalDescription()
                .breadthFirst()
                .evaluator(
                Evaluators.all());

        //meet Trevor the travelling traverser 
        Traverser trevor = td.traverse(getRefnode());

        for (Node n : trevor.nodes()) {
            returnNodes.add(n);
        }

        return convertNeoNodesToSSIPNodes(returnNodes);

    }

    /*
     WARNING: THIS METHOD ONLY RETURNS NODES WITH AT LEAST ONE TRANSITIVE RELATION TO THE START NODE
     */
    public Map<String, MetaDataInterface> returnAllNodesMap(MetaDataInterface startNode) {

        //stores flattened graph at the end
        List<Node> returnNodes = new ArrayList<Node>();

        //neo4j traversal object
        TraversalDescription td = graphDb.traversalDescription()
                .breadthFirst()
                .evaluator(
                Evaluators.all());

        //change label to startNode.getType() once indexing is corrected
        //meet Trevor the travelling traverser 
        Traverser trevor = td.traverse(findNode(startNode.getType(), startNode.getId()));

        for (Node n : trevor.nodes()) {
            returnNodes.add(n);
        }

        return convertNeoNodesToSSIPNodes(returnNodes);

    }

    /*
     This method starts the traversal at the reference node 
     */
    public Map<String, MetaDataInterface> traversal(StepDescription... steps) {

        //stores flattened graph at the end
        List<Node> returnNeoNodes = new ArrayList<Node>();

        List<Node> seedNodes = new ArrayList<Node>();
        seedNodes.add(getRefnode());

        for (int i = 0; i < steps.length; i++) {

            List<StepDescription.RelTypes> rels = steps[i].getRelations();


            //define the traversal rules for each step
            TraversalDescription td = graphDb.traversalDescription()
                    .breadthFirst()
                    .evaluator(
                    Evaluators.toDepth(steps[i].getDepth()));

            //add relationship rules of traversal if we have any
            if (!rels.isEmpty()) {
                for (StepDescription.RelTypes rel : rels) {
                    td = td.relationships(rel);
                }
            }

            List<Node> newSeedNodes = new ArrayList<Node>();
            for (Node seed : seedNodes) {
                //meet Trevor the travelling traverser 
                Traverser trevor = td.traverse(seed);

                System.out.println("Neo4J seed node: " + seed);

                //convert step result into SSIP subgraph to seed next step and to return
                for (Node n : trevor.nodes()) {
                    returnNeoNodes.add(n);
                }
                for (Path p : trevor) {
                    newSeedNodes.add(p.endNode());
                    System.out.println("end node property keys: " + p.endNode().getPropertyKeys());
                }
            }
            seedNodes.clear();//not sure if this line can be deleted
            seedNodes.addAll(newSeedNodes);
            newSeedNodes.clear();

        }

        return convertNeoNodesToSSIPNodes(returnNeoNodes);

    }

    @Override
    public String getDatabaseName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clearDatabase(String dbname) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Node getRefnode() {
        refnode = findNode(refIndex, refId);
        return refnode;
    }

    @Deprecated
    public void addEdge(MetaDataInterface nodeFrom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void createIndex(String label, String property) {
        tx.success();
        tx.close();
        IndexDefinition indexDefinition;
        try (Transaction schemaTx = graphDb.beginTx()) {
            System.out.println("attempting to create index");

            Schema schema = graphDb.schema();
            indexDefinition = schema.indexFor(DynamicLabel.label(label))
                    .on(property)
                    .create();
            schemaTx.success();
            indexDefinition.getPropertyKeys();
            System.out.println("index created");
        }
        try (Transaction tx = graphDb.beginTx()) {
            Schema schema = graphDb.schema();
            schema.awaitIndexOnline(indexDefinition, 10, TimeUnit.SECONDS);
            System.out.println("index loaded");
        }
        tx = graphDb.beginTx();
    }

    /**
     * Helper method to get indexes by Label, wrapped in a tx
     *
     * @param label
     * @return one or more IndexDefinitions to iterate over, or null if there
     * were none matching the label.
     */
    public Iterable<IndexDefinition> getIndexByLabel(Label label) {


        //check if index for type already exists; if not create it 

        //######################################################
        //THIS IF STATEMENT IS FLAWED!!!!!!!!!!!!!!!!
        //######################################################
//        if (getIndexByLabel(DynamicLabel.label(property)) == null) {
//            createIndex(property, property);
//        }

        Iterable<IndexDefinition> x = null;
        try (Transaction tx = graphDb.beginTx()) {
            x = graphDb.schema().getIndexes(label);
        }
        //count indexes; return null if 0
        int i = 0;
        for (IndexDefinition index : x) {
            i++;
        }
        if (i == 0) {
            return null;
        }
        return x;
    }
}
