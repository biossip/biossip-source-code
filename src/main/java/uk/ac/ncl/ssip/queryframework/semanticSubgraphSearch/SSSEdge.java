/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework.semanticSubgraphSearch;

import java.util.logging.Logger;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.ssip.metadata.SSIPNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 * 
 * Extends Default Edge in the JGraphT library. This edge is used in SSSDataStructure
 * to allow for SSIPNodes to be easily extracted from edges.
 */
public class SSSEdge<V> extends DefaultEdge {

    private V v1;
    private V v2;
    private String label;
    private static final long serialVersionUID = 1L;
    private SSIPNode from;
    private SSIPNode to;
    private static final Logger LOG = Logger.getLogger(SSSEdge.class.getName());

    public SSSEdge(V v1, V v2) {
        this.v1 = v1;
        this.v2 = v2;
        this.from= (SSIPNode) v1;
        this.to = (SSIPNode) v2;

    }

    /**
     * Get edge from SSIPNode
     * @return 
     */
    public SSIPNode getFrom() {
        return from;
    }

    /**
     * Get edge to SSIPNode
     * @return 
     */
    public SSIPNode getTo() {
        return to;
    }

    /**
     * Set edge from SSIPNode
     * @param from 
     */
    public void setFrom(SSIPNode from) {
        this.from = from;
    }

    /**
     * Set edge to SSIPNode
     * @param to 
     */
    public void setTo(SSIPNode to) {
        this.to = to;
    }
}
