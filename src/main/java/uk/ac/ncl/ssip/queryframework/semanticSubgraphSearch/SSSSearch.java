/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework.semanticSubgraphSearch;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.dataaccesslayer.SSSStepDescription;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class SSSSearch {

    public SSSSub query;
    public SSSQuery[] querySteps;
    public SSIPNode[] queryOrder;
    public SSIPNode[] MappingOrder;
    public SSSDataStructure dat;
    //some of the methods i have implemented are not in the graph handler interface
    public Neo4J handler = new Neo4J("/Users/joemullen/Documents/neotest-uniprot-1");
    public String directory = "/Users/joemullen/Desktop/DiseaseOP/mappings";
    private int STATE = 0;

    public static void main(String[] args) {
        SSSSearch s = new SSSSearch();
        s.run();
    }

    /**
     * 3. check each addition to the data structure- implement the feasability
     * checks
     */
    /**
     * Perform an iterative, exhaustive search for a subgraph.
     */
    public void run() {

        handler.initialiseDatabaseConnection();

        //resync the indexes
        handler.syncIndexes(10);

        this.query = new SSSSub();
        query.createSimple();
        this.queryOrder = query.getQueryOrder();
        this.querySteps = query.getQuerySteps();

        //get the initial nodes that will be the seeds for the search
        Set<SSIPNode> initialCandidateSet = initialNodes();

        for (SSIPNode init : initialCandidateSet) {
            //create a new datastructure
            dat = new SSSDataStructure();
            //add the seed node to the data structure
            dat.addNode(init);
            STATE++;

            //start at one as we have already assigned our first node from the initial candidate set
            Set<SSIPNode> nextNodes = new HashSet<>();
            if (STATE == 1) {
                nextNodes.add(init);
            }

            for (int i = 0; i < querySteps.length; i++) {
                Set<SSIPNode> nextNodesFromThisState = new HashSet<>();
                for (SSIPNode thisNode : nextNodes) {
                    //get next nodes
                    SSSStepDescription[] steps = new SSSStepDescription[1];
                    steps[0] = new SSSStepDescription(1, true);
                    Set<String> relations = querySteps[i].getReltypes();
                    for (String r : relations) {
                        steps[0].addRelation(r);
                    }               

                    //get next nodes
                    Map<String, MetaDataInterface> nextNodess = handler.getNextNodesMappings(thisNode, steps);
                    Set<SSIPNode> nodes = new HashSet<SSIPNode>();
                    for (String name : nextNodess.keySet()) {
                        //check here for compatability
                        nodes.add((SSIPNode) nextNodess.get(name));
                        nextNodesFromThisState.add((SSIPNode) nextNodess.get(name));
                    }

                    dat.addNextNodes(nodes, thisNode);
                }

                //check the topology
                for (SSIPNode checker : nextNodesFromThisState) {
                    MappingOrder = dat.getShortestPathNodes(dat.getParentNode(), checker);
                    if (closedWorldCheck() == false) {
                        //remove nodes from the data structure
                        dat.removeNodesFromDat(MappingOrder);
                        //remove node fromt the nextNodes
                        nextNodesFromThisState.remove(checker);
                    }
                }
                //search for the next nodes
                nextNodes = nextNodesFromThisState;
                STATE++;
            }

            //export the subgraphs
            Set<SSIPNode> leaves = dat.getLeafNodes();
            int count = 0;
            int test = 5;
            for (SSIPNode s : leaves) {
                //mappings!!!!!               
                SSIPNode[] nodes = dat.getShortestPathNodes(dat.getParentNode(), s);
                count++;
                if (count == test) {
                    break;
                }
            }

            System.out.println("Finished searching with initial node: " + init.getId());
        }

        handler.finaliseDatabaseConnectionNoUPDATES();
//        SSSDSVisualisation s = new SSSDSVisualisation(dat, "whatever", false);
//        s.visualise();
    }

    /**
     * Allows a mapping to be exported to a .gexf file.
     *
     * @param nodes
     * @param name
     */
    public void exportSubToGephi(SSIPNode[] nodes, String name) {
        GephiExporter geph = new GephiExporter();
        Map<String, MetaDataInterface> nodeMap = new HashMap<String, MetaDataInterface>();
        for (int g = 0; g < nodes.length; g++) {
            nodeMap.put(nodes[g].getType() + nodes[g].getId(), nodes[g]);
        }
        geph.export(nodeMap, directory + "/" + name + ".gexf");
    }

    /**
     * Get the state at which the search currently is.
     *
     * @return
     */
    public int getState() {
        return STATE;
    }

    /**
     * Set the state of the search.
     *
     * @param state
     */
    public void setState(int state) {
        this.STATE = state;
    }

    /**
     * Get the next relation types that are to be used to traversed to get the
     * next nodes.
     *
     * @return
     */
    public SSIPRelationType getNextRel() {
        SSIPNode now = queryOrder[STATE];
        Map<MetaDataInterface, SSIPRelationType> rel = now.getRelations();
        SSIPRelationType reltype = null;
        for (MetaDataInterface r : rel.keySet()) {
            reltype = rel.get(r);
        }
        return reltype;
    }

    /**
     * Returns a set of initial nodes for the search. This initial candidate set
     * should be provided by the user.
     *
     * @return
     */
    public Set<SSIPNode> initialNodes() {
        Set<SSIPNode> candidateSet = new HashSet<SSIPNode>();
        SSIPNode sil = new SSIPNode("Protein", "HUMAN_5HT3B");
        candidateSet.add(sil);
        return candidateSet;
    }

    /**
     * Check that the potential mapping has the same number of relations between
     * other nodes in the mapping as its corresponding node in the query.
     *
     * @return
     */
    public boolean closedWorldCheck() {

        boolean check = true;
        for (int one = 0; one < STATE; one++) {
            for (int two = 0; two < STATE; two++) {
                if (queryOrder[one].hasRelaionTo(queryOrder[two])) {
                    if (!MappingOrder[one].hasRelaionTo(MappingOrder[two])) {
                        return false;
                    }
                }
                if (!queryOrder[one].hasRelaionTo(queryOrder[two])) {
                    if (MappingOrder[one].hasRelaionTo(MappingOrder[two])) {
                        return false;
                    }
                }
            }
        }
        return check;
    }
}
