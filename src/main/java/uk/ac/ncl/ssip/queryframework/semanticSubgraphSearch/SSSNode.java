/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework.semanticSubgraphSearch;

import uk.ac.ncl.ssip.metadata.SSIPNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 * 
 * This class provides a node type for the SSSDatastructure. Due to the fact
 * that the same node may appear in the structure multiple times this class
 * allows for a UID to used to keep the nodes distinct and allow for duplication.
 * 
 */
public class SSSNode {

    private String UID;
    private SSIPNode ssip;

    public SSSNode(String UID, SSIPNode ssip) {
        this.UID = UID;
        this.ssip = ssip;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public SSIPNode getSsip() {
        return ssip;
    }

    public void setSsip(SSIPNode ssip) {
        this.ssip = ssip;
    }
}
