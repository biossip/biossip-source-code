/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework.semanticSubgraphSearch;

import java.util.Set;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class SSSQuery {

    private SSIPNode from;
    private Set<String> type;
    private SSIPNode to;

    public SSSQuery(SSIPNode from,SSIPNode to,  Set<String> type) {
        this.from = from;
        this.type = type;
        this.to = to;
    }

    public SSIPNode getFrom() {
        return from;
    }

    public Set<String>  getType() {
        return type;
    }

    public SSIPNode getTo() {
        return to;
    }
    
    public Set<String> getReltypes() {
        return type;
    }

    public void setFrom(SSIPNode from) {
        this.from = from;
    }

    public void setType(Set<String>  type) {
        this.type = type;
    }

    public void setTo(SSIPNode to) {
        this.to = to;
    }

    public void printOutQuery() {
        System.out.println("FROM: " + from.getId());
        System.out.println("TO: " + to.getId());
        System.out.println("Types: " + type.toString());
    }
}
