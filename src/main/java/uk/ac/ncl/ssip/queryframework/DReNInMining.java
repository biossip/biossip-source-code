/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework;

import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class DReNInMining {

    public StepDescription[] getAllTargets() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.BINDS_TO);
        return steps;
    }

    public StepDescription[] getAllNeighbours(int depth) {
        StepDescription[] steps = new StepDescription[depth];
        for (int i = 0; i < depth; i++) {
            steps[i] = new StepDescription(1, true);

        }
        return steps;
    }

    public StepDescription[] getAllRareDiseases() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.INVOLVED_IN);
        return steps;
    }
    
    public StepDescription[] getDrugDrugInteractions() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.INTERACTS_WITH);
        return steps;
    }
    
    public StepDescription[] getAllNDFRTInds() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.MAY_PREVENT);
        steps[0].addRelation(StepDescription.RelTypes.MAY_TREAT);
        return steps;
    }
    
     public StepDescription[] getAllSIDERInds() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.TREATS);
        return steps;
    }
     
     public StepDescription[] getAllSIDERSideEFFECTS() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.HAS_SIDE_EFFECT);
        return steps;
    }
    
     public StepDescription[] getDiseases() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.INVOLVED_IN);
        return steps;
    }
     
     public StepDescription[] getDISGENETDiseases() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.DISGENET_INVOLVED_IN);
        return steps;
    }
     
    
    public StepDescription[] getAllPathways() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.PART_OF_PATHWAY);
        return steps;
    }

    public StepDescription[] getAllCCGOTerms() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.LOCATED_IN_CELLULAR_COMPONENT);
        return steps;
    }

    public StepDescription[] getGene() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.IS_ENCODED_BY);
        return steps;
    }

    public StepDescription[] getProteinInteractions() {
        StepDescription[] steps = new StepDescription[1];
        steps[0] = new StepDescription(1, true);
        steps[0].addRelation(StepDescription.RelTypes.INTERACTS_WITH_PROTEIN);
        return steps;
    }
}
