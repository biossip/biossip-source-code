/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework.GremlinTutorials;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraphFactory;

/**
 *
 * @author joemullen
 */
public class Tutorial01 {

    public static void main(String[] args) {

        Tutorial01 te = new Tutorial01();
        te.testMethods();

    }

    public void testMethods() {

        //create tinkergraph (a 6/6 graph provided with Gremlin)
        TinkerGraph g = TinkerGraphFactory.createTinkerGraph();

        System.out.println("[INFO] Vertice IDs");

        //look at all the vertices
        for (Vertex v : g.getVertices()) {
            System.out.println(v.getId());
        }

        System.out.println("[INFO] Vertice names");
        //look at all the names of the vertices
        for (Vertex v : g.getVertices()) {
            for (String prop : v.getPropertyKeys()) {
                System.out.println(prop + ":" + v.getProperty(prop));
            }
        }

        System.out.println("[INFO] Graph Edges");
        //look at all the edges
        for (Edge e : g.getEdges()) {
            System.out.println(e.toString());
        }


        Vertex v = g.getVertex("1");

        System.out.println("[INFO] All outoging edges of v");
        //whole edge
        for (Edge out : v.getEdges(Direction.OUT)) {
            System.out.println(v.toString());

        }

        System.out.println("[INFO] All outoging edges of v target node");
        //traversal from one vertex to the adjacent outgoing vertices
        for (Edge out : v.getEdges(Direction.OUT)) {
            System.out.println(out.getVertex(Direction.IN));

        }

        System.out.println("[INFO] All outoging edges of v of type 'knows'");
        //just look at the 'knows' labelled edges
        for (Edge out : v.getEdges(Direction.OUT, "knows")) {
            System.out.println(out.toString());

        }

        System.out.println("[INFO] All outoging edges of v and types");
        //look at the edge type 
        for (Edge out : v.getEdges(Direction.OUT)) {
            System.out.println(out.toString());

        }

        System.out.println("[INFO] Weight values on all of thedges leaving v");
        //look at the edge weights 
        for (Edge out : v.getEdges(Direction.OUT)) {
            System.out.println(out.getProperty("weight"));
        }

        System.out.println("[INFO] Only look at those edges foutgoing from v whose weight is less than 1");
        //look at the edge weights 
        for (Edge out : v.getEdges(Direction.OUT)) {
            double w = Double.parseDouble(out.getProperty("weight").toString());
            if (w < 1.0) {
                System.out.println(w);
            }
           

        }




        //look at the edge weights 
        for (Edge out : v.getEdges(Direction.OUT)) {
            //returns the relation types
            System.out.println(out.getLabel());

        }








    }
}
