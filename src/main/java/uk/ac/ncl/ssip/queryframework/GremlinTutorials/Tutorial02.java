/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework.GremlinTutorials;

import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraphFactory;
import com.tinkerpop.gremlin.java.GremlinPipeline;

/**
 * @author joemullen
 * Look at https://github.com/tinkerpop/gremlin/wiki/Using-Gremlin-through-Java
 * pipes in Gremlin appear to be algorithms
 */
public class Tutorial02 {
    

    public static void main(String[] args) {

        Tutorial02 te = new Tutorial02();
        te.testMethods();

    }

    public void testMethods() {

        //create tinkergraph (a 6/6 graph provided with Gremlin)
        TinkerGraph g = TinkerGraphFactory.createTinkerGraph();
        GremlinPipeline pipe = new GremlinPipeline();
        pipe.start(g.getVertex(1)).out("knows").property("name");
        System.out.println(pipe.next());

    }
}
