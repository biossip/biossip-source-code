///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package uk.ac.ncl.ssip.queryframework.semanticSubgraphSearch;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.Set;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import org.graphstream.graph.*;
//import org.graphstream.graph.implementations.SingleGraph;
//import org.graphstream.stream.file.FileSinkImages;
//import org.graphstream.stream.file.FileSinkImages.LayoutPolicy;
//import org.graphstream.stream.file.FileSinkImages.OutputType;
//import org.graphstream.stream.file.FileSinkImages.RendererType;
//import org.graphstream.stream.file.FileSinkImages.Resolutions;
//import org.openide.util.Exceptions;
//import uk.ac.ncl.ssip.metadata.SSIPNode;
//
///**
// *
// * @author joemullen
// *
// * Allows a simple visualisation of the data structure; either to the console
// * and to file or straight to file.
// */
//public class SSSDSVisualisation {
//
//    private String opdirec = "/Users/joemullen/Desktop/DiseaseOP/";
//    private SSSDataStructure ssd;
//    private Graph graph;
//    private String name;
//    private Set<String> highLight;
//    private boolean onlyToFile;
//
//    public SSSDSVisualisation(SSSDataStructure ssd, String name, boolean onlyToFile) {
//
//        this.name = name;
//        graph = new SingleGraph(name);
//        this.ssd = ssd;
//        this.onlyToFile = onlyToFile;
//
//    }
//
//    /**
//     * Uses graphstream to visualise the SSS data structure.
//     */
//    public void visualise() {
//
//        FileSinkImages pic = new FileSinkImages(OutputType.PNG, Resolutions.VGA);
//        pic.setRenderer(RendererType.SCALA);
//
//        pic.setLayoutPolicy(LayoutPolicy.COMPUTED_FULLY_AT_NEW_IMAGE);
//
//
//        if (onlyToFile == false) {
//            graph.display();
//        }
//
//        graph.addAttribute(
//                "ui.stylesheet", "url('SSSgraphstreamStyle.css')");
//
//        System.setProperty("org.graphstream.ui.renderer",
//                "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
//
//
//        //colour nodes based on their state
//        int count1 = 1;
//
//        Iterator<SSIPNode> it = ssd.getGraph().vertexSet().iterator();
//        while (it.hasNext() ) {
//            SSIPNode v = it.next();
//            int depth = ssd.getShortestPathLength(ssd.getParentNode(), v);
//            switch (depth) {
//                case 1:
//                    graph.addNode(v.getId() + v.getType() + count1).addAttribute("ui.class", "state0");
//                    count1++;
//                    break;
//                case 2:
//                    graph.addNode(v.getId() + v.getType() + count1).addAttribute("ui.class", "state1");
//                    count1++;
//                    break;
//                case 3:
//                    graph.addNode(v.getId() + v.getType() + count1).addAttribute("ui.class", "state2");
//                    count1++;
//                    break;
//
//                case 4:
//                    graph.addNode(v.getId() + v.getType() + count1).addAttribute("ui.class", "state3");
//                    count1++;
//                    break;
//
//                case 5:
//                    graph.addNode(v.getId() + v.getType() + count1).addAttribute("ui.class", "state4");
//                    count1++;
//                    break;
//                case 6:
//                    graph.addNode(v.getId() + v.getType() + count1).addAttribute("ui.class", "state5");
//                    count1++;
//                    break;
//            }
//
//        }
//
//        Set<String> dupedEdges = new HashSet<String>();
//        count1 = 1;
//        int count2 = 1;
//        //addrels
//        for (SSIPNode v : ssd.getGraph().vertexSet()) {
//            count2 = 1;
//            for (SSIPNode v2 : ssd.getGraph().vertexSet()) {
//                if (ssd.getGraph().containsEdge(v, v2) && !dupedEdges.contains(v2.getId() + count2 + v.getId() + count1)) {
//                    graph.addEdge(v.getId() + count1 + v2.getId() + count2, v.getId() + v.getType() + count1, v2.getId() + v2.getType() + count2);
//                    dupedEdges.add(v.getId() + count1 + v2.getId() + count2);
//                }
//                count2++;
//            }
//            count1++;
//
//        }
//
//        if (onlyToFile == false) {
//            graph.display();
//        }
//
//        try {
//
//            pic.writeAll(graph, opdirec + name + System.currentTimeMillis() / 100 + ".png");
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }
//
//    }
//}
