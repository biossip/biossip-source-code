/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework.semanticSubgraphSearch;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * This class creates a SSIP sub graph and provides methods for it to drive the
 * SSSSearch.
 */
public class SSSSub {

    private Set<SSIPNode> queryGraph;
    private SSIPNode[] queryOrder;
    private SSSQuery[] querySteps;
    int testSize = 0;
    private static int STATE = 1;
    //SSIPNode from
    SSIPNode queryFrom = null;
    SSIPRelationType.RelTypes relType = null;

    public static void main(String[] args) {

        String startingClass = DReNInMetadata.NodeType.PROTEIN.getText();
        SSSSub s = new SSSSub();
        s.getFirstNode(startingClass);
        s.calcSubOrderSub(startingClass);
        SSIPNode[] queryOrder = s.getQueryOrder();
        s.printOrder();
        SSSQuery[] steps = s.getQuerySteps();
        for (int y = 0; y < steps.length; y++) {
            steps[y].printOutQuery();
        }

    }

    public SSSQuery[] getQuerySteps() {
        return querySteps;
    }

    public int getTestSize() {
        return testSize;
    }

    public static int getSTATE() {
        return STATE;
    }

    public SSIPNode getQueryFrom() {
        return queryFrom;
    }

    public SSIPRelationType.RelTypes getRelType() {
        return relType;
    }

    public SSSSub() {

        this.queryGraph = createSimple();
        this.queryOrder = new SSIPNode[testSize];

    }

    public Set<SSIPNode> getQueryGraph() {
        return queryGraph;
    }

    public SSIPNode[] getQueryOrder() {
        return queryOrder;
    }

    public Set<SSIPNode> createSimple() {
        Set<SSIPNode> graph = new HashSet<SSIPNode>();
        SSIPNode node1 = new SSIPNode(DReNInMetadata.NodeType.PROTEIN.getText(), "pro_1");
        SSIPNode node2 = new SSIPNode(DReNInMetadata.NodeType.PROTEIN.getText(), "pro_2");
        SSIPNode node3 = new SSIPNode(DReNInMetadata.NodeType.PROTEIN.getText(), "prot_3");
       

        node1.addRelation(node2, new SSIPRelationType(DReNInMetadata.RelTypes.INTERACTS_WITH_PROTEIN.toString(), 1));
        node2.addRelation(node1, new SSIPRelationType(DReNInMetadata.RelTypes.INTERACTS_WITH_PROTEIN.toString(), 1));
        
        node2.addRelation(node3, new SSIPRelationType(DReNInMetadata.RelTypes.INTERACTS_WITH_PROTEIN.toString(), 1));
        node3.addRelation(node2, new SSIPRelationType(DReNInMetadata.RelTypes.INTERACTS_WITH_PROTEIN.toString(), 1));
        
        node3.addRelation(node1, new SSIPRelationType(DReNInMetadata.RelTypes.INTERACTS_WITH_PROTEIN.toString(), 1));
        node1.addRelation(node3, new SSIPRelationType(DReNInMetadata.RelTypes.INTERACTS_WITH_PROTEIN.toString(), 1));


        graph.add(node1);
        graph.add(node2);
        graph.add(node3);


        SSIPNode[] qNodes = new SSIPNode[graph.size()];

        testSize = graph.size();
        //querysteps capture the relations and so there is one less
        querySteps = new SSSQuery[testSize-1];
        return graph;

    }

    /**
     * Add nodes with no outgoing edges to the order
     */
    public SSIPNode incomingNodes() {

        SSIPNode next = null;
        for (SSIPNode toAdd : queryGraph) {
            if (alreadyAdded(toAdd) == false) {
                if (attachedToOrder(toAdd)) {
                    next = toAdd;
                }
            }
        }
        return next;
    }

    /**
     * Calculates the order at which the subgraph will be searched for. This is
     * done using the outgoing edges of the nodes and traversing these to get
     * the next node. If there are no more outgoing edges then incoming edges of
     * nodes that have not been added to the order are looked at.
     */
    public void calcSubOrderSub(String startingClass) {
        //we have already added the first node
        getFirstNode(startingClass);

        //while we haven't added all the nodes to the queryOrder
        while (STATE < testSize) {

            SSIPNode previous = queryOrder[STATE - 1];
            queryFrom = previous;
            SSIPNode toAdd = null;
            int toAddRels = 0;
            Set<SSIPNode> connected = getAllConnectedNodes(previous);
            //if there are no more outgoing nodes to go through then go back
            //or all of thepossible outgoing nodes have already been added
            //then look at the outgoing nodes
            if (connected.isEmpty() || alreadyAddedSet(connected)) {
                toAdd = incomingNodes();
            } //            //when we leave the while loop state may be increased; so leave if it has surpassed
            else {
                for (SSIPNode q : connected) {
                    //add the node with the greates number of outgoing edges to the order first
                    //if it is has not already been added to the order array
                    if (alreadyAdded(q) == false) {
                        int relSize = getEdgeset(q);
                        if (relSize >= toAddRels) {
                            toAdd = q;
                            toAddRels = relSize;
                        }
                    }
                }
            }
            STATE++;
            queryOrder[STATE - 1] = toAdd;
            SSSQuery s = new SSSQuery(queryFrom, toAdd, getRelationTypes(queryFrom, toAdd));
            //don't have a relation from nothing to the first node and so the querysteps start
            //at the second node= when state =2
            querySteps[STATE - 2] = s;
            printQuerySteps();
        }

    }

    /**
     * Returns the first node that will be in the search. This is calculated as
     * the node with the highest outgoing connectivity of the specified
     * startingClass.
     *
     * @param startingClass
     */
    public void getFirstNode(String startingClass) {
        SSIPNode first = null;
        int outGoingEdges = 0;
        for (SSIPNode q : queryGraph) {
            if (q.getType().equals(startingClass)) {
                if (getEdgeset(q) > outGoingEdges) {
                    first = q;
                    outGoingEdges = getEdgeset(q);
                }
            }
        }
        queryOrder[0] = first;
    }

    /**
     * Returns all nodes that have an outgoing relation from check.
     *
     * @param check
     * @return
     */
    public Set<SSIPNode> getAllConnectedNodes(SSIPNode check) {
        Set<SSIPNode> outGoing = new HashSet<SSIPNode>();
        Map<MetaDataInterface, SSIPRelationType> rels = check.getRelations();
        for (MetaDataInterface node : rels.keySet()) {
            outGoing.add((SSIPNode) node);
        }
        return outGoing;
    }

    /**
     * Checks if the node in question has already been added to queryOrder.
     *
     * @param check
     * @return
     */
    public boolean alreadyAdded(SSIPNode check) {

        boolean duplicate = false;
        for (int i = 0; i < queryOrder.length; i++) {
            if (queryOrder[i] != null) {
                if (queryOrder[i].equals(check)) {
                    return true;
                }
            }
        }
        return duplicate;

    }

    /**
     * Checks the QueryOrder for a set of nodes. If all the nodes in the Set
     * have been added to the QueryOrder the method returns true.
     *
     * @param check
     * @return
     */
    public boolean alreadyAddedSet(Set<SSIPNode> check) {

        boolean duplicate = false;
        int nodes = check.size();
        int added = 0;
        for (SSIPNode dup : check) {
            for (int i = 0; i < queryOrder.length; i++) {
                if (queryOrder[i] != null) {
                    if (queryOrder[i].equals(dup)) {
                        //return true;
                        added++;
                    }
                }
            }
        }
        if (added == nodes) {
            return true;
        }

        return duplicate;

    }

    /**
     * Checks to see if a node with no outgoing edges is attached to another
     * node in the graph through an incoming edge.
     *
     * @param check
     * @return
     */
    public boolean attachedToOrder(SSIPNode check) {
        boolean duplicate = false;
        for (int y = 0; y < queryOrder.length; y++) {
            if (queryOrder[y] != null) {
                Set<SSIPNode> outGoing = new HashSet<SSIPNode>();
                Map<MetaDataInterface, SSIPRelationType> rels = queryOrder[y].getRelations();
                for (MetaDataInterface node : rels.keySet()) {
                    if (check.equals(node)) {
                        queryFrom = queryOrder[y];
                        return true;
                    }
                }
            }
        }
        return duplicate;

    }

    /**
     * Method returns the number of outgoing edges from a node.
     *
     * @param sip
     * @return
     */
    public int getEdgeset(SSIPNode sip) {
        int edgeset = 0;
        Map<MetaDataInterface, SSIPRelationType> rels = sip.getRelations();
        edgeset = rels.size();
        return edgeset;

    }

    /**
     * Returns the relation types that are present between node From and node
     * To.
     *
     * @param from
     * @param to
     * @return
     */
    public Set<String> getRelationTypes(SSIPNode from, SSIPNode to) {
        Set<String> rels = new HashSet<String>();
        Map<MetaDataInterface, SSIPRelationType> rela = from.getRelations();
        for (MetaDataInterface node : rela.keySet()) {
            if (to.equals(node)) {
                rels.add(rela.get(node).name());
            }

        }
        return rels;
    }

    /**
     * Prints out the QueryOrder to the console.
     */
    public void printOrder() {
        for (int i = 0; i < queryOrder.length; i++) {
            if (queryOrder[i] != null) {
                System.out.println(i + ":  " + queryOrder[i].getId());
            }
        }
    }
    
    
    public void printQuerySteps() {
        for (int i = 0; i < querySteps.length; i++) {
            System.out.println("STEP: "+ i);
            if (querySteps[i] != null) {
                querySteps[i].printOutQuery();
            }
        }
    }
}
