/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework.semanticSubgraphSearch;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.ssip.metadata.SSIPNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * This class provides an undirected data structure to store potential mappings
 * during a semantic subgraph search. Allowing for an exhaustive search to be
 * implemented.
 */
public class SSSDataStructure {

    SimpleGraph<SSIPNode, SSSEdge> mappingsTree;
    private int UID = 0;
    private SSIPNode parentNode;

    public SSSDataStructure() {
        this.mappingsTree = new SimpleGraph<>(
                SSSEdge.class);
    }

    /**
     * Returns the parent node of the data structure.
     *
     * @return
     */
    public SSIPNode getParentNode() {
        return parentNode;
    }

    /**
     * Returns the graph data structure..
     *
     * @return
     */
    public SimpleGraph<SSIPNode, SSSEdge> getGraph() {
        return mappingsTree;
    }

    /**
     * Adds a node to the graph.
     *
     * @param add
     */
    public void addNode(SSIPNode add) {
        if (mappingsTree.vertexSet().isEmpty()) {
            this.parentNode = add;
        }
        //System.out.println("added: " + add.getId() + add.getType());
        mappingsTree.addVertex(add);
    }

    /**
     * Adds all potential next nodes to the data structure and a relation to the
     * node from which the traversal identified them.
     *
     * @param add
     * @param from
     */
    public void addNextNodes(Set<SSIPNode> add, SSIPNode from) {
        for (SSIPNode s : add) {
            mappingsTree.addVertex(s);
            mappingsTree.addEdge(from, s, new SSSEdge<SSIPNode>(from, s));

        }
    }

    /**
     * Allows relations to be added to the data structure.
     *
     * @param from
     * @param to
     */
    public void addRelations(SSIPNode from, SSIPNode to) {
        mappingsTree.addEdge(from, to);
    }

    /**
     * Returns a string representation of the data structure.
     *
     * @return
     */
    public String toString() {
        String graph = "\n";
        int node = 1;
        for (SSIPNode v : mappingsTree.vertexSet()) {
            graph = graph + "[NODES" + node + "]: " + v.getId() + "\t" + v.getType() + "\n";
            node++;
        }
        for (SSIPNode v : mappingsTree.vertexSet()) {
            int intcheck = mappingsTree.vertexSet().size();
            for (SSIPNode v2 : mappingsTree.vertexSet()) {
                if (mappingsTree.containsEdge(v, v2)) {
                    graph = graph + "\t" + "[EDGE]: " + v.getId() + "\t" + v2.getId() + "\n";

                }
            }
        }
        return graph;
    }

    /**
     * Returns all leaf nodes of the data structure. Leaf nodes are identified
     * by the fact that they have only one associated edge.
     *
     * @return
     */
    public Set<SSIPNode> getLeafNodes() {
        Set<SSIPNode> leaf = new HashSet<SSIPNode>();
        for (SSIPNode v : mappingsTree.vertexSet()) {
            if (mappingsTree.edgesOf(v).size() == 1) {
                leaf.add(v);
            }
        }
        return leaf;
    }

    /**
     * Returns the depth of the data structure.
     *
     * @return
     */
    public int getTreeDepth() {
        int depth = 0;
        for (SSIPNode v : mappingsTree.vertexSet()) {
            int shortestPathLength = getShortestPathLength(parentNode, v);
            if (shortestPathLength > depth) {
                depth = shortestPathLength;
            }
        }
        return depth;
    }

    /**
     * Returns the edge length of a shortest path between to nodes in the data
     * structure.
     *
     * @param from
     * @param to
     * @return
     */
    public int getShortestPathLength(SSIPNode from, SSIPNode to) {
        @SuppressWarnings("unchecked")
        DijkstraShortestPath tr2 = new DijkstraShortestPath(mappingsTree,
                from, to);
        List<Object> why = tr2.getPath().getEdgeList();
        return why.size();
    }

    /**
     * Returns all SSIPNodes which are present on the shortest path between two
     * nodes in the data structure.
     * 
     * This is also used to get the mapping so far.
     *
     * @param from
     * @param to
     * @return
     */
    public SSIPNode[] getShortestPathNodes(SSIPNode from, SSIPNode to) {
        @SuppressWarnings("unchecked")
        DijkstraShortestPath tr2 = new DijkstraShortestPath(mappingsTree,
                from, to);
        Object[] why = tr2.getPath().getEdgeList().toArray();
        SSIPNode[] nodes = new SSIPNode[why.length + 1];
        for (int i = 0; i < why.length; i++) {
            SSSEdge se = (SSSEdge) why[i];
            //last node need to add both Node
            if (i == why.length - 1) {
                nodes[i] = se.getFrom();
                nodes[i + 1] = se.getTo();
            } else {
                nodes[i] = se.getFrom();
            }
        }
        return nodes;
    }

    /**
     * Return all nodes at a certain depth in the data structure. This will be
     * used when we have more complicated subgraphs, i.e. when there are more
     * than one edge coming out of them. We can only search for one at a time.
     *
     * @param depth
     * @return
     */
    public Map<SSIPNode, Set<SSIPNode>> getAllNodesAndChildrenAtDepth(int depth) {
        Map<SSIPNode, Set<SSIPNode>> nodes = new HashMap<SSIPNode, Set<SSIPNode>>();
        //keys = node at depth [depth]
        Set<SSIPNode> leafNodes = getLeafNodes();
        for (SSIPNode sip : leafNodes) {
            SSIPNode[] no = getShortestPathNodes(parentNode, sip);
            SSIPNode keyNode = no[depth - 1];
            SSIPNode childNode = no[no.length - 1];
            if (nodes.containsKey(keyNode)) {
                Set<SSIPNode> children = nodes.get(keyNode);
                children.add(childNode);
                nodes.put(keyNode, children);
            } else {
                Set<SSIPNode> children = new HashSet<SSIPNode>();
                children.add(childNode);
                nodes.put(keyNode, children);
            }
        }
        //values = all leaf nodes
        // that is so we can find all the nodes that come from KEY and add them to the
        // datastructure as being related to each of the values

        //get the leaf nodes
        //get the shortest path from them to the parent node
        //then extract the node from the array at point [depth]

        return nodes;
    }
    
    public void removeNodesFromDat(SSIPNode [] MappingOrder){
        //do not remove the parent node [1]
        for (int y=1; y < MappingOrder.length; y++){
            //remove relations
            Set<SSSEdge> rels = mappingsTree.edgesOf(MappingOrder[y]);
            for (SSSEdge r:rels){
                //check it hasn't already been deleted
                if (mappingsTree.containsEdge(r)){             
                    mappingsTree.removeEdge(r);
                }          
            }
            //remove node
            mappingsTree.removeVertex(MappingOrder[y]);
        
        }
    
    }
}
