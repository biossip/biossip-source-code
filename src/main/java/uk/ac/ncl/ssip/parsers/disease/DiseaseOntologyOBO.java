/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.disease;

import uk.ac.ncl.ssip.Disease_Graph;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.parsers.genericOBO.OboParse;
import uk.ac.ncl.ssip.queryframework.DReNInMining;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class DiseaseOntologyOBO extends OboParse {

    public static void main(String[] args) {

        String graphFile = "/Users/joemullen/Desktop/diseaseOnt-temp.gexf";
        Neo4J backend = new Neo4J("/Users/joemullen/Documents/neotest-disease-temp");
        String disont = "/Users/joemullen/Desktop/Datasets/DiseaseOntology/HumanDOr21.obo";
        backend.setCommitSize(10000);
        backend.initialiseDatabaseConnection();
        backend.createIndex();
        backend.syncIndexes(10);
        DiseaseOntologyOBO orph = new DiseaseOntologyOBO(disont, backend);
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
//        
//         //reinitialise connection 
//        backend.initialiseDatabaseConnection();
//
//        //resync the indexes
//        backend.syncIndexes(10);
//
//
//        System.out.println("export all code starts here...");
//        GephiExporter gexfExportSubgraph = new GephiExporter();
//        //query database
//        gexfExportSubgraph.export(backend.returnAllNodesMap(), graphFile);
//
//        //close database again 
//        backend.finaliseDatabaseConnectionNoUPDATES();

    }

    public DiseaseOntologyOBO(String file, GraphHandlerInterface handler) {
        super(file, OboParse.database.DISEASEONTOLOGY, handler);
    }
}
