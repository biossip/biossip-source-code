/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.OpenPHACTS;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import uk.ac.ncl.ssip.metadata.SSIPNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Diseases {

    private CallOpenPHACTS call;
    private Mapping freetext;
    private final String DISEASE_XPATH = "/result/items/item";
    //private final String TAR_COMPONENTS = "hasAssay/hasTarget/hasTargetComponent";
    String target = "http://purl.uniprot.org/uniprot/Q9Y5Y9";

    public Diseases(CallOpenPHACTS call) {
        this.call = call;
    }

    public static void main(String[] args) {
//
//        Diseases c = new Diseases();
//        c.requestInfo();
    }

    public Set<SSIPNode> getDiseaseNodes(String URI) {
        Document doc = null;
        try {
            doc = call.callXMLMethod(getAllDiseasesFromTargetRequest());
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        return extractDiseasesData(doc);

    }

    public Set<SSIPNode> extractDiseasesData(Document doc) {
        Set<SSIPNode> diseases = new HashSet<SSIPNode>();
        SSIPNode dis = null;
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        NodeList entry = null;
        String href = null;
        String name = null;
        String forGene = null;
        String encodesFor = null;

        try {
            entry = (NodeList) xpath.evaluate(DISEASE_XPATH, doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
        for (int i = 0; i < entry.getLength(); i++) {
            System.out.println("---------------");
            try {
                href = (String) xpath.evaluate("@href", entry.item(i), XPathConstants.STRING);
                name = (String) xpath.evaluate("name", entry.item(i), XPathConstants.STRING);
                forGene = (String) xpath.evaluate("forGene//@href", entry.item(i), XPathConstants.STRING);
                encodesFor = (String) xpath.evaluate("forGene/encodes//@href", entry.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
//            System.out.println("disease: " + href);
//            System.out.println("disease name: " + name);
//            System.out.println("forGene: " + forGene);
//            System.out.println("encodesFor: " + encodesFor);

            dis = new SSIPNode("Disease", name);
            diseases.add(dis);
        }

        return diseases;
    }

    public String getDiseaseInformation() {

        //need to complete
        return null;

    }

    public String getAllDiseasesFromTargetRequest() throws UnsupportedEncodingException {
        String urlCall = call.getOpenPHACTSURL() + "/disease/byTarget?uri=" + URLEncoder.encode(target, "UTF-8") + "&" + call.getAppID() + call.getAppKey() + call.getPageSize() + call.getReturnFormat();
        return urlCall;
    }
}
