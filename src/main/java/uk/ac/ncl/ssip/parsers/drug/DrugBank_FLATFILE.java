///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package uk.ac.ncl.ssip.parsers;
//
//import com.entanglementgraph.graph.Edge;
//import com.entanglementgraph.graph.EntityKeys;
//import com.entanglementgraph.graph.Node;
//import com.entanglementgraph.graph.commands.EdgeUpdate;
//import com.entanglementgraph.graph.commands.NodeUpdate;
//import com.scalesinformatics.util.UidGenerator;
//import java.io.BufferedReader;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//
///**
// *
// * @author joemullen 
// * 
// * This parser has been developed for the integration of older (pre 4) versions
// * of DrugBank http://www.drugbank.ca/ - for versions higher than 3 please use 
// * the DrugBank XML parser.
// *
// */
//
//public class DrugBank_FLATFILE {
//
//    private String drugbankDB;
//    private Graph compGraph;
//    private Graph tarGraph;
//    private Graph bindsGraph;
//    private DataSource dataSource;
//
//    public DrugBank_FLATFILE(String dbloc, String clustername, String cluster, String dbname, String graphname, String username, String password, int subSize) throws IOException {
//
//        this.drugbankDB = dbloc;
//        this.dataSource = new DrugBank();
//        this.compGraph = new Graph(clustername, cluster, dbname, "Compounds", username, password, subSize);
//        compGraph.establishDbConnection();
//        this.tarGraph = new Graph(clustername, cluster, dbname, "Target", username, password, subSize);
//        tarGraph.establishDbConnection();
//        this.bindsGraph = new Graph(clustername, cluster, dbname, "Comp-Tar", username, password, subSize);
//        bindsGraph.establishDbConnection();
//        execute();
//
//    }
//
//    public void execute() throws FileNotFoundException, IOException {
//
//
//        InputStream stream = null;
//        InputStreamReader reader = null;
//        BufferedReader buffered = null;
//        int targetCount = 1;
//        String targets = null;
//        String allTargets = "";
//
//        //properties required for a compound 
//        String DBID = null;
//        String name = null;
//        Set<String> accessions = new HashSet<String>();
//        Set<String> type = new HashSet<String>();
//        Set<String> category = new HashSet<String>();
//        String smiles = null;
//        String inchi = null;
//        String inchikey = null;
//        String organism = null;
//        boolean organismCheck = false;
//        boolean typeCheck = false;
//        boolean categoryCheck = false;
//        //proprerties required for a Target
//        String targetName = null;
//        boolean getTargetID = false;
//        boolean tarseqcheck = false;
//        String targetSeq = null;
//
//        List<String> allTargetSSs = new ArrayList<String>();
//
//        try {
//            // open input stream test.txt for reading purpose.
//            stream = new FileInputStream(drugbankDB);
//            // create new input stream reader
//            reader = new InputStreamReader(stream);
//            // create new buffered reader
//            buffered = new BufferedReader(reader);
//            String line;
//            // reads to the end of the stream 
//            while ((line = buffered.readLine()) != null) {
//                if (line.startsWith("#BEGIN_DRUGCARD")) {
//                    DBID = line.substring(16);
//                    accessions.add(DBID);
//                }
//                if (line.startsWith("# Smiles_String_isomeric:")) {
//                    smiles = buffered.readLine();
//                }
//
//                if (line.startsWith("# Drug_Target_" + targetCount + "_SwissProt_ID:")) {
//                    targets = buffered.readLine();
//                    if (targetCount > 1) {
//                        allTargets = allTargets + "\t" + targets;
//                    } else {
//                        allTargets = targets;
//                    }
//                    if (!allTargetSSs.contains(targets)) {
//                        allTargetSSs.add(targets);
//                        Set<String> accs = new HashSet<String>();
//                        accs.add(targets);
//                        createEntTarget(targets, targetName, accs, targetSeq);
//                    } else {
//                        //System.out.println("Already added: " + targets);
//                    }
//                    targets = "";
//                    targetSeq = "";
//                    targetCount++;
//                }
//
//                if (line.startsWith("# Drug_Target_" + targetCount + "_Name:")) {
//                    targetName = buffered.readLine();
//                }
//
//                if (line.isEmpty()) {
//                    tarseqcheck = false;
//                    typeCheck = false;
//                    categoryCheck = false;
//                    organismCheck = false;
//                }
//
//                if (tarseqcheck == true) {
//                    targetSeq = targetSeq + line;
//
//                }
//                if (typeCheck == true) {
//                    //older verions of the database have the types on a single line
//                    //separated by a semicolon
//                    if (line.contains(";")) {
//
//                        String[] split = line.trim().split(";");
//                        for (int y = 0; y < split.length; y++) {
//
//                            type.add(split[y].trim());
//                        }
//                     //newer versions don't
//                    } else {
//                        type.add(line.trim());
//                    }
//                }
//
//                if (categoryCheck == true) {
//                    category.add(line.trim());
//                }
//
//                if (line.startsWith("# Drug_Type")) {
//                    typeCheck = true;
//                }
//
//                if (line.startsWith("# Organisms_Affected:")) {
//                    organism = buffered.readLine();
//                }
//
//                if (line.startsWith("# Drug_Category")) {
//                    categoryCheck = true;
//                }
//
//                if (line.startsWith("# Drug_Target_" + targetCount + "_Protein_Sequence:")) {
//                    tarseqcheck = true;
//                    buffered.readLine();
//                }
//
//                if (line.startsWith("# Generic_Name:")) {
//                    name = buffered.readLine();
//                }
//
//                if (line.startsWith("#END_DRUGCARD")) {
//                    if (targets == "") {
//                        targets = "-";
//                    }
//                    if (DBID == "") {
//                        DBID = "-";
//                    }
//
//                    Node<Compound> from = createEntComp(DBID, name, accessions, type, category, smiles, organism);
//                    type = new HashSet<String>();
//                    category = new HashSet<String>();
//                    organism = "";
//                    accessions = new HashSet<String>();
//                    smiles = "";
//
//                    String[] split = allTargets.split("\t");
//                    for (int i = 0; i < split.length; i++) {
//                        createEntBindsTo(from, new EntityKeys(Target.class.getSimpleName(), split[i]));
//                    }
//
//                    targetCount = 1;
//                    DBID = "";
//                    allTargets = "";
//
//                }
//
//
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//
//            // releases resources associated with the streams
//            if (stream != null) {
//                stream.close();
//            }
//            if (reader != null) {
//                reader.close();
//            }
//            if (buffered != null) {
//                buffered.close();
//            }
//        }
//
//        //commit final transactions
//        compGraph.finalTransaction();
//        tarGraph.finalTransaction();
//        bindsGraph.finalTransaction();
//
//    }
//
//    @SuppressWarnings("unchecked")
//    public Node<Compound> createEntComp(String UID, String name, Set<String> accs, Set<String> type, Set<String> cat, String SMILES, String organism) {
//        Compound comp = new Compound(name, accs, type, cat, SMILES, organism, dataSource);
//        Node<Compound> compEntry = new Node<Compound>(new EntityKeys(Compound.class.getSimpleName(), UID), comp);
//        compGraph.addNodeUpdate(new NodeUpdate(compEntry));
//        return compEntry;
//    }
//
//    public Node<Target> createEntTarget(String UID, String name, Set<String> accs, String seq) {
//        Target tar = new Target(name, accs, seq, dataSource);
//        Node<Target> dbtar = new Node<Target>(new EntityKeys(Target.class.getSimpleName(), UID), tar);
//        tarGraph.addNodeUpdate(new NodeUpdate(dbtar));
//        return dbtar;
//    }
//
//    public void createEntBindsTo(Node from, EntityKeys to) {
//        binds_To hs = new binds_To(dataSource);
//        @SuppressWarnings("unchecked")
//        Edge edge = new Edge(new EntityKeys(binds_To.class.getSimpleName(), UidGenerator.generateUid()), hs);
//        edge.setFrom(from.getKeys());
//        edge.setTo(to);
//        bindsGraph.addEdgeUpdate(new EdgeUpdate(edge));
//    }
//}
