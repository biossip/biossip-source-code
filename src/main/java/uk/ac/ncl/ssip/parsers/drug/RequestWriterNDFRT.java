/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.drug;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 *
 * @author joemullen
 */
public class RequestWriterNDFRT {
      


    public String getAllInfo(String NUI, String APIURL) throws UnsupportedEncodingException {

        String urlCall =  APIURL + "/allInfo/" + URLEncoder.encode(NUI, "UTF-8");
        System.out.println("[INFO] urlCall calculated for getAllInfo()");

        return urlCall;

    }

    public String getMayTreat(String compoundNUI, String APIURL) throws UnsupportedEncodingException{

        //String urlCall = APIURL + "/nui=" + URLEncoder.encode(compoundNUI+"&roleName=may_treat {NDFRT}", "UTF-8") +"&transitive=false";
        String urlCall = APIURL + "/nui=" + URLEncoder.encode(compoundNUI, "UTF-8") +"&roleName=may_treat%20%7BNDFRT%7D&transitive=false";
        
        System.out.println("[INFO] urlCall calculated for getMayTreatInfo()");
        return urlCall;
    
    }
    public String getMayPrevent(String compoundNUI, String APIURL) throws UnsupportedEncodingException{

        String urlCall = APIURL + "/nui=" + URLEncoder.encode(compoundNUI, "UTF-8") +"&roleName=may_prevent%20%7BNDFRT%7D&transitive=false";
        System.out.println("[INFO] urlCall calculated for getMayPreventMethod()");
        return urlCall;
    
    }
    
    public String getAllKindNUIs(String kind, String APIURL) throws UnsupportedEncodingException{

        String urlCall = APIURL + "/allconcepts?kind=" + URLEncoder.encode(kind, "UTF-8");
        System.out.println("[INFO] urlCall calculated for getAllKindNUIs()");
        return urlCall;
    
    }
    
     public String getDiseaseUMLS(String diseaseNUI, String APIURL) throws UnsupportedEncodingException{
         
        // http://rxnav.nlm.nih.gov/REST/Ndfrt/properties/nui=N0000153235&propertyName=UMLS_CUI
     
        String urlCall = APIURL + "/properties/nui=" + URLEncoder.encode(diseaseNUI, "UTF-8") + "&propertyName=UMLS_CUI";
        System.out.println("[INFO] urlCall calculated for getDiseaseUMLS()");
        return urlCall;
    }
    
  
    
}
