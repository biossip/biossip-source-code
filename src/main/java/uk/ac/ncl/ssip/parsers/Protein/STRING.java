/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.protein;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 * This parser takes data from STRING, the p:p interaction database. Files
 * available from
 * http://string-db.org/newstring_cgi/show_download_page.pl?UserId=S0emM7neUjDY&sessionId=r9_vYyEEnwL6.
 * **YOU CAN SELECT ONLY HUMAN FILES** String -> UniProt mappings.txt
 * ftp://string-db.org/STRING/9.1/mapping_files/
 * 9606.protein.links.detailed.v9.1.txt protein-protein interactions with scores
 * 9606.protein.actions.detailed.v9.1.txt protein-protein interactions with
 * types and scores
 *
 * STRING is a database of known and predicted protein interactions. The
 * interactions include direct (physical) and indirect (functional)
 * associations; they are derived from four sources: 1.Genomic Context 2.
 * High-throughput Experiments 3. (Conserved) Coexpression 4.Previous Knowledge
 *
 * As there are multiple ids that a single STRING protein can map too in uniprot
 * we need to makes sure we are getting the most up-to-date UNIPROT id so we use
 * the mapping file from UniProt, downloaded from:
 * ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/by_organism/
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class STRING {

    private Map<String, Set<String>> StringToUniprot;
    private String MapFile;
    private String InteractionsFile;
    private GraphHandlerInterface handler;
    private int confidenceThreshold;
    private int interactionsAdded = 0;
    private final static Logger LOGGER = Logger.getLogger(STRING.class.getName());

    public static void main(String[] args) {

        Neo4J backend = new Neo4J("/Users/joemullen/Documents/n");
        String mappings = "/Users/joemullen/Desktop/Datasets/STRING/UNIPROTPROTEINMAPSrelease.2012_1.vs.human.string.v9.1.via_blast.v1.02172012.txt";

        String interactionsWithTypes = "/Users/joemullen/Desktop/Datasets/STRING/9606.protein.actions.detailed.v9.1.txt";

        STRING interactionsadd = new STRING(600, mappings, interactionsWithTypes, backend);
        interactionsadd.run();


    }

    public STRING(int confidence, String MapFile, String InteractionsFile, GraphHandlerInterface handler) {
        this.confidenceThreshold = confidence;
        this.StringToUniprot = new HashMap<String, Set<String>>();
        this.MapFile = MapFile;
        this.InteractionsFile = InteractionsFile;
        this.handler = handler;
    }

    public void run() {
        //getAccessionMappings();
        getInteractionsTypesAndOverallScores();
        // getInteractions();
    }

    public void getAccessionMappings() {
        String thisLine;
        //Open the file for reading
        try {
            LOGGER.info("Getting accession maps from "
                    + MapFile);
            BufferedReader br = new BufferedReader(new FileReader(MapFile));
            //miss the first line
            br.readLine();
            while ((thisLine = br.readLine()) != null) {
                //variables
                String uniprotUID = null;
                String string_9 = null;
                double percentIdentityBlast = 0.0;
                //split the line on tab
                String[] split = thisLine.split("\t");
                //split first string to get uniprot
                uniprotUID = split[0];
                //the string_9.0 id is here
                string_9 = split[1];
                //blast identity
                percentIdentityBlast = Double.parseDouble(split[2]);


                //make sure it is the correct uniprot accession
                if (uniprotUID.length() > 6 && percentIdentityBlast == 100.00) {
                    //add these to the map
                    if (StringToUniprot.containsKey(string_9)) {
                        Set<String> uniProtUIDs = StringToUniprot.get(string_9);
                        uniProtUIDs.add(uniprotUID);
                        StringToUniprot.put(string_9, uniProtUIDs);

                    } else {
                        Set<String> uniProtUIDs = new HashSet<String>();
                        uniProtUIDs.add(uniprotUID);
                        StringToUniprot.put(string_9, uniProtUIDs);
                    }

                }

            }
        } catch (IOException e) {
            System.err.println("Error: " + e);
        }

        // System.out.println(StringToUniprot.toString());
    }

    public void getInteractionsTypesAndOverallScores() {
        //variables
        String protein1 = null;
        String protein2 = null;
        String mode = null;
        String action = null;
        int a_is_acting = 0;
        int combined_score = 0;
        String sources = null;
        String transferred_sources = null;

        String thisLine;
        //Open the file for reading
        try {

            LOGGER.info("Getting interactions from "
                    + InteractionsFile);
            BufferedReader br = new BufferedReader(new FileReader(InteractionsFile));
            //skip first line
            br.readLine();
            while ((thisLine = br.readLine()) != null) {

                SSIPNode ssipProt1 = null;
                SSIPNode ssipProt2 = null;
                //split the line on tab
                String[] split = thisLine.split("\t");
                //assign strings
                protein1 = split[0];
                protein2 = split[1];
                mode = split[2];
                action = split[3];
                a_is_acting = Integer.parseInt(split[4]);
                combined_score = Integer.parseInt(split[5]);


                if (combined_score > confidenceThreshold) {

                    ssipProt1 = new SSIPNode("STRING", protein1);
                    ssipProt2 = new SSIPNode("STRING", protein2);
                    SSIPRelationType symmetrical = new SSIPRelationType(DReNInMetadata.RelTypes.INTERACTS_WITH_PROTEIN.toString(), 1);

                    symmetrical.addAttribute("Mode", mode);
                    symmetrical.addAttribute("Score", combined_score/1000);
                    ssipProt1.addRelation(ssipProt2, symmetrical);
                    ssipProt2.addRelation(ssipProt1, symmetrical);

                    handler.addNode(ssipProt2);
                    handler.addNode(ssipProt1);
                    //syymetric relation
                    interactionsAdded++;
                    interactionsAdded++;
                }
            }
        } catch (IOException e) {
            System.err.println("Error: " + e);
        }

        LOGGER.info(interactionsAdded + " interactions added using a threshold of " + confidenceThreshold);

    }
}
