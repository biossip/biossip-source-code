/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.OpenPHACTS;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class contains methods that call the CallOpenPHACTS free text query.
 * This ensures that everything we parse utilises the same accessions.
 *
 * @author joemullen
 */
public class CallOpenPHACTS {

    private String OPENPHACTSURL = "https://beta.openphacts.org/1.4";
    private String APPID = "app_id=374a8178";
    private String APPKEY = "&app_key=35178959e35c09eaeeed7ecbfc91aa10";
    private String RETURNFORMAT = CallOpenPHACTS.format.xml.toString();
    private String RETURNMETADATA = CallOpenPHACTS.metadata.formats.toString();
    private String ASS_ORGANISM = "&assay_organism=Homo+sapiens";
    private String TAR_ORGANISM = "&target_organism=Homo+sapiens";
    private String MINEXACTIVITY = "&minEx-activity_value=8300";
    private String PAGESIZE = "&_pageSize=all";
    private String SIMTYTPE = CallOpenPHACTS.similarityType.Tanimoto.type;
    private String THRESHOLD = threshold(0.91);
    private final String ACCESSION_XPATH = "/result/primaryTopic/result/item//@href";
    private final static Logger LOGGER = Logger.getLogger(CallOpenPHACTS.class.getName());

    public static void main(String[] args) {
    }

    public Document callXMLMethod(String request) {
        Document doc = null;
        try {
            HttpClient openPHACTSclient = HttpClientBuilder.create().build();
            HttpGet getMethod = new HttpGet(request);
            LOGGER.info("Executing request: " + "\n" + getMethod.getURI());
            HttpResponse reponseBody = null;
            try {
                reponseBody = openPHACTSclient.execute(getMethod);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
            HttpEntity entity = reponseBody.getEntity();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = null;
            try {
                builder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                Exceptions.printStackTrace(ex);
            }
            doc = builder.parse(entity.getContent());
            getMethod.releaseConnection();
            LOGGER.info("Connection Released");
        } catch (SAXException | IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return doc;
    }

    public enum format {

        json, tsv, rdf, rdfjson, html, ttl, xml;

        @Override
        public String toString() {
            String f = super.toString();
            return "&_format=" + f;
        }
    }

    public enum similarityType {

        Tanimoto(0), Tversky(1), Euclidian(2);
        String type;

        similarityType(int type) {

            this.type = "&searchOptions.SimilarityType=" + type;

        }
    }

    public String splitAtHash(String URI) {
        String[] URISplit = URI.split("#");
        return URISplit[1];
    }

    public enum metadata {

        execution, sites, formats, view, all;
    }

    private String threshold(double score) {
        String returning = "&searchOptions.Threshold=" + score;
        return returning;

    }
    
    

    public String getAppID() {
        return APPID;
    }

    public String getAppKey() {
        return APPKEY;
    }

    public String getAssOrganism() {
        return ASS_ORGANISM;
    }

    public String getTarOrganism() {
        return TAR_ORGANISM;
    }

    public String getReturnFormat() {
        return RETURNFORMAT;
    }

    public String getReturnMetadata() {
        return RETURNMETADATA;
    }

    public String getOpenPHACTSURL() {
        return OPENPHACTSURL;
    }

    public String getPageSize() {
        return PAGESIZE;
    }
}
