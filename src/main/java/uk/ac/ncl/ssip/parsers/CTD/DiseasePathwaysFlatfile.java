/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.CTD;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import uk.ac.ncl.ssip.parsers.drug.SIDERFlatfile;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/ # IMPORTANT!!! Use
 * is subject to the terms set forth at http://ctdbase.org/about/legal.jsp #
 * These terms include: # # 1. All forms of publication (e.g., web sites,
 * research papers, databases, # software applications, etc.) that use or rely
 * on CTD data must cite CTD. # Citation guidelines:
 * http://ctdbase.org/about/publications/#citing # # 2. All electronic or online
 * applications must include hyperlinks from # contexts that use CTD data to the
 * applicable CTD data pages. # Linking instructions:
 * http://ctdbase.org/help/linking.jsp # # 3. You must notify CTD, and describe
 * your use of our data: # http://ctdbase.org/help/contact.go # # 4. For quality
 * control purposes, you must provide CTD with periodic # access to your
 * publication of our data. # # More information: http://ctdbase.org/downloads/
 * # # Report created: Thu Aug 07 10:09:37 EDT 2014
 *
 *
 */
public class DiseasePathwaysFlatfile {

    private String file = "/Users/joemullen/Desktop/Datasets/CTD/CTD_diseases_pathways.tsv";
    private final static Logger LOGGER = Logger.getLogger(DiseasePathwaysFlatfile.class.getName());
    private int rels = 0;
    private int reactomeRels = 0;
    private int KEGGrels = 0;

    public void run() {
        try {
            BufferedReader br = null;

            String thisLine;

            //Open the file for reading
            br = new BufferedReader(new FileReader(file));
            try {
                while ((thisLine = br.readLine()) != null) {
                    if (!thisLine.startsWith("#")) {
                        //data in line
                        String DiseaseName = null;
                        String DiseaseID = null;
                        String PathwayName = null;
                        String PathwayID = null;
                        String InferenceGeneSymbol = null;
                        //split the line
                        String[] split = thisLine.split("\t");
                        //assign values to variables
                        DiseaseName = split[0];
                        DiseaseID = split[1];
                        PathwayName = split[2];
                        PathwayID = split[3];
                        InferenceGeneSymbol = split[4];
                        System.out.println(DiseaseID + " associated with " + PathwayID);
                        rels++;

                        if (PathwayID.startsWith("REACT")) {
                            reactomeRels++;
                        }

                        if (PathwayID.startsWith("KEGG:")) {
                            KEGGrels++;
                        }

                    }
                }
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (FileNotFoundException ex) {
            Exceptions.printStackTrace(ex);

        }

        LOGGER.info("Parsed [" + rels + "] disease-pathway associations");
        LOGGER.info("Of which [" + reactomeRels + "] are reactome pathways");
        LOGGER.info("Of which [" + KEGGrels + "] are KEGG pathways");
    }
}
