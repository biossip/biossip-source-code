/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.protein;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * Used to map Gene Ontology terms to UniProt proteins.
 *
 * http://www.ebi.ac.uk/GOA/downloads
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class GOA {

    private String filePath;
    private GraphHandlerInterface handler;
    private boolean biologicalProcess;
    private boolean molecularFunction;
    private boolean cellularComponent;
    private Set<String> goCACHE;
    private int bp = 0;
    private int mf = 0;
    private int cc = 0;
    private final static Logger LOGGER = Logger.getLogger(GOA.class.getName());

    public GOA(String filepath, GraphHandlerInterface handler, boolean bp, boolean mf, boolean cc) {

        this.goCACHE = new HashSet<String>();
        this.biologicalProcess = bp;
        this.molecularFunction = mf;
        this.cellularComponent = cc;
        this.filePath = filepath;
        this.handler = handler;
    }

    public static void main(String[] args) {
//        String graphFile = "/Users/joemullen/Desktop/Datasets/GOA/gene_association.goa_human";
//        Neo4J backend = new Neo4J("/Users/joemullen/Documents/neotest-uniprot");
//        GOA test = new GOA(graphFile, backend, true, true, true);
//        test.run();
//        
    }

    public void run() {
        parse();
    }

    public void parse() {
        try {
            LOGGER.info("Parsing GO interactions from "
                    + filePath);

            String thisLine;
            //Open the file for reading
            int testCount = 0;
            BufferedReader br = null;
            br = new BufferedReader(new FileReader(filePath));
            //data to extract
            String dbTakenFrom = null;
            String uniProtID = null;
            String goTermID = null;
            String goType = null;
            //nodes
            SSIPNode prot = null;
            SSIPNode go = null;

            while ((thisLine = br.readLine()) != null) {

                if (!thisLine.startsWith("!")) {
                    String[] split = thisLine.split("\t");
                    dbTakenFrom = split[0];
                    uniProtID = split[1];
                    goTermID = split[4];
                    goType = split[8];

                    if (goType.equals("P") && biologicalProcess) {
                        //molecular function
                        //  go = new SSIPNode(DReNInMetadata.NodeType.BIOLOGICAL_PROCESS.toString(), goTermID);
                        go = new SSIPNode(DReNInMetadata.NodeType.BIOLOGICAL_PROCESS.getText(), goTermID);
                        prot = new SSIPNode("Protein", uniProtID);
                        prot.addRelation(go, new SSIPRelationType(SSIPRelationType.RelTypes.PART_OF_BIOLOGICAL_PROCESS.toString(), 1));
                        handler.addNode(prot);
                        bp++;
                        if (!goCACHE.contains(goTermID)) {
                            handler.addNode(go);
                            goCACHE.add(goTermID);

                        }
                    }


                    if (goType.equals("F") && molecularFunction) {
                        //molecular function
                        go = new SSIPNode(DReNInMetadata.NodeType.MOLECULAR_FUNCTION.getText(), goTermID);
                        prot = new SSIPNode("Protein", uniProtID);
                        prot.addRelation(go, new SSIPRelationType(SSIPRelationType.RelTypes.HAS_MOLECULAR_FUNCTION.toString(), 1));
                        handler.addNode(prot);
                        mf++;
                        if (!goCACHE.contains(goTermID)) {
                            handler.addNode(go);
                            goCACHE.add(goTermID);

                        }
                    }


                    if (goType.equals("C") && cellularComponent == true) {
                        //cellular component
                        go = new SSIPNode(DReNInMetadata.NodeType.CELLULAR_COMPONENT.getText(), goTermID);
                        prot = new SSIPNode("Protein", uniProtID);
                        prot.addRelation(go, new SSIPRelationType(SSIPRelationType.RelTypes.LOCATED_IN_CELLULAR_COMPONENT.toString(), 1));

                        handler.addNode(prot);
                        cc++;
                        if (!goCACHE.contains(goTermID)) {
                            handler.addNode(go);
                            goCACHE.add(goTermID);

                        }
                    }

                }


            }

            LOGGER.info(
                    "Created " + bp + " BP relations");
            LOGGER.info(
                    "Created " + mf + " MF relations");
            LOGGER.info(
                    "Created " + cc + " CC relations");

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
