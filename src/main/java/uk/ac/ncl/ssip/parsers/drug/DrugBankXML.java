/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.drug;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.*;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 * @author joemullen Class parsers in an XML representation of the DrugBank
 * database. Extracts: [Nodes] drugs (both small molecules and biotech) [Nodes]
 * human targets (mainly proteins) [Interactions] drug-drug [Interactions]
 * drug-target
 */
public class DrugBankXML {
    //required

    private String filepath;
    private Scanner sc;
    private GraphHandlerInterface handler;
    private final static Logger LOGGER = Logger.getLogger(DrugBankXML.class.getName());
    private final String institute = "Drug Bank";
    //specific to this parser
    private String ELEMENT = "drug";
    private final String DRUG_NAME_XPATH = "/drug/name";
    private final String DRUG_DBID_XPATH = "/drug/drugbank-id";
    private final String DRUG_TYPE_XPATH = "/drug/@type";
    private final String DRUG_GROUPS_XPATH = "/drug/groups";
    private final String DRUG_CATEGORIES_XPATH = "/drug/categories";
    private final String DRUG_AFFORG_XPATH = "/drug/affected-organisms";
    private final String TARGET_XPATH = "/drug/targets/target";
    private final String INTERACTIONS_XPATH = "/drug/drug-interactions/drug-interaction";
    private boolean parseDrug = false;
    private boolean element = false;
    private String content = "";
    //counts
    private int parsedDrugs = 0;
    private int smallMolecule = 0;
    private int biotech = 0;
    private int nutraceuticals = 0;
    private Set tarNames = new HashSet<String>();
    private int numberOfDrugs = 0;
    private Map drugTypeCache = new HashMap<>();
    private Map drugInteractionsCache = new HashMap<>();
    //required for gephi export in Neo4J- needs to be removed
    private SSIPNode previous = null;
    private boolean initial = true;
    private int nonHuman = 0;
    int count = 0;
    private boolean getTargets;

    public DrugBankXML(String filepath, GraphHandlerInterface handler, boolean targets) {
        this.filepath = filepath;
        this.handler = handler;
        this.getTargets = targets;
    }

    public enum NodeTypes {

        Small_Molecule("Small_Molecule"),
        Bio_Tech("Bio_Tech"),
        Protein("Protein");
        private final String type;

        private NodeTypes(String s) {
            type = s;
        }

        public boolean equalsName(String otherName) {
            return (otherName == null) ? false : type.equals(otherName);
        }

        public String toString() {
            return type;

        }
    }

    public static void main(String[] args) throws Exception {
    }

    /**
     * Parse in the XML database as a stream
     *
     * @param filestream
     */
    public void run() {
        BufferedReader br = null;

        try {
            String line;
            br = new BufferedReader(new FileReader(filepath));
            while ((line = br.readLine()) != null) {

                if (line.equals("</drug>")) {
                    content = content + line;
                    Document drugDoc = getXML(content);
                    count++;
                    try {
                        getDrugs(drugDoc);
                    } catch (XPathExpressionException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                    content = "";
                    parseDrug = false;
                }
                if (parseDrug) {
                    content = content + line;
                }
                if (line.startsWith("<drug ")) {
                    content = line;
                    parseDrug = true;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        
        //add the drug drug interactions
        addDrugInteractions();

        LOGGER.info("Parsed " + smallMolecule + " small molecules");
        LOGGER.info("Parsed " + biotech + " biotech");
        LOGGER.info("Parsed " + nutraceuticals + " nutraceuticals");
        LOGGER.info("Did not parse " + nonHuman + " drugs as they are not affective against humans");
        LOGGER.info("Parsed " + tarNames.size() + " unique targets");
    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     */
    public Document getXML(String convert) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            try {
                Document document = builder.parse(new InputSource(new StringReader(convert)));
                return document;
            } catch (SAXException | IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (ParserConfigurationException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    /**
     * Retrieve all the required information from the XML document
     *
     * @param document
     * @throws XPathExpressionException
     */
    public void getDrugs(Document document) throws XPathExpressionException {
        //initial node for Neo4J
        SSIPNode initialNode = null;
        NodeList nodes = document.getChildNodes();
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        //initialise an empty SSIP Node
        SSIPNode sn = null;

        //drug attributes to be extracted     
        String dbID = null;
        String name = null;
        Set<String> accessions = null;
        String type = null;
        Set<String> groups = null;
        Set<String> categories = null;
        //not an attribute just a check- only want drugs that affect humans
        Set<String> affectedOrganisms = null;
        boolean affectsHumans = false;
        boolean node = false;
        //cache the drug type


        dbID = (String) xpath.evaluate(DRUG_DBID_XPATH, document, XPathConstants.STRING);
        type = (String) xpath.evaluate(DRUG_TYPE_XPATH, document, XPathConstants.STRING);
        name = (String) xpath.evaluate(DRUG_NAME_XPATH, document, XPathConstants.STRING);


        //get all drug groups
        NodeList groupsNodes = (NodeList) xpath.evaluate(DRUG_GROUPS_XPATH, document, XPathConstants.NODESET);
        groups = getAttributeFromNodeList(groupsNodes, "group", xpath);

        //get all drug categories
        NodeList categoriesNodes = (NodeList) xpath.evaluate(DRUG_CATEGORIES_XPATH, document, XPathConstants.NODESET);
        categories = getAttributeFromNodeList(categoriesNodes, "category", xpath);

        //get all affected organisms- we are only concerned with those that affect humans
        NodeList affectedOrganismNodes = (NodeList) xpath.evaluate(DRUG_AFFORG_XPATH, document, XPathConstants.NODESET);
        affectedOrganisms = getAttributeFromNodeList(affectedOrganismNodes, "affected-organism", xpath);

        if (affectedOrganisms.contains("Humans and other mammals")) {
            affectsHumans = true;
        }

        if (!affectedOrganisms.contains("Humans and other mammals")) {
            nonHuman++;
        }

        if (type.equals("biotech")) { //&& affectsHumans == true) {
            type = NodeTypes.Bio_Tech.type.toString();
            sn = new SSIPNode(type, name);
            biotech++;
            node = true;
        }
        if (type.equals("small molecule")) {// && affectsHumans == true) {
            type = NodeTypes.Small_Molecule.type.toString();
            sn = new SSIPNode(type, name);
            smallMolecule++;
            node = true;
        }

        Set<String> targetIds = null;
        if (getTargets) {
            //get all targets
            NodeList targets = (NodeList) xpath.evaluate(TARGET_XPATH, document, XPathConstants.NODESET);
            targetIds = getTargets(xpath, targets);
        }

        //get all interactions
        NodeList interactions = (NodeList) xpath.evaluate(INTERACTIONS_XPATH, document, XPathConstants.NODESET);


        //submit node
        if (node) {
            //    get drug interacitons and cache
            Set<String> drugInteractions = getDrugInteractions(xpath, interactions);
            //      if we have any targets create corresponding node
            if (targetIds != null && !targetIds.isEmpty()) {
                for (String protein : targetIds) {
                    // System.out.println(protein);
                    SSIPNode proteinNode = new SSIPNode(NodeTypes.Protein.type, protein);
                    handler.addNode(proteinNode);
                    sn.addRelation(proteinNode, new SSIPRelationType(SSIPRelationType.RelTypes.BINDS_TO.toString(), 1));
                }
            }
            
            


            //druginteraction relations
            drugTypeCache.put(name, type);
            drugInteractionsCache.put(name, drugInteractions);

            //add properties
            sn.addProperty("DBID", dbID);
         //   sn.addProperty("Groups", groups);
           // sn.addProperty("Categories", categories);

            handler.addNode(sn);
            previous = sn;
            numberOfDrugs++;

        }
    }

    /**
     * Create target nodes and interactions from the drug to these
     *
     * @param xpath
     * @param targets
     */
    public Set<String> getTargets(XPath xpath, NodeList targets) {

        Set<String> targetIDs = new HashSet<String>();
        for (int i = 0; i < targets.getLength(); i++) {
            //target attributes: we only need all this info if we haven't already got our proteins from another source
            //such as UNIRPOT_SWISSPROT
            String id = null;
            String SwissProtpolypeptideID = null;
            String tarName = null;
            String aminoAcidSequence = null;
            String geneSequence = null;
            String ncbiTaxonomy = null;

            try {
                id = (String) xpath.evaluate("id", targets.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }


            try {
                String polypepSource = (String) xpath.evaluate("polypeptide/@source", targets.item(i), XPathConstants.STRING);
                if (polypepSource.equals("Swiss-Prot")) {
                    SwissProtpolypeptideID = (String) xpath.evaluate("polypeptide/@id", targets.item(i), XPathConstants.STRING);
                }

            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
            try {
                tarName = (String) xpath.evaluate("name", targets.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
            try {
                aminoAcidSequence = (String) xpath.evaluate("polypeptide/amino-acid-sequence", targets.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
            try {
                geneSequence = (String) xpath.evaluate("polypeptide/gene-sequence", targets.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
            try {
                ncbiTaxonomy = (String) xpath.evaluate("organism/@ncbi-taxonomy-id", targets.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

            if (SwissProtpolypeptideID != null) {
                targetIDs.add(SwissProtpolypeptideID);
            }

            tarNames.add(id);
        }
        return targetIDs;
    }

    /**
     * Create relations between the drug and all drugs it interacts with
     *
     * @param xpath
     * @param interactions
     */
    public Set<String> getDrugInteractions(XPath xpath, NodeList interactions) {

        Set<String> drugIDs = new HashSet<String>();
        //interaction attributes
        String todbID = null;
        String todrugName = null;
        String interactionDescription = null;

        for (int i = 0; i < interactions.getLength(); i++) {

            try {
                todbID = (String) xpath.evaluate("drugbank-id", interactions.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

            try {
                todrugName = (String) xpath.evaluate("name", interactions.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

            try {
                interactionDescription = (String) xpath.evaluate("description", interactions.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

            drugIDs.add(todrugName);
        }

        return drugIDs;
    }

    /**
     * Returns the set of attributes from a nodeList, when it is the same tag to
     * be used each time.
     *
     * @param nodes
     * @param tag
     * @param xpath
     * @return
     */
    public Set<String> getAttributeFromNodeList(NodeList nodes, String tag, XPath xpath) {
        Set<String> attributes = new HashSet<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            try {
                attributes.add((String) xpath.evaluate(tag, nodes.item(i), XPathConstants.STRING));
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        return attributes;
    }

    /**
     *Adds drug drug interactions to the graph
     */
    public void addDrugInteractions() {
        for (Object drug : drugInteractionsCache.keySet()) {
            String id = (String) drug;
            String type = (String) drugTypeCache.get(id);
            //  System.out.println("type:" + type);
            SSIPNode from = new SSIPNode(type, id);
            // System.out.println("FROM type:" + type+ " id:" + id);
            Set<String> toDrugs = null;
            if (drugInteractionsCache.containsKey(drug)) {
                toDrugs = (Set) drugInteractionsCache.get(drug);
                for (String to : toDrugs) {
                    if (drugTypeCache.containsKey(to)) {
                        String typeTo = (String) drugTypeCache.get(to);
                        //if it is not in the map then we didn't add it to the graph
                        if (typeTo != null) {
                            //System.out.println("TO type:" + typeTo+ " id:" + to);
                            from.addRelation(new SSIPNode(typeTo, to), new SSIPRelationType(SSIPRelationType.RelTypes.INTERACTS_WITH.toString(), 1));
                        }
                    }
                }
                handler.addNode(from);
            }
        }
    }
}