/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.disease;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.*;
import org.w3c.dom.NodeList;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author joemullen
 *
 * This parser was developed to parse all interactions between rare diseases and
 * their associated genes from ORPHANET. Dataset is available for download from
 * http://www.orphadata.org/cgi-bin/inc/product6.inc.php
 *
 */
public class OrphanetRelations {

    private GraphHandlerInterface handler;
    private String filepath;
    private Scanner sc;
    private String ELEMENT = "Disorder";
    private String NAMESPACE = "";
    private String characters;
    private boolean element = false;
    private String GENEID = "UNIPROTKB/SWISSPROT";
    private final String DISEASE_XPATH = "/Disorder/OrphaNumber";
    private final String GENES_XPATH = "/Disorder/DisorderGeneAssociationList/DisorderGeneAssociation/Gene";
    private final String GENE_NAMES_EXREF_XPATH = GENES_XPATH + "/ExternalReferenceList/ExternalReference";
    private final String GENE_NAMES_EXSOURCE_XPATH = GENE_NAMES_EXREF_XPATH + "/Source";
    private final static Logger LOGGER = Logger.getLogger(OrphanetRelations.class.getName());
    private int successfulRelations = 0;
    private int unknownRelations = 0;

    public static void main(String[] args) {
//        File xmlDB = new File("/Users/joemullen/Desktop/Datasets/ORPHANET/diseaseGeneInteraction.xml");
//        OrphanetRelations uf = new OrphanetRelations(xmlDB);
//        uf.streamFile();
    }

    public OrphanetRelations(String filepath,GraphHandlerInterface handler ) {
        this.filepath = filepath;
        this.handler = handler;
        //this.GOTermCACHE = new HashSet<String>();
    }

    public void run() {

        StringWriter buf = new StringWriter(1024);
        element = false;
        try {
            LOGGER.info("Parsing Orphanet disease-gene file "
                    + filepath);

            //Setup XML 
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory
                    .createXMLEventReader(new FileReader(filepath));

            while (eventReader.hasNext()) {
                // Get the next event
                XMLEvent event = eventReader.nextEvent();
                //if we have passed the start tag append the string with the event
                if (element) {
                    event.writeAsEncodedUnicode(buf);

                }
                //if we reach the endelement create XML document and extract data
                if (event.isEndElement()) {
                    EndElement e = event.asEndElement();
                    String name = e.getName().toString();
                    if (name.equals(ELEMENT)) {
                        element = false;
                        // System.out.println(buf.toString());
                        Document doc = getXML(buf.toString());
                        extractData(doc);
                        // System.out.println("-----------------");
                        buf = new StringWriter(1024);
                    }
                }
                //if we reach the start element start wrinting to StringWriter
                if (event.isStartElement()) {
                    StartElement s = event.asStartElement();
                    String name = s.getName().toString();
                    if (name.equals(ELEMENT)) {
                        element = true;
                        event.writeAsEncodedUnicode(buf);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        LOGGER.info("Successfully parsed " + successfulRelations + " disease-gene relations");
        LOGGER.info("Failed to parse " + unknownRelations + " disease-gene relations due to unkown " + GENEID + " ids");


    }

    /**
     * Takes and XML element and extracts all concept classes and relation types
     * required using XMLPath
     *
     * @param document
     */
    public void extractData(Document document) {
        //create an XpathFactory to mine the document
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        String disease = getOrphanetDisease(document, xpath);
        Set<String> associatedGenes = new HashSet<>();
        try {
            associatedGenes = getAssociatedGeneSymbols(document, xpath);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }

        SSIPNode rareDisease = new SSIPNode(DReNInMetadata.NodeType.RARE_DISEASE.getText(), "Orphanet:"+disease);
        for (String gen: associatedGenes){
        
            SSIPRelationType rel = new SSIPRelationType(SSIPRelationType.RelTypes.INVOLVED_IN.toString(), 1);
            SSIPNode gene = new SSIPNode(DReNInMetadata.NodeType.GENE.getText(), gen);
            gene.addRelation(rareDisease, rel);
            handler.addNode(gene);
            
        
        }
        
        handler.addNode(rareDisease);

    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     */
    public Document getXML(String convert) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            try {
                Document document = builder.parse(new InputSource(new StringReader(convert)));
                return document;
            } catch (SAXException | IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (ParserConfigurationException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    /**
     * Retrieve the diseases ID
     *
     * @param doc
     * @param xpath
     * @return
     */
    public String getOrphanetDisease(Document doc, XPath xpath) {
        String id = null;
        try {
            id = (String) xpath.evaluate(DISEASE_XPATH, doc, XPathConstants.STRING);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
        return id;
    }

    /**
     * Returns the Gene IDs for all associated genes
     *
     * @param doc
     * @param xpath
     * @return
     * @throws XPathExpressionException
     */
    public Set<String> getAssociatedGeneSymbols(Document doc, XPath xpath) throws XPathExpressionException {

        Set<String> associatedGenes = new HashSet<String>();
        //get all associated genes
        NodeList genes = (NodeList) xpath.evaluate(GENES_XPATH, doc, XPathConstants.NODESET);

        for (int i = 0; i < genes.getLength(); i++) {
            String name = null;
            String geneSymbol = null;
            try {
                //NodeList genesIDs = (NodeList) xpath.evaluate(GENE_NAMES_EXREF_XPATH, genes.item(i), XPathConstants.NODESET);
                associatedGenes.add((String)xpath.evaluate("Symbol", genes.item(i), XPathConstants.STRING));
                //add the Id to the set of associated genes
                //associatedGenes.add(getAssociatedGeneSources(genesIDs, xpath));
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

           // System.out.println("symbol: " + geneSymbol);
        }
        return associatedGenes;
    }

    /**
     * Method returns the id associated to the source we wish to use. If there
     * is no such ID associated to the gene then we return "unknown".
     *
     * @param genes
     * @param xpath
     * @return
     * @throws XPathExpressionException
     */
//    public String getAssociatedGeneSources(NodeList genes, XPath xpath) throws XPathExpressionException {
//
//        String geneIDName = null;
//        boolean geneID = false;
//        //look at all the data sources for each gene external reference
//        for (int i = 0; i < genes.getLength(); i++) {
//            String source = null;
//            try {
//                source = (String) xpath.evaluate("Source", genes.item(i), XPathConstants.STRING);
//                //if the source mathces the source type we want we use this
//                if (source.equals(GENEID)) {
//                    geneIDName =  (String) xpath.evaluate("Reference", genes.item(i), XPathConstants.STRING);;
//                    geneID = true;
//                    successfulRelations++;
//                }
//            } catch (XPathExpressionException ex) {
//                Exceptions.printStackTrace(ex);
//            }
//        }
//        //if there is no associated ID for the sourcetype we are interested in then we retun an unknown string
//        if (geneID == false) {
//            geneIDName = ("unknown");
//            unknownRelations++;
//        }
//        //return the name
//        return geneIDName;
//    }
}
