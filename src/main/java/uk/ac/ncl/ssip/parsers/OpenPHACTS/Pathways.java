/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.OpenPHACTS;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import uk.ac.ncl.ssip.metadata.SSIPNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Pathways {

    private CallOpenPHACTS call;
    private Mapping freetext;
    private final String TISSUE_XPATH = "/result/items/item";
    //private final String TAR_COMPONENTS = "hasAssay/hasTarget/hasTargetComponent";
    String target = "http://purl.uniprot.org/uniprot/P55795";
    String compound = "http://www.conceptwiki.org/concept/83931753-9e3f-4e90-b104-e3bcd0b4d833";

    public Pathways(CallOpenPHACTS call) {
        this.call = call;
    }

    public static void main(String[] args) {

//        Pathways c = new Pathways();
//        c.requestInfo();

    }

    public Set<SSIPNode> getPathwayNodeTARGET(String URI) {

        Document doc = null;
        try {
            doc = call.callXMLMethod(getPathwayForTargetRequest());
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        return extractPathwayData(doc);

    }

    public Set<SSIPNode> getPathwayNodeCOMP(String URI) {


        Document doc = null;
        try {
            doc = call.callXMLMethod(getPathwayForCompoundRequest());
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        return extractPathwayData(doc);

    }

    public Set<SSIPNode> extractPathwayData(Document doc) {

        Set<SSIPNode> paths = new HashSet<SSIPNode>();

        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        //ssip node
        SSIPNode path = null;

        NodeList entry = null;
        //variables
        String href = null;
        String description = null;
        String organism = null;

        try {
            entry = (NodeList) xpath.evaluate(TISSUE_XPATH, doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
        for (int i = 0; i < entry.getLength(); i++) {
            System.out.println("---------------");
            try {
                href = (String) xpath.evaluate("@href", entry.item(i), XPathConstants.STRING);
                description = (String) xpath.evaluate("description", entry.item(i), XPathConstants.STRING);
                organism = (String) xpath.evaluate("pathway_organism/label", entry.item(i), XPathConstants.STRING);

            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
            System.out.println("href: " + href);
            System.out.println("label: " + description);
            System.out.println("organism: " + organism);

            path = new SSIPNode("Pathway", href);
            paths.add(path);

        }
        return paths;
    }

    public String getPathwayInformation() {
        //need to complete
        return null;

    }

    public String getPathwayForTargetRequest() throws UnsupportedEncodingException {
        String urlCall = call.getOpenPHACTSURL() + "/pathways/byTarget?uri=" + URLEncoder.encode(target, "UTF-8") + "&" + call.getAppID() + call.getAppKey() + call.getPageSize() + call.getReturnFormat();
        return urlCall;
    }

    public String getPathwayForCompoundRequest() throws UnsupportedEncodingException {
        String urlCall = call.getOpenPHACTSURL() + "/pathways/byCompound?uri=" + URLEncoder.encode(compound, "UTF-8") + "&" + call.getAppID() + call.getAppKey() + call.getPageSize() + call.getReturnFormat();
        return urlCall;
    }
}
