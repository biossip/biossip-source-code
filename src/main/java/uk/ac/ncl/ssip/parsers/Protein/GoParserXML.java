/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.Protein;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
import java.util.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;

/**
 *
 * @author Joseph Mullen http://intbio.ncl.ac.uk/?people=joe-mullen
 *
 */
public class GoParserXML {

    //private Set<String> GOTermCACHE;
    //required
    private String filepath;
    private Scanner sc;
    private GraphHandlerInterface handler;
    private final static Logger LOGGER = Logger.getLogger(GoParserXML.class.getName());
    private final String institute = "Drug Bank";
    private String ELEMENT = "term";
    private String characters;
    boolean element = false;
    //Xpath queries
    private String GO_ID = "term/id";
    private String GO_NAME = "term/name";
    private String GO_NAMESPACE = "term/namespace";
    private String GO_RELS = "term/is_a";
    private int goTerms = 0;
    private int count = 0;

    public static void main(String[] args) {
        String xmlDB = "/Users/joemullen/Dropbox/SSIP/test_go_daily-termdb.obo-xml";
        //String graphFile = "/Users/joemullen/Desktop/gephi_export_trav_subgraph.gexf";
        Neo4J backend = new Neo4J("/Users/joemullen/Documents/neotest-GO");

        backend.setCommitSize(10000);
        backend.initialiseDatabaseConnection();
        /**
         * TODO Need to index on the geneIDs
         */
        backend.createIndex();
        backend.createIndex("GOID");
        backend.syncIndexes(10);
        GoParserXML gp = new GoParserXML(xmlDB, backend);
        gp.run();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();

    }

    public GoParserXML(String filepath, GraphHandlerInterface handler) {
        this.filepath = filepath;
        this.handler = handler;
        //this.GOTermCACHE = new HashSet<String>();
    }

    /**
     * parse Proteins, GO terms and KO terms from a UniProt_sprot.xml
     *
     * @throws IOException
     */
    public void run() {

        StringWriter buf = new StringWriter(1024);
        element = false;
        try {
            LOGGER.info("Parsing uniprot file "
                    + filepath);

            //Setup XML 
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory
                    .createXMLEventReader(new FileReader(filepath));

            int count = 0;

            while (eventReader.hasNext()) {


                count++;
                // Get the next event
                XMLEvent event = eventReader.nextEvent();
                //if we have passed the start tag append the string with the event
                if (element) {
                    event.writeAsEncodedUnicode(buf);

                }

                //if we reach the endelement create XML document and extract data
                if (event.isEndElement()) {
                    EndElement e = event.asEndElement();
                    String name = e.getName().toString();
                    if (name.equals(ELEMENT)) {
                        element = false;

                        Document doc = getXML(buf.toString());
                        extractData(doc, buf.toString());
                        // System.out.println("-----------------");
                        // System.out.println("-----------------");
                        buf = new StringWriter(1024);
                    }
                }
                //if we reach the start element start wrinting to StringWriter
                if (event.isStartElement()) {
                    StartElement s = event.asStartElement();
                    String name = s.getName().toString();
                    if (name.equals(ELEMENT)) {
                        element = true;
                        event.writeAsEncodedUnicode(buf);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  GOTermCACHE.clear();

        LOGGER.info("Parsed " + goTerms + " GO terms");


    }

    /**
     * Takes and XML element and extracts all concept classes and relation types
     * required using XMLPath
     *
     * @param document
     */
    public void extractData(Document document, String data) {

        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();

        //initialize nodes
        SSIPNode GOnode = null;

        //testing GO properties
        String id = null;
        String name = null;
        String type = null;
        Set<String> is_a = new HashSet<String>();

        try {
            id = (String) xpath.evaluate(GO_ID, document, XPathConstants.STRING);
            name = (String) xpath.evaluate(GO_NAME, document, XPathConstants.STRING);
            type = (String) xpath.evaluate(GO_NAMESPACE, document, XPathConstants.STRING);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
        try {
            NodeList isRels = (NodeList) xpath.evaluate(GO_RELS, document, XPathConstants.NODESET);
            is_a = getRels(isRels, xpath);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }

        if (type.equals("biological_process")) {
            GOnode = new SSIPNode(DReNInMetadata.NodeType.BIOLOGICAL_PROCESS.getText(), name);
        }
        if (type.equals("molecular_function")) {
            GOnode = new SSIPNode(DReNInMetadata.NodeType.MOLECULAR_FUNCTION.getText(), name);
        }
        if (type.equals("cellular_component")) {
            GOnode = new SSIPNode(DReNInMetadata.NodeType.CELLULAR_COMPONENT.getText(), name);
        }

        GOnode.addProperty("GOID", id);

        for (String rel : is_a) {
            SSIPNode to = new SSIPNode("GOID", rel);
            GOnode.addRelation(to, new SSIPRelationType(DReNInMetadata.RelTypes.IS_A.toString()));

        }

//        System.out.println("ID "+ id);
//        System.out.println("Name "+ name);
//        System.out.println("type "+ type);
//        System.out.println("rels "+ is_a.toString());

        handler.addNode(GOnode);
        goTerms++;


    }

    public Set<String> getRels(NodeList nodes, XPath xpath) throws XPathExpressionException {
        Set<String> rels = new HashSet<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            rels.add(nodes.item(i).getTextContent());
        }
        return rels;
    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     */
    public Document getXML(String convert) {
        //System.out.println("-----------------");
        //System.out.println(convert);
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            try {
                Document document = builder.parse(new InputSource(new StringReader(convert)));
                return document;
            } catch (SAXException | IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (ParserConfigurationException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }
}
