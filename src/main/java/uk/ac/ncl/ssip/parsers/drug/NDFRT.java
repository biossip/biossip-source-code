/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.drug;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author joemullen
 */
public class NDFRT extends RequestWriterNDFRT {

    private format FORMAT = format.xml;
    private String NDFRTURL = "http://rxnav.nlm.nih.gov/REST/Ndfrt";
    private String PHARMGKBDRUGIDs = "/Users/joemullen/Desktop/Datasets/PharmGKB/drugs/drugs.tsv";
    private final static Logger LOGGER = Logger.getLogger(NDFRT.class.getName());
    private Map<String, String> NUI2DB = new HashMap<String, String>();
    private String filepath;
    private GraphHandlerInterface handler;

    /**
     * WARNING- takes about an hour to run.
     *
     * @param filepath
     * @param handler
     */
    //can also get interacting drugs from NDFRT
    //this is quite good: http://www.javacodegeeks.com/2012/09/simple-rest-client-in-java.html
    public NDFRT(String filepath, GraphHandlerInterface handler) {
        this.filepath = filepath;
        this.handler = handler;

    }

    public void run() {
        try {
            String request = getAllKindNUIs("drug_kind", NDFRTURL);
            Document doc = callXMLMethod(request);
            Set<String> NUIs = getDrugOrDiseaseNUIs(doc);
            setMappings();
            // N0000022115 sildenafil
            for (String conceptNUIs : NUIs) {
                if (NUI2DB.containsKey(conceptNUIs)) {
                    //System.out.println("getting here??");
                    String dbDrug = NUI2DB.get(conceptNUIs);
                    String mayTreatCall = getMayTreat(conceptNUIs, NDFRTURL);
                    Document mayTreat = callXMLMethod(mayTreatCall);
                    Set<String> mayTreatNUIs = getDrugOrDiseaseNUIs(mayTreat);
                    Set<String> mayTUMLS = new HashSet<String>();
                    //get the disease UMLS
                    for (String nui : mayTreatNUIs) {

                        String re = getDiseaseUMLS(nui, NDFRTURL);
                        Document disUMLS = callXMLMethod(re);
                        mayTUMLS.add(extractProperty(disUMLS, "UMLS_CUI"));
                    }


                    String mayPreventCall = getMayPrevent(conceptNUIs, NDFRTURL);
                    Document mayPrevent = callXMLMethod(mayPreventCall);
                    Set<String> mayPrevNUIs = getDrugOrDiseaseNUIs(mayPrevent);
                    Set<String> mayPUMLS = new HashSet<String>();
                    for (String nui : mayPrevNUIs) {
                        String re = getDiseaseUMLS(nui, NDFRTURL);
                        Document disUMLS = callXMLMethod(re);
                        mayPUMLS.add(extractProperty(disUMLS, "UMLS_CUI"));
                    }

                    SSIPNode drug = new SSIPNode("DBID", dbDrug);

                    for (String umls : mayPUMLS) {
                        SSIPRelationType re = new SSIPRelationType(SSIPRelationType.RelTypes.MAY_PREVENT.toString());
                        SSIPNode dis = new SSIPNode("UMLS_CUI", umls);
                        drug.addRelation(dis, re);
                    }

                    for (String umls : mayTUMLS) {
                        SSIPRelationType re = new SSIPRelationType(SSIPRelationType.RelTypes.MAY_TREAT.toString());
                        SSIPNode dis = new SSIPNode("UMLS_CUI", umls);
                        drug.addRelation(dis, re);
                    }


                    handler.addNode(drug);


                }
            }
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void setMappings() {
        BufferedReader br = null;
        Map<String, String> NUI2DBlocal = new HashMap<String, String>();
        String thisLine;
        try {

            //Open the file for reading
            br = new BufferedReader(new FileReader(PHARMGKBDRUGIDs));
            try {
                while ((thisLine = br.readLine()) != null) {
                    String db = null;
                    String ndfrt = null;
                    String[] split = thisLine.split("\t");

                    //get the DrugBank ID which is a crossRef
                    String crossRefs = split[6];
                    // System.out.println("----------------");
                    // System.out.println(crossRefs);
                    String[] split2 = crossRefs.split(",");
                    for (int i = 0; i < split2.length; i++) {
                        String[] split3 = split2[i].split(":");
                        if (split3[0].equals("drugBank")) {
                            db = split3[1];
                            // System.out.println(db);
                        }
                    }

                    //get the NDFRT id, which is in the external vocab
                    if (split.length > 9) {
                        String externVocab = split[9];
                        //   System.out.println(externVocab);
                        split2 = externVocab.split(",");
                        for (int i = 0; i < split2.length; i++) {
                            String[] split3 = split2[i].split(":");
                            if (split3[0].equals("NDFRT")) {
                                ndfrt = split3[1];
                            }
                        }

                        if (db != null && ndfrt != null) {
                            // System.out.println("added: "+ ndfrt+ "  "+ db);
                            NUI2DBlocal.put(ndfrt.substring(0, 11), db);
                        }

                    }

                }
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (FileNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        this.NUI2DB = NUI2DBlocal;

    }

    public Set<String> getDrugOrDiseaseNUIs(Document doc) {
        Set<String> nuis = new HashSet<String>();
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        NodeList concepts = null;
        String conceptNUI = null;
        try {
            concepts = (NodeList) xpath.evaluate("/ndfrtdata/groupConcepts/concept", doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }

        //this is where we are specifying a limit to the number of diseases
        if (concepts.getLength() > 0) {
            for (int i = 0; i < concepts.getLength(); i++) {
                try {
                    conceptNUI = (String) xpath.evaluate("conceptNui", concepts.item(i), XPathConstants.STRING);
                    nuis.add(conceptNUI);
                    // System.out.println("Extracted NUI: " + conceptNUI);
                } catch (XPathExpressionException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }



        return nuis;
    }

    public String extractProperty(Document doc, String property) {
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        String umls = null;
        NodeList properties = null;
        try {
            properties = (NodeList) xpath.evaluate("/ndfrtdata/groupProperties/property", doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }

        String propertyName = null;
        String propertyValue = null;
        for (int i = 0; i < properties.getLength(); i++) {
            if (propertyName != null) {
                break;
            }
            try {
                propertyName = (String) xpath.evaluate("propertyName", properties.item(i), XPathConstants.STRING);
                if (propertyName.equals(property)) {
                    propertyValue = (String) xpath.evaluate("propertyValue", properties.item(i), XPathConstants.STRING);
                }
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        return propertyValue;
    }

    public Document callXMLMethod(String request) {
        Document doc = null;
        try {
            HttpClient ndfrtclient = HttpClientBuilder.create().build();
            HttpGet getMethod = new HttpGet(request);
            getMethod.addHeader("accept", FORMAT.toString());
            LOGGER.info("Executing request: " + "\n" + getMethod.getURI());
            HttpResponse reponseBody = null;
            try {
                reponseBody = ndfrtclient.execute(getMethod);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
            HttpEntity entity = reponseBody.getEntity();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = null;
            try {
                builder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                Exceptions.printStackTrace(ex);
            }
            doc = builder.parse(entity.getContent());
            if (doc == null) {
                System.out.println("NOT working");
            }
            // System.out.println(doc.toString());
            getMethod.releaseConnection();
            LOGGER.info("Connection Released");
        } catch (SAXException | IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return doc;
    }

    private enum format {

        xml, json, plainText;

        @Override
        public String toString() {
            String f = super.toString();
            if (f.equals("json")) {
                return "application/json";
            } else if (f.equals("plainText")) {
                return "text/plain";

            } else if (f.equals("xml")) {

                return "application/xml";
            } else {
                return null;
            }
        }
    }
}
