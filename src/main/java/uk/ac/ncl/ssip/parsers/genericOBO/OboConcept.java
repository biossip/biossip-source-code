/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.genericOBO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author joemullen
 */
public class OboConcept {

    private String id = "";
    private List<String> alt_ids = new ArrayList<String>();
    private String name = "";
    private String namespace = "";
    private HashMap<String, HashSet<String>> synonyms = new HashMap<String, HashSet<String>>();
    private String definition = "";
    private List<List<String>> relations = new ArrayList<List<String>>();
    private List<String> refs = new ArrayList<String>();
    private String[] definitionRefs;
    /**
     * Defines that this synonym is exact, see OBO/GO documentation
     */
    public static String exactSynonym = "exact_synonym";
    /**
     * Normal synonym.
     */
    public static String normalSynonym = "synonym";
    // few more classe see GO/OBO documentation
    public static String broadSynonym = "broad_synonym";
    public static String narrowlSynonym = "narrow_synonym";
    public static String relatedSynonym = "related_synonym";

    public List<String> getAlt_ids() {
        return alt_ids;
    }

    public void addAlt_ids(String alt_id) {
        this.alt_ids.add(alt_id);
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public List<String> getRefs() {
        return refs;
    }

    public void addRef(String ref) {
        this.refs.add(ref);
    }

    public List<List<String>> getRelations() {
        return relations;
    }

    public void addRelation(List<String> relations) {
        this.relations.add(relations);
    }

    public void addSynonym(String type, String synonym) {

        HashSet<String> existingSet = this.synonyms.get(type);
        if (existingSet == null) {
            HashSet<String> set = new HashSet<String>();
            set.add(synonym);
            this.synonyms.put(type, set);
        } else {
            existingSet.add(synonym);
        }
    }

    public String[] getDefinitionRefs() {
        return definitionRefs;
    }

    public void setDefinitionRefs(String[] definitionRefs) {
        this.definitionRefs = definitionRefs;
    }

    public HashMap<String, HashSet<String>> getSynonyms() {
        return synonyms;
    }

    public HashSet<String> getSynonymsOfType(String type) {
        return synonyms.get(type);
    }

    public SSIPNode getDiseaseOntologyNode() {
        SSIPNode doOnt = null;
        doOnt = new SSIPNode(DReNInMetadata.NodeType.DISEASE.getText(), id);
        //we have synonyms- what to do with them??
        // System.out.println("synonyms: "+ synonyms.toString());
        doOnt.addProperty("Name", name);
        int UMLS = 1;
        if (!refs.isEmpty()) {
            for (String ref : refs) {
                String[] split = ref.split(":");
                if (split[0].equals("UMLS_CUI")) {
                    doOnt.addProperty("UMLS_CUI"+UMLS, split[1]);
                    UMLS++;
                }

            }

            doOnt.addProperty("Refs", refs.toString());
        }


        // System.out.println("UMLSs: " + UMLS);

        for (List<String> lis : relations) {
            int count = 0;
            String type = null;
            String to = null;
            for (String s : lis) {
                if (count == 0) {
                    type = s;
                }
                if (count == 1) {
                    to = s;
                }
                count++;
            }
            if (type.equals("is_a")) {
                SSIPRelationType is = new SSIPRelationType(SSIPRelationType.RelTypes.IS_A_DISEASE.toString(), 1);
                doOnt.addRelation(new SSIPNode(DReNInMetadata.NodeType.DISEASE.getText(), to), is);

            }
            if (!type.equals("is_a")) {
                System.out.println("relation in do of type: " + type + " not included");
            }
        }
        return doOnt;
    }

    public SSIPNode getOrphanetNode() {

        SSIPNode orph = null;

        orph = new SSIPNode(DReNInMetadata.NodeType.RARE_DISEASE.getText(), id);
        orph.addProperty("Name", name);
        int UMLS = 1;
        if (!refs.isEmpty()) {

            for (String ref : refs) {
                String[] split = ref.split(":");
                if (split[0].equals("UMLS")) {
                    orph.addProperty("UMLS_CUI"+UMLS, split[1]);
                    //   System.out.println(split[1]);
                    UMLS++;
                }
            }

            orph.addProperty("Refs", refs.toString());
        }

        for (List<String> lis : relations) {
            int count = 0;
            String type = null;
            String to = null;
            for (String s : lis) {
                if (count == 0) {
                    type = s;
                }
                if (count == 1) {
                    to = s;
                }
                count++;
            }
            if (type.equals("is_a")) {
                SSIPRelationType is = new SSIPRelationType(SSIPRelationType.RelTypes.IS_A_RARE_DISEASE.toString(), 1);
                orph.addRelation(new SSIPNode(DReNInMetadata.NodeType.RARE_DISEASE.getText(), to), is);

            }

            if (type.equals("part_of")) {
                SSIPRelationType is = new SSIPRelationType(SSIPRelationType.RelTypes.PART_OF_RARE_DISEASE.toString(), 1);
                orph.addRelation(new SSIPNode(DReNInMetadata.NodeType.RARE_DISEASE.getText(), to), is);

            }

        }

        return orph;
    }

    @Override
    public String toString() {

        String obo = "";
        obo = "id: " + id + "\n";
        obo = obo + "\n" + "altIds: " + alt_ids.toString();
        obo = obo + "\n" + "name: " + name;
        obo = obo + "\n" + "namespace: " + namespace;
        obo = obo + "\n" + "synonyms: " + synonyms;
        obo = obo + "\n" + "definition: " + definition;
        obo = obo + "\n" + "relations: " + relations.toString();
        obo = obo + "\n" + "refs: " + refs.toString();
//        obo = obo + "\n" + "definitionRefs: " + Arrays.toString(getDefinitionRefs());
        return obo;

    }
}
