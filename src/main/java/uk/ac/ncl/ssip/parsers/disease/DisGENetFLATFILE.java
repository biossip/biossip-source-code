/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.disease;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author joemullen Just use OpenPHACTS 1. download the curated gene-disease
 * associations file from: http://www.disgenet.org/web/DisGeNET/v2.1/downloads
 * (curated_gene_disease_associations.txt)
 *
 */
public class DisGENetFLATFILE {

    private String line;
    int Number = 0;
    int count = 0;
    private Map<String, List<String>> UMLSrels = new HashMap<String, List<String>>();
    private Map<String, List<String>> OMIMSrels = new HashMap<String, List<String>>();
    private Map<String, List<String>> ALLrels = new HashMap<String, List<String>>();
    private double scoreThresh;
    private String filepath;
    private GraphHandlerInterface handler;
    private final static Logger LOGGER = Logger.getLogger(DisGENetFLATFILE.class.getName());

    public static void main(String[] args) {
//        DisGENetFLATFILE dg = new DisGENetFLATFILE(0.5);
//        dg.parse();
    }

    public DisGENetFLATFILE(double score, String filepath, GraphHandlerInterface handler) {
        this.scoreThresh = score;
        this.filepath = filepath;
        this.handler = handler;


    }

    public void run() {
        Set<String> types = new HashSet<String>();
        BufferedReader br = null;
        int test = 50;
        int count = 0;
        try {
            //String file = "/Users/joemullen/Desktop/Datasets/DiseGeNET/curated_gene_disease_associations.txt";

            //variables in file
            String geneId = null;
            String geneSymbol = null;
            String geneName = null;
            String diseaseId = null;
            String diseaseName = null;
            double score = 0.000000000;
            int NumberOfPubmeds = 0;
            String associationType = null;
            String source = null;


            String line;
            br = new BufferedReader(new FileReader(filepath));
            try {
                //skip the first line
                br.readLine();
                while ((line = br.readLine()) != null) {
                    count++;
                    String[] split = line.split("\t");
                    geneId = split[0];
                    geneSymbol = split[1];
                    geneName = split[2];
                    diseaseId = split[3];
                    if (diseaseId.startsWith("umls:")) {
                        diseaseId = diseaseId.substring(5);
                    } else {

                        System.out.println("this is not a disease UMLS");
                    }
                    diseaseName = split[4];
                    score = Double.parseDouble(split[5]);
                    NumberOfPubmeds = Integer.parseInt(split[6]);
                    associationType = split[7];
                    source = split[8];

                    if (score >= scoreThresh) {

                       // System.out.println(geneSymbol + ">>>" + diseaseId);
                        SSIPNode gene = new SSIPNode(DReNInMetadata.NodeType.GENE.getText(), geneSymbol );
                        SSIPRelationType sr = new SSIPRelationType(SSIPRelationType.RelTypes.DISGENET_INVOLVED_IN.toString());
                        SSIPNode disease = new SSIPNode("UMLS_CUI", diseaseId);
                        sr.addAttribute("Score", score);
                        sr.addAttribute("Type", associationType);
                        gene.addRelation(disease, sr);
                        handler.addNode(gene);
                        
                        types.add(associationType);
                    }
                }
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (FileNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

      //  System.out.println(types.toString());
    }

}
