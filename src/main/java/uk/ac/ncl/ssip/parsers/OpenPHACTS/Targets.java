/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.OpenPHACTS;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import uk.ac.ncl.ssip.metadata.SSIPNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Targets {

    private Set<String> targetIDs;
    private CallOpenPHACTS call;
    private Mapping freetext;
    private Map<String, String> targetCACHE;
    private final String TARG_ITEMS = "/result/primaryTopic/exactMatch/item";
    private final String UNIPROT_ORGANISM = "";

    public Targets(Set tarIds) {
        this.targetIDs = tarIds;
        this.call = new CallOpenPHACTS();
        this.targetCACHE = new HashMap<String,String>();
    }
    
    public Targets(CallOpenPHACTS call) {
        this.call = call;
        this.targetCACHE = new HashMap<String,String>();

    }

    public SSIPNode getSingleProteinNode(String URI) {
        Document doc = null;
        try {
            doc = call.callXMLMethod(tarRequest(URI));
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        return extractSingleProteinData(doc, URI);
    }
    
    public SSIPNode getProteinFamilyNode(String URI) {
        targetCACHE.put(URI, "ProteinFamily>>>"+URI);
        return new SSIPNode("ProteinFamily", URI);
    }
    
    public SSIPNode getCellLineNode(String URI) { 
        targetCACHE.put(URI, "CellLine>>>"+URI);
        return new SSIPNode("CellLine", URI);
    }
    
    public SSIPNode getProteinComplexNode(String URI){
        targetCACHE.put(URI, "ProteinComplex>>>"+URI);
        return new SSIPNode("ProteinComplex", URI);
    }
    
    public SSIPNode getTissueNode(String URI) { 
        targetCACHE.put(URI, "Tissue>>>"+URI);
        return new SSIPNode("Tissue", URI);
    }
    
    public SSIPNode getOrganismNode(String URI) {   
        targetCACHE.put(URI, "Organism>>>"+URI);
        return new SSIPNode("Organism", URI);
    }

    public SSIPNode extractSingleProteinData(Document doc, String URI) {
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();

        //target variables
        String dataSourceHref = null;
        String uniProtID = null;
        String prefLabel = null;
        String organism = null;
        //Set<String> DBtypes = new HashSet<String>();
        NodeList items = null;
        try {
            prefLabel = (String) xpath.evaluate("/result/primaryTopic/prefLabel", doc, XPathConstants.STRING);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }

        try {
            items = (NodeList) xpath.evaluate(TARG_ITEMS, doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
        for (int i = 0; i < items.getLength(); i++) {
            try {
                dataSourceHref = (String) xpath.evaluate("@href", items.item(i), XPathConstants.STRING);
                //System.out.println(dataSourceHref);
                if (dataSourceHref.startsWith("http://purl.uniprot.org/uniprot/")) {
                    //uniprot stuff
                    uniProtID = dataSourceHref.substring(32);
                    organism = (String) xpath.evaluate("organism//@href", items.item(i), XPathConstants.STRING);
                }

                if (dataSourceHref.startsWith("http://www4.wiwiss.fu-berlin.de/drugbank/resource/targets/")) {
                    //drugbank target info                   
                }
                if (dataSourceHref.startsWith("http://rdf.ebi.ac.uk/resource/chembl/target/")) {
                    //Chembl target
                }
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        targetCACHE.put(URI, "Protein>>>"+uniProtID);
        return new SSIPNode("Protein", uniProtID);

    }
    
    
     public Map<String, String> gettargetCACHE() {
        return targetCACHE;
    }

    public String tarRequest(String compURI) throws UnsupportedEncodingException {
        String urlCall = call.getOpenPHACTSURL() + "/target?uri=" + URLEncoder.encode(compURI, "UTF-8") + "&" + call.getAppID() + call.getAppKey() + call.getReturnFormat();
        return urlCall;
    }

    @Override
    public String toString() {
        String f = super.toString();
        return "&_metadata=" + f;
    }
}
