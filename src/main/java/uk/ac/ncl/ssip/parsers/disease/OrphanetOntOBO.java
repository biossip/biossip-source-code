/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.disease;

import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.parsers.genericOBO.OboParse;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class OrphanetOntOBO extends OboParse {
    
    
    public static void main(String[] args) {

        String graphFile = "/Users/joemullen/Desktop/diseaseOnt-temp.gexf";
        Neo4J backend = new Neo4J("/Users/joemullen/Documents/neotest-disease-temp");
        String disont = "/Users/joemullen/Desktop/Datasets/ORPHANET/orphanet_ordo.obo";
        backend.setCommitSize(10000);
        backend.initialiseDatabaseConnection();
        backend.createIndex();
        backend.syncIndexes(10);
        OrphanetOntOBO orph = new OrphanetOntOBO(disont, backend);
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();


    }

    public OrphanetOntOBO(String file, GraphHandlerInterface handler) {
        super(file, OboParse.database.ORPHANET, handler);
    }
}
