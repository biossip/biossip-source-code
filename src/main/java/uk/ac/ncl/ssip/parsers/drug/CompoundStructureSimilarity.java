/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.drug;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.w3c.dom.Document;
import uk.ac.ncl.ssip.parsers.OpenPHACTS.CallOpenPHACTS;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class CompoundStructureSimilarity {

    private CallOpenPHACTS open;

    public static void main(String[] args) throws UnsupportedEncodingException {
        
        CompoundStructureSimilarity cs = new CompoundStructureSimilarity();
        cs.run();
        
        
        //mine
        //https://beta.openphacts.org/1.4/structure/similarity?app_id=374a8178&app_key=35178959e35c09eaeeed7ecbfc91aa10&searchOptions.Molecule=CCCC1%3DNN(C)C2%3DC1NC(%3DNC2%3DO)C1%3DC(OCC)C%3DCC(%3DC1)S(%3DO)(%3DO)N1CCN(C)CC1+&searchOptions.SimilarityType=0&searchOptions.Threshold=0.8&_format=xml
        
        //https://beta.openphacts.org/1.4/structure/similarity?app_id=374a8178&app_key=35178959e35c09eaeeed7ecbfc91aa10&searchOptions.Molecule=CCCC1%3DNN(C)C2%3DC1NC(%3DNC2%3DO)C1%3DC(OCC)C%3DCC(%3DC1)S(%3DO)(%3DO)N1CCN(C)CC1&searchOptions.SimilarityType=0&searchOptions.Threshold=0.8&_format=xml
    }
    
    public CompoundStructureSimilarity(){
    
        this.open = new CallOpenPHACTS();
    
    }
    
    public void run() throws UnsupportedEncodingException{
     String smiles  = "CC(=O)Oc1ccccc1C(=O)O";
     String req = getCompoundSimRequest(smiles);
     
     Document doc = open.callXMLMethod(req);
        
    
    }
    

    public String getCompoundSimRequest(String SMILES) throws UnsupportedEncodingException {
        String urlCall = open.getOpenPHACTSURL() + "/structure/similarity?" + open.getAppID() + open.getAppKey() + "&searchOptions.Molecule=CCCC1%3DNN(C)C2%3DC1NC(%3DNC2%3DO)C1%3DC(OCC)C%3DCC(%3DC1)S(%3DO)(%3DO)N1CCN(C)CC1" + "+&searchOptions.SimilarityType=0&searchOptions.Threshold=0.8" + open.getReturnFormat();
        return urlCall;
    }
}
//+ URLEncoder.encode(SMILES, "UTF-8") 