/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.OpenPHACTS;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Compounds {

    private Set<String> drugIDs;
    private Set<String> compURIs;
    private Map<String, String> drugCACHE;
    //private Map<String, String> targetCACHE;
    private CallOpenPHACTS call;
    private Mapping freetext;
    private GraphHandlerInterface handler;
    private final String DBURIPREFIX = "http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugs/";
    private final String DRUG_ITEMS = "/result/primaryTopic/exactMatch/item";
    private final String TAR_XPATH = "/result/items/item";
    private final String TAR_COMPONENTS = "hasAssay/hasTarget/hasTargetComponent";

    public Compounds(Set compIds, GraphHandlerInterface handler) {
        this.drugCACHE = new HashMap<String, String>();
        this.compURIs = new HashSet<String>();
        this.handler = handler;
        this.drugIDs = compIds;
        this.call = new CallOpenPHACTS();

    }

    public Compounds(CallOpenPHACTS call) {
        this.drugCACHE = new HashMap<String, String>();
        this.call = call;
    }

    public void getAllInfo() {
        for (String id : drugIDs) {
            getCompoundNode(id);
        }
        //get tissue info


    }

    public Map<String, List<String>> getTargetURIsOfCompound(String URI) {
        Map<String, List<String>> targets = null;
        Document tarDoc = null;
        try {
            tarDoc = call.callXMLMethod(getCompPharmacology(
                    URI));
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        targets = extractTarData(tarDoc);
        return targets;
    }

    public SSIPNode getCompoundNode(String id) {
        //System.out.println(URI);
        String request;
        Document doc = null;
        try {
            doc = call.callXMLMethod(getCompInfoRequest(id));
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        return extractCompData(doc, id);

    }

    public SSIPNode extractCompData(Document doc, String URI) {
        //pharmacology doc of compound
        Document tarDoc = null;

        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        //compound node
        SSIPNode comp = null;
        //variable for compound node
        String dataSourceHref = null;
        String chemblType = null;
        String genericDBName = null;
        //Set<String> DBtypes = new HashSet<String>();
        NodeList items = null;
        try {
            items = (NodeList) xpath.evaluate(DRUG_ITEMS, doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
        for (int i = 0; i < items.getLength(); i++) {
            try {
                dataSourceHref = (String) xpath.evaluate("@href", items.item(i), XPathConstants.STRING);
                // System.out.println(dataSourceHref);
                if (dataSourceHref.startsWith("http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugs/")) {
                    //drugbank stuff
                    genericDBName = (String) xpath.evaluate("genericName", items.item(i), XPathConstants.STRING);

                }

                if (dataSourceHref.startsWith("http://rdf.ebi.ac.uk/resource/chembl/molecule/")) {
                    //chembl stuff
                    chemblType = call.splitAtHash((String) xpath.evaluate("type//@href", items.item(i), XPathConstants.STRING));

                }

                if (dataSourceHref.startsWith("http://ops.rsc.org")) {
                    //OPS stuff
                }
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        //create compound node
        // System.out.println(genericDBName);
        // System.out.println(chemblType);
        comp = new SSIPNode(chemblType, genericDBName);
        drugCACHE.put(URI, chemblType + ">>>" + genericDBName);
        return comp;

    }

    public Map<String, List<String>> extractTarData(Document doc) {
        //store the possible targets in a hashmap; with type and then a set of URIs
        Map<String, List<String>> targets = new HashMap<String, List<String>>();
        //Set<SSIPNode> targets = new HashSet<SSIPNode>();
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        NodeList entry = null;
        NodeList tarComponent = null;
        NodeList individTar = null;
        //variables for target
        String type = null;
        String name = null;
        Set<String> URIs = new HashSet<String>();
        try {
            entry = (NodeList) xpath.evaluate(TAR_XPATH, doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
        for (int i = 0; i < entry.getLength(); i++) {
            System.out.println("--------------------");
            try {
                name = (String) xpath.evaluate("hasAssay/hasTarget/title", entry.item(i), XPathConstants.STRING);
                type = call.splitAtHash((String) xpath.evaluate("hasAssay/hasTarget/type//@href", entry.item(i), XPathConstants.STRING));

                tarComponent = (NodeList) xpath.evaluate(TAR_COMPONENTS, entry.item(i), XPathConstants.NODESET);
                String tarURI = null;
                if (type.equals("CellLine") || type.equals("Organism")) {
                    tarURI = (String) xpath.evaluate("hasAssay/hasTarget//@href", entry.item(i), XPathConstants.STRING);
                    URIs.add(tarURI);

                }
                for (int u = 0; u < tarComponent.getLength(); u++) {

                    System.out.println(type);
                    if (type.equals("SingleProtein")) {
                        tarURI = (String) xpath.evaluate("exactMatch//@href", tarComponent.item(u), XPathConstants.STRING);
                        System.out.println("tarURI: " + tarURI);
                        URIs.add(tarURI);
                    }




                    if (type.equals("UnclassifiedTarget")) {
                        System.out.println("unclassified");
                        System.out.println(name);
                    }

                    if (type.equals("Tissue")) {
                        System.out.println("Tissue");
                        System.out.println(name);
                    }

                    if (type.equals("ProteinFamily")) {
                        System.out.println("ProteinFamily");
                        System.out.println(name);
                    }

                    if (type.equals("NucleicAcid")) {
                        System.out.println("Nucleic Acid");
                        System.out.println(name);
                    }

                    if (type.equals("ProteinComplex")) {
                        System.out.println("ProteinComplex");
                        System.out.println(name);

                    } else {
                        //there are multiple URIs to collect
                        individTar = (NodeList) xpath.evaluate("item", tarComponent.item(u), XPathConstants.NODESET);
                        for (int p = 0; p < individTar.getLength(); p++) {
                            tarURI = ((String) xpath.evaluate("exactMatch//@href", individTar.item(p), XPathConstants.STRING));
                            URIs.add(tarURI);
                        }
                    }
                }

            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }


            if (targets.containsKey(type)) {
                //System.out.println("contain key type of: " + type);
                List<String> URIsets = targets.get(type);
                String list = getURIsAsString(URIs);
                URIsets.add(list);
                targets.put(type, URIsets);
            } else {
                List<String> URIsets = new ArrayList<String>();
                URIsets.add(getURIsAsString(URIs));
                targets.put(type, URIsets);

            }

            //reset varibles 
            URIs.clear();
        }

        System.out.println("targets: " + targets.toString());
        return targets;
    }

    public String getURIsAsString(Set<String> uris) {
        int count = 0;
        String allURIs = "";

        for (String uri : uris) {
            count++;
            if (count == 1) {
                allURIs = uri;
            } else {
                allURIs = allURIs + ">>>" + uri;

            }
        }
        return allURIs;
    }

    public Map<String, String> getdrugCACHE() {
        return drugCACHE;
    }

    public String getCompInfoRequest(String compURI) throws UnsupportedEncodingException {
        String urlCall = call.getOpenPHACTSURL() + "/compound?uri=" + URLEncoder.encode(compURI, "UTF-8") + "&" + call.getAppID() + call.getAppKey() + call.getReturnFormat();
        return urlCall;
    }

    public String getCompPharmacology(String compURI) throws UnsupportedEncodingException {
        String urlCall = call.getOpenPHACTSURL() + "/compound/pharmacology/pages?uri=" + URLEncoder.encode(compURI, "UTF-8") + "&" + call.getAppID() + call.getAppKey() + call.getTarOrganism() + call.getPageSize() + call.getReturnFormat();
        return urlCall;
    }
}
