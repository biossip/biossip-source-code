package uk.ac.ncl.ssip.parsers.Protein;

/*
 * Copyright 2013 Joseph Mullen
 *
 */
import java.util.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;
import uk.ac.ncl.ssip.metadata.DReNInMetadata;

/**
 *
 * @author Joseph Mullen http://intbio.ncl.ac.uk/?people=joe-mullen
 *
 */
public class UniProtSwissProtXML {

    private String filepath;
    private Scanner sc;
    private GraphHandlerInterface handler;
    private final static Logger LOGGER = Logger.getLogger(UniProtSwissProtXML.class.getName());
    private String ELEMENT = "entry";
    private String NAMESPACE = "{http://uniprot.org/uniprot}";
    private String characters;
    boolean element = false;
    //Xpath queries
    private String ORGANISM_XPATH = "/entry/organism/dbReference/@id";
    private String PROTEIN_UNIPROTACCESSIONS_XPATH = "/entry/accession";
    private String PROTEIN_SEQ = "/entry/sequence";
    private String PROTEIN_UNIPROTUID_XPATH = "/entry/name";
    private String PROTEIN_NAME_XPATH = "/entry/protein/recommendedName/fullName";
    private String GENE_NAME_XPATH = "/entry/gene/name[@type=\"primary\"]";
    private String DBREFS_XPATH = "/entry/dbReference";
    private int count = 0;
    //counts
    private int proteins = 0;
    private int genes = 0;
    //boolean
    private boolean includeGenes;
    private boolean includeProteins;

    public static void main(String[] args) {
        File xmlDB = new File("/Users/joemullen/Desktop/Datasets/UniProtSwissProt/uniprot_sprot.xml");
        //UniProtSwissProtXML uf = new UniProtSwissProtXML(xmlDB);
        //uf.streamFile();
    }

    public UniProtSwissProtXML(String filepath, GraphHandlerInterface handler, boolean proteins, boolean genes) {
        this.filepath = filepath;
        this.handler = handler;
        this.includeProteins = proteins;
        this.includeGenes = genes;
        //this.GOTermCACHE = new HashSet<String>();
    }

    /**
     * Parse Proteins and Genes from UniProt
     *
     * @throws IOException
     */
    public void run() {

        StringWriter buf = new StringWriter(1024);
        element = false;
        try {
            LOGGER.info("Parsing uniprot file "
                    + filepath);

            //Setup XML 
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory
                    .createXMLEventReader(new FileReader(filepath));

            int count = 0;

            while (eventReader.hasNext()) {

                count++;
                // Get the next event
                XMLEvent event = eventReader.nextEvent();
                //if we have passed the start tag append the string with the event
                if (element) {
                    event.writeAsEncodedUnicode(buf);

                }

                //if we reach the endelement create XML document and extract data
                if (event.isEndElement()) {
                    EndElement e = event.asEndElement();
                    String name = e.getName().toString();
                    if (name.equals(NAMESPACE + ELEMENT)) {
                        element = false;

                        Document doc = getXML(buf.toString());
                        extractData(doc, buf.toString());
                        // System.out.println("-----------------");
                        // System.out.println("-----------------");
                        buf = new StringWriter(1024);
                    }
                }
                //if we reach the start element start wrinting to StringWriter
                if (event.isStartElement()) {
                    StartElement s = event.asStartElement();
                    String name = s.getName().toString();
                    if (name.equals(NAMESPACE + ELEMENT)) {
                        element = true;
                        event.writeAsEncodedUnicode(buf);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  GOTermCACHE.clear();

        System.out.println("accs: " + count);
        LOGGER.info("Parsed " + proteins + " proteins");
        LOGGER.info("Parsed " + genes + " genes");

    }

    /**
     * Takes and XML element and extracts all concept classes and relation types
     * required using XMLPath
     *
     * @param document
     */
    public void extractData(Document document, String data) {

        Map<String, Object> attributes = new HashMap<String, Object>();

        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();

        //initialize nodes
        SSIPNode protein = null;
        SSIPNode gene = null;
        //testing for homo sapiens
        String organism = null;
        //protein attributes
        String proteinUID = null;
        Set<String> uniprotAccessions = new HashSet<String>();
        String uniprotFullName = null;
        String stringAcc = null;
        String sequence = null;

        //gene attributes
        String geneName = null;
        // Set<String> goTerms = null;

        //check the protein comes from a human
        try {
            organism = (String) xpath.evaluate(ORGANISM_XPATH, document, XPathConstants.STRING);

        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }

        if (organism.equals("9606")) {
            //System.out.println(data);
            try {
                uniprotFullName = (String) xpath.evaluate(PROTEIN_NAME_XPATH, document, XPathConstants.STRING);
                proteinUID = (String) xpath.evaluate(PROTEIN_UNIPROTUID_XPATH, document, XPathConstants.STRING);
                sequence = (String) xpath.evaluate(PROTEIN_SEQ, document, XPathConstants.STRING);

            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
            try {
                NodeList accs = (NodeList) xpath.evaluate(PROTEIN_UNIPROTACCESSIONS_XPATH, document, XPathConstants.NODESET);
                for (int i = 0; i < accs.getLength(); i++) {
                    uniprotAccessions.add(accs.item(i).getTextContent());
                }

            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

            try {
                NodeList dbrefs = (NodeList) xpath.evaluate(DBREFS_XPATH, document, XPathConstants.NODESET);
                stringAcc = getSTRINGAcc(dbrefs, "STRING", xpath);

            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

            try {
                geneName = (String) xpath.evaluate(GENE_NAME_XPATH, document, XPathConstants.STRING);

                if (geneName != null) {
                    //create gene node
                    gene = new SSIPNode(DReNInMetadata.NodeType.GENE.getText(), geneName);
                }
                
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

            //create protein node
            protein = new SSIPNode(DReNInMetadata.NodeType.PROTEIN.getText(), proteinUID);
            SSIPRelationType sr = new SSIPRelationType(SSIPRelationType.RelTypes.IS_ENCODED_BY.toString(), 1);
            if (geneName != null) {
                protein.addRelation(gene, sr);
            }
            //add all the uniprot acessions as individual properties as they will be indexed on these
            //for integration
            int count = 1;
            for (String uniacc : uniprotAccessions) {
                protein.addProperty("UniProtID" + count, uniacc);
                count++;
            }
            protein.addProperty("Name", uniprotFullName);
            protein.addProperty("AASeq", sequence);
            if (stringAcc != null) {
                protein.addProperty("STRING", stringAcc);

            }

            //can get other gene IDs
            gene.addProperty("Name", geneName);

            if (includeGenes) {
                handler.addNode(gene);
                genes++;
            }
            if (includeProteins) {
                handler.addNode(protein);
                proteins++;
            }
        }
    }

    public Set<String> getAttributeFromDBREfNodeList(NodeList nodes, String dbref, XPath xpath) throws XPathExpressionException {
        Set<String> attributes = new HashSet<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            NamedNodeMap dbsource = nodes.item(i).getAttributes();
            Node datasource = dbsource.getNamedItem("type");
            String ds = datasource.getNodeValue();
            if (ds.equals(dbref)) {
                attributes.add(dbsource.getNamedItem("id").getNodeValue());
            }
        }
        return attributes;
    }

    public String getSTRINGAcc(NodeList nodes, String dbref, XPath xpath) throws XPathExpressionException {
        String STRINGacc = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            NamedNodeMap dbsource = nodes.item(i).getAttributes();
            Node datasource = dbsource.getNamedItem("type");
            String ds = datasource.getNodeValue();
            if (ds.equals(dbref)) {
                STRINGacc = (dbsource.getNamedItem("id").getNodeValue());
            }
        }
        return STRINGacc;
    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     */
    public Document getXML(String convert) {
  
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            try {
                Document document = builder.parse(new InputSource(new StringReader(convert)));
                return document;
            } catch (SAXException | IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (ParserConfigurationException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }
}
