/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.OpenPHACTS;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.xml.xpath.XPathConstants;
import org.openide.util.Exceptions;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Mapping {

    public CallOpenPHACTS call;
    private final String URI_XPATH = "/result/primaryTopic/result/item//@href";

    public Mapping(CallOpenPHACTS call) {
        this.call = call;
    }

    public String freeTextToConcept(String freeText) throws UnsupportedEncodingException {
        String urlCall = call.getOpenPHACTSURL() + "/search/freetext?" + call.getAppID() + call.getAppKey() + "&q=" + URLEncoder.encode(freeText, "UTF-8") + call.getReturnFormat();
        return urlCall;
    }

    public String getCompoundURI(String db) {

        String URI = null;
        try {
            XPathFactory xpf = XPathFactory.newInstance();
            XPath xpath = xpf.newXPath();
            Document doc = call.callXMLMethod(freeTextToConcept(db));
            try {
                URI = (String) xpath.evaluate(URI_XPATH, doc, XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        return URI;
    }
}
