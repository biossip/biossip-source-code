/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.drug;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.mappers.OpenPHACTSMapping;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author joemullen
 */
/**
 * This class parsers in the contents of SIDER from a flatfile. SIDER database
 * can be found at : http://sideeffects.embl.de/download/ [Concepts] {Drugs,
 * Drug combinations, Diseases} [Relations] {Side Effect, Indication, Involved
 * in drug combination} At the moment only extracting relations between single
 * compounds.
 *
 * @author joemullen
 */
public class SIDERFlatfile {

    private Map labelIDtoSingleDrug;
    private Map labelIDtoDrugCombination;
    private final static Logger LOGGER = Logger.getLogger(SIDERFlatfile.class.getName());
    private int indicationRelCount = 0;
    private int indicationRelCountNULL = 0;
    private int sideEffectRelCount = 0;
    private int sideEffectRelCountNULL = 0;
    private int combinations = 0;
    private int drugs = 0;
    private int diseases = 0;
    private Map<String, String> GENERICNAME2DB;
    private Map<String, String> MEDDRA2MESH;
    private String PHARMGKBDRUGIDs;
    private String SIDERLABLEMAPPINGS;
    private String SIDEREFFECTSRAW;
    private String SIDERINDICATIONSRAW;
    private GraphHandlerInterface handler;
    private OpenPHACTSMapping mapper;
    private BufferedWriter bw;
    private Set<String> notMap;

    public static void main(String[] args) {
        Neo4J backend = new Neo4J("/Users/joemullen/Documents/mappertest");
        String pharmGKBMapping = "/Users/joemullen/Desktop/Datasets/PharmGKB/drugs/drugs.tsv";
        String siderlabs = "/Users/joemullen/Desktop/Datasets/SIDER/label_mapping.tsv";
        String sidereff = "/Users/joemullen/Desktop/Datasets/SIDER/adverse_effects_raw.tsv";
        String siderinds = "/Users/joemullen/Desktop/Datasets/SIDER/indications_raw.tsv";
        //>>>>>>>>>>>>>>>>>> STEP_8 SIDER (side_effects and indications)
        backend.setCommitSize(10000);
        backend.initialiseDatabaseConnection();
        backend.syncIndexes(10);
        SIDERFlatfile sf = new SIDERFlatfile(pharmGKBMapping, siderlabs, sidereff, siderinds, backend);
        //String testMeddra = "c0242350";
        sf.run();
        //System.out.println(sf.getMESHfromMEDDRA(testMeddra));
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();

    }

    public SIDERFlatfile(String PHARMGKBDRUGIDs, String SIDERLABLEMAPPINGS, String SIDEREFFECTSRAW, String SIDERINDICATIONSRAW, GraphHandlerInterface handler) {

        this.handler = handler;
        this.GENERICNAME2DB = new HashMap<String, String>();
        this.labelIDtoSingleDrug = new HashMap();
        this.labelIDtoDrugCombination = new HashMap();
        this.PHARMGKBDRUGIDs = PHARMGKBDRUGIDs;
        this.SIDERLABLEMAPPINGS = SIDERLABLEMAPPINGS;
        this.SIDEREFFECTSRAW = SIDEREFFECTSRAW;
        this.SIDERINDICATIONSRAW = SIDERINDICATIONSRAW;
        this.mapper = new OpenPHACTSMapping();
        this.MEDDRA2MESH = new HashMap<String, String>();
        this.notMap = new HashSet<String>();
        try {
            this.bw = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/Datasets/meddra2mesh.txt"));
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    public void run() {
        getGenericNameToDb();
        //get label mappings
        getLabelMappings(new File(SIDERLABLEMAPPINGS));
        //get side effects
        getInfo(new File(SIDEREFFECTSRAW), true);
        //get indications
        getInfo(new File(SIDERINDICATIONSRAW), false);
        //get???
        try {
            bw.append("Failed to map: " + notMap.toString());
            bw.close();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }


    }

    /**
     * Get the mappings of the generic name -> drugBank id from PharmGKB.
     */
    public void getGenericNameToDb() {

        BufferedReader br = null;
        Map<String, String> PUBCHEM2DBlocal = new HashMap<String, String>();
        String thisLine;
        try {
            //Open the file for reading
            br = new BufferedReader(new FileReader(PHARMGKBDRUGIDs));
            try {
                while ((thisLine = br.readLine()) != null) {
                    String db = null;
                    String pubchem = null;
                    String[] split = thisLine.split("\t");

                    //get the DrugBank ID and the pubchem IDs which are crossRefs
                    String crossRefs = split[6];
                    String[] split2 = crossRefs.split(",");
                    for (int i = 0; i < split2.length; i++) {
                        String[] split3 = split2[i].split(":");
                        if (split3[0].equals("drugBank")) {
                            db = split3[1];
                            // System.out.println(db);
                        }

//                        if (split3[0].equals("pubChemCompound")) {
//                            pubchem = split3[1];
//                        }
                    }

                    pubchem = split[1].toLowerCase();

                    if (db != null && pubchem != null) {
                        //System.out.println("added: " + pubchem + "  " + db);
                        PUBCHEM2DBlocal.put(pubchem, db);
                    }
                }
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (FileNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        this.GENERICNAME2DB = PUBCHEM2DBlocal;

    }

    /**
     * Method extracts the label Id and the drug or drug combination it refers
     * too. This is stored in labelIDtoDrugs. The file to use for this is
     * [label_mapping.tsv] e.g. we have a label id
     * (20100629_c6c048ec-0b99-4fa4-9df7-9926db93749b) mapped to a drug generic
     * name (sildenafil)
     *
     * @param file
     */
    public void getLabelMappings(File file) {

        String thisLine;
        //Open the file for reading
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((thisLine = br.readLine()) != null) {
                String genericNames = null;
                String brandNames = null;
                String marker = null;
                String labelIdentifier = null;
                String STITCHFlatCompound = null;
                String STITCHSTEREOCompound = null;

                String[] split = thisLine.split("\t");
                //genericNames
                genericNames = split[0];
                //brand names
                brandNames = split[1];
                //marker
                //  a marker if the drug could be successfully mapped to STITCH. Possible values:
                // - [empty field]: success
                // - combination: two or more drugs were combined
                // - not found: could not find the name in the database
                // - mapping conflict: the available names point to two different compounds
                // - template: a package insert that contains information for a group of related drugs
                marker = split[2];
                //stitch compound ID flat compound
                STITCHFlatCompound = split[3];
                //stitc compound id stereo isomer 
                STITCHSTEREOCompound = split[4];
                //label identifier
                labelIdentifier = split[6];
                if (brandNames.contains(";")) {
                    //it is a combination
                    //System.out.println("COMBINATIOSN label: "+labelIdentifier+ "brandNames: "+ brandNames);
                    labelIDtoDrugCombination.put(labelIdentifier, brandNames);
                } else {
                    //it is a single drug
                    labelIDtoSingleDrug.put(labelIdentifier, brandNames);
                    // System.out.println("SINGLE label: "+labelIdentifier+ "brandNames: "+ brandNames);
                }
            }
        } catch (IOException e) {
            System.err.println("Error: " + e);
        }

        LOGGER.info("Retrieved all mappings from " + file.toPath().toString());

    }

    /**
     * Extracts has_indication [indications_raw.tsv] or has_side_effect
     * [adverse_effects_raw.tsv]. Returns a list of interactions in the form
     * [DRUGBANK ID] > [DISEASE UMLS]
     *
     * @param file file location
     * @param sideEffects if passing the indications this should be false
     */
    public void getInfo(File file, boolean sideEffects) {


        int count = 0;
        int test = 10;

        String thisLine;
        //Open the file for reading
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((thisLine = br.readLine()) != null) { // while loop begins here

//                if (count == test) {
//                    break;
//                }

                count++;
                String lableID = null;
                String diseaseUMLS = null;
                String indication = null;
                String drugOrCombinationNAMES = null;
                String[] split = thisLine.split("\t");
                //labelID
                lableID = split[0];
                //label identifier try single drug
                if (labelIDtoSingleDrug.containsKey(lableID)) {
                    drugOrCombinationNAMES = (String) labelIDtoSingleDrug.get(split[0]);
                }
                //label identifier try drug combination
                if (labelIDtoDrugCombination.containsKey(lableID)) {
                    drugOrCombinationNAMES = (String) labelIDtoDrugCombination.get(split[0]);
                }
                //concept ID
                diseaseUMLS = split[1];
                //name of side effect 
                indication = split[2];

                /**
                 * TODO need to deal with the drug combinations
                 */
                if (sideEffects) {
                    //create side effect relation
                    if (drugOrCombinationNAMES == null) {
                        sideEffectRelCountNULL++;
                    } else {
                        if (GENERICNAME2DB.containsKey(drugOrCombinationNAMES.toLowerCase())) {
//                            SSIPNode dis = new SSIPNode("UMLS_CUI", diseaseUMLS);
//                            SSIPNode drug = new SSIPNode("DBID", GENERICNAME2DB.get(drugOrCombinationNAMES.toLowerCase()));
//                            SSIPRelationType sr = new SSIPRelationType(SSIPRelationType.RelTypes.HAS_SIDE_EFFECT.toString());
//                            drug.addRelation(dis, sr);
//                            handler.addNode(drug);

                            System.out.println(("----------------"));
                            String mesh = null;
                            if (!MEDDRA2MESH.containsKey(diseaseUMLS)) {
                                mesh = getMESHfromMEDDRA(diseaseUMLS);
                                if (mesh != null) {
                                    MEDDRA2MESH.put(diseaseUMLS, mesh);
                                    bw.append(diseaseUMLS + "\t" + mesh + "\n");
                                } else {

                                    notMap.add(diseaseUMLS);

                                }
                            } else {
                                mesh = MEDDRA2MESH.get(diseaseUMLS);

                            }
                            System.out.println("MEDDRA: " + diseaseUMLS + ">>>>MESH:" + mesh);

                            sideEffectRelCount++;
                        } else {
                            sideEffectRelCountNULL++;
                        }

                    }

                } else {
                    //create an indication relation
                    if (drugOrCombinationNAMES == null) {
                        indicationRelCountNULL++;

                    } else {

                        if (GENERICNAME2DB.containsKey(drugOrCombinationNAMES.toLowerCase())) {

//                            SSIPNode dis = new SSIPNode("UMLS_CUI", diseaseUMLS);
//                            SSIPNode drug = new SSIPNode("DBID", GENERICNAME2DB.get(drugOrCombinationNAMES.toLowerCase()));
//                            SSIPRelationType sr = new SSIPRelationType(SSIPRelationType.RelTypes.TREATS.toString());
//                            drug.addRelation(dis, sr);
//                            handler.addNode(drug);

                            System.out.println(("----------------"));
                            String mesh = null;
                            if (!MEDDRA2MESH.containsKey(diseaseUMLS)) {
                                mesh = getMESHfromMEDDRA(diseaseUMLS);
                                if (mesh != null) {
                                    MEDDRA2MESH.put(diseaseUMLS, mesh);
                                    bw.append(diseaseUMLS + "\t" + mesh + "\n");
                                } else {

                                    notMap.add(diseaseUMLS);

                                }
                            } else {
                                mesh = MEDDRA2MESH.get(diseaseUMLS);

                            }

                            System.out.println("MEDDRA: " + diseaseUMLS + ">>>>MESH:" + mesh);


                            indicationRelCount++;
                        } else {
                            indicationRelCountNULL++;
                        }

                    }
                }

            }
        } catch (IOException e) {
            System.err.println("Error: " + e);

        }
        if (sideEffects) {
            LOGGER.info("Parsed " + sideEffectRelCount + " side_effect relations from " + file.toPath().toString());
            LOGGER.info("Further " + sideEffectRelCountNULL + " side_effect relations referred to NULL drugs");
        } else {
            //create an indication relation
            indicationRelCount++;
            LOGGER.info("Parsed " + indicationRelCount + " has_indication relations " + file.toPath().toString());
            LOGGER.info("Further " + indicationRelCountNULL + " side_effect relations referred to NULL drugs");
        }
    }

    public String getMESHfromMEDDRA(String mesh) {
        String ids = "/result/primaryTopic/result/exactMatch/item";
        String MESH = null;
        try {


            String req = mapper.freeTextToConcept(mesh);
            Document doc = mapper.callMethod(req);
            System.out.println(doc.toString());

            //do something with the returned document
            XPathFactory xpf = XPathFactory.newInstance();
            XPath xpath = xpf.newXPath();

            NodeList idList = null;
            try {
                idList = (NodeList) xpath.evaluate(ids, doc, XPathConstants.NODESET);
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
            if (idList.getLength() > 0) {
                for (int i = 0; i < idList.getLength(); i++) {
                    try {
                        String url = (String) xpath.evaluate("url//@href", idList.item(i), XPathConstants.STRING);
                        if (url.startsWith("http://purl.bioontology.org/ontology/MSH/")) {

                            MESH = url.replaceFirst("http://purl.bioontology.org/ontology/MSH/", "");
                        }

                    } catch (XPathExpressionException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }

        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }

        return MESH;

    }
    /**
     * Extracts all MedDRA diseases from [meddra_adverse_effects.tsv] creates
     * disease nodes; as we are using the diseaseUMLS that means we can map to
     * other disease sources that use the same format--- we don't need to create
     * the diseases in this parser.
     */
//    public void getMedDRASideEffectsInfo(File file) {
//
//        Set<String> unmapped = new HashSet<String>();
//        String thisLine;
//        //Open the file for reading
//        try {
//            BufferedReader br = new BufferedReader(new FileReader(file));
//            while ((thisLine = br.readLine()) != null) {
//                String stitchCompoundIDFLAT = null;
//                String stitchCompoundIDSTEREO = null;
//                String drugName = null;
//                String diseaseName = null;
//                String diseaseUMLS = null;
//                String diseaseCT = null;
//                String diseaseMedDRAname = null;
//
//                String[] split = thisLine.split("\t");
//                //stitch ID split[0]
//                stitchCompoundIDFLAT = split[0];
//                //stitch ID split[1]
//                stitchCompoundIDSTEREO = split[1];
//                //UMLS concept ID as found on the label split [2]
//                //drug name
//                drugName = split[3];
//                //side effect name
//                diseaseName = split[4];
//                //MedDRA concept type (LLT = lowest level term, PT = preferred term)
//                diseaseCT = split[5];
//                //UMLS concept id for MedDRA term
//                diseaseUMLS = split[6];
//                //MedDRA side effect name
//                diseaseMedDRAname = split[7];
//                //we only want to create the relation using the lowest level term
//                if (diseaseCT.equals("LLT")) {
//                    //System.out.println(drugName + " side effect " + diseaseUMLS);
//                    if (PUBCHEM2DB.containsKey(stitchCompoundIDSTEREO.substring(1, stitchCompoundIDSTEREO.length()))) {
//                        System.out.println("whaatttatatatat");
//                        System.out.println(PUBCHEM2DB.get(stitchCompoundIDSTEREO.substring(1, stitchCompoundIDSTEREO.length())) + " side effect " + diseaseUMLS);
//
//                    } //                    if (PUBCHEM2DB.containsKey(drugName)) {
//                    //                        System.out.println(PUBCHEM2DB.get(drugName) + " side effect " + diseaseUMLS);
//                    //                        
//                    //                    } 
//                    else {
//
//                        unmapped.add(drugName);
//
//                    }
//
//                }
//
//            }
//
//            System.out.println(unmapped.size());
//            System.out.println(unmapped.toString());
//
//        } catch (IOException e) {
//            System.err.println("Error: " + e);
//        }
//    }
}
