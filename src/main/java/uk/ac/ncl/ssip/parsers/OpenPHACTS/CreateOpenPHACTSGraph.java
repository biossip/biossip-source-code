/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.OpenPHACTS;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;
import uk.ac.ncl.ssip.queryframework.DReNInMining;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class CreateOpenPHACTSGraph {

    private Set<String> drugIDs;
    private Map<String, String> drugCACHE;
    private Map<String, String> targetCACHE;
    //instances of classes
    private CallOpenPHACTS call;
    private Mapping freetext;
    private Targets target;
    private Pathways path;
    private Compounds compound;
    private Tissue tissue;
    private Diseases disease;
    //SSIP specific
    private GraphHandlerInterface handler;

    public static void main(String[] args) {

        String graphFile = "/Users/joemullen/Desktop/gephi_export_trav_sildenafil.gexf";//"F://sildenafil.gexf";//
       
        Neo4J backend = new Neo4J("/Users/joemullen/Documents/neotest-sildenafil");//"F://sildenofil");//
        //connect to database 
        backend.initialiseDatabaseConnection();
        //create index
    //    backend.createIndex();

        //run compound

        String db = "Sildenafil";
        //String db = "Flucloxacillin";
        // String db = "DB00203";
        //String db ="medicarpin";
        //sildenfil URI http://www.conceptwiki.org/concept/8f5662bd-9c25-4017-904c-46ed3d1a9f28
        Set<String> test = new HashSet<String>();
        test.add(db);
        CreateOpenPHACTSGraph c = new CreateOpenPHACTSGraph(test, backend);
        c.run();


//        System.out.println("export all code starts here...");
//        GephiExporter gexfExportSubgraph = new GephiExporter();
//
//  //      backend.syncIndexes(10);
//        //query database
//      //  gexfExportSubgraph.export(backend.returnAllNodesMap(), graphFile);
//        DReNInMining dm = new DReNInMining();
//        StepDescription[] steps = dm.getAllTargets();
//        gexfExportSubgraph.export(backend.traversal(new SSIPNode("SmallMolecule", "Sildenafil"), steps), graphFile);

        //close database again 
        backend.finaliseDatabaseConnectionNoUPDATES();



    }

    private CreateOpenPHACTSGraph(Set<String> drugDBIds, GraphHandlerInterface handler) {

        this.handler = handler;
        this.drugIDs = drugDBIds;
        this.call = new CallOpenPHACTS();
        this.freetext = new Mapping(call);
        this.path = new Pathways(call);
        this.target = new Targets(call);
        this.compound = new Compounds(call);
        this.tissue = new Tissue(call);
        this.disease = new Diseases(call);

    }

    public void run() {
        Map<String, String> drugCache = null;
        Map<String, String> targetCache = null;
        Set<SSIPNode> targetNODES = new HashSet<SSIPNode>();
        //for every drugBank id provided
        for (String id : drugIDs) {
            String URI = null;
            SSIPNode drug = null;

            URI = freetext.getCompoundURI(id);

            drug = compound.getCompoundNode(URI);
            Map<String, List<String>> targets = compound.getTargetURIsOfCompound(URI);

            //create the target nodes
            for (String type : targets.keySet()) {
                if (type.equals("SingleProtein")) {
                    List<String> URIs = targets.get(type);
                    for (String individ : URIs) {
                        targetNODES.add(target.getSingleProteinNode(individ));
                    }
                }

                if (type.equals("ProteinFamily")) {
                    List<String> URIs = targets.get(type);
                    String familyURI = "";
                    for (String individ : URIs) {
                        //the family URI is the first one
                        String[] split = individ.split(">>>");
                        familyURI = split[0];
                        targetNODES.add(target.getProteinFamilyNode(familyURI));
                    }
                }

                if (type.equals("ProteinComplex")) {
                    List<String> URIs = targets.get(type);
                    String familyURI = "";
                    for (String individ : URIs) {
                        //the family URI is the first one
                        String[] split = individ.split(">>>");
                        familyURI = split[0];
                        targetNODES.add(target.getProteinComplexNode(familyURI));
                    }

                }

                if (type.equals("CellLine")) {

                    List<String> URIs = targets.get(type);
                    String familyURI = "";
                    for (String individ : URIs) {
                        //the family URI is the first one
                        String[] split = individ.split(">>>");
                        familyURI = split[0];
                        targetNODES.add(target.getCellLineNode(familyURI));
                    }

                }

                if (type.equals("Tissue")) {
                    List<String> URIs = targets.get(type);
                    String familyURI = "";
                    for (String individ : URIs) {
                        //the family URI is the first one
                        String[] split = individ.split(">>>");
                        familyURI = split[0];
                        targetNODES.add(target.getTissueNode(familyURI));
                    }

                }

                if (type.equals("Organism")) {
                    List<String> URIs = targets.get(type);
                    String familyURI = "";
                    for (String individ : URIs) {
                        //the family URI is the first one
                        String[] split = individ.split(">>>");
                        familyURI = split[0];
                        targetNODES.add(target.getOrganismNode(familyURI));
                    }

                }


            }


            //add relations from all the targets to the drug node
            for (SSIPNode node : targetNODES) {
                drug.addRelation(node, new SSIPRelationType(SSIPRelationType.RelTypes.BINDS_TO.toString(), 1));
            }
//        //add comp node to transaction first
            handler.addNode(drug);

            //add all the target nodes to the graph
            for (SSIPNode node : targetNODES) {
                handler.addNode(node);
            }
            //clear variables
            targetNODES.clear();
        }
        //add all pathways and tissue relations to the drugs
        drugCACHE = compound.getdrugCACHE();
        //add all pathways and tissue relations to the drugs
        targetCACHE = target.gettargetCACHE();

        addCompoundPathways();
        addTargetPathwaysAndTissues();
        addTargetDiseases();

    }

    public void addCompoundPathways() {

        for (String id : drugCACHE.keySet()) {
            String UIDs = drugCACHE.get(id);
            String[] split = UIDs.split(">>>");
            SSIPNode comp = new SSIPNode(split[0], split[1]);
            Set<SSIPNode> pathways = path.getPathwayNodeCOMP(id);
            for (SSIPNode pa : pathways) {
                comp.addRelation(pa, new SSIPRelationType(SSIPRelationType.RelTypes.PART_OF_PATHWAY.toString(), 1));

            }
            handler.addNode(comp);

            for (SSIPNode pa : pathways) {
                System.out.println(pa.getId());
                handler.addNode(pa);
            }

        }
    }

    public void addTargetPathwaysAndTissues() {

        for (String id : targetCACHE.keySet()) {
            String UIDs = targetCACHE.get(id);
            String[] split = UIDs.split(">>>");
            System.out.println("UIDs: " + UIDs);
            SSIPNode tar = new SSIPNode(split[0], split[1]);
            Set<SSIPNode> pathways = path.getPathwayNodeTARGET(id);
            Set<SSIPNode> tissues = tissue.getTissueNodes(id);
            for (SSIPNode pa : pathways) {
                tar.addRelation(pa, new SSIPRelationType(SSIPRelationType.RelTypes.PART_OF_PATHWAY.toString(), 1));
            }

            for (SSIPNode tis : tissues) {
                tar.addRelation(tis, new SSIPRelationType(SSIPRelationType.RelTypes.PART_OF_TISSUE.toString(), 1));
            }

            handler.addNode(tar);
            for (SSIPNode pa : pathways) {
                handler.addNode(pa);
            }

            for (SSIPNode tis : tissues) {
                handler.addNode(tis);
            }

        }
    }

    public void addTargetDiseases() {

        for (String id : targetCACHE.keySet()) {
            String UIDs = targetCACHE.get(id);
            String[] split = UIDs.split(">>>");
            SSIPNode tar = new SSIPNode(split[0], split[1]);
            Set<SSIPNode> diseases = disease.getDiseaseNodes(id);

            for (SSIPNode dis : diseases) {
                tar.addRelation(dis, new SSIPRelationType(SSIPRelationType.RelTypes.INVOLVED_IN.toString(), 1));
            }
            handler.addNode(tar);
            for (SSIPNode dis : diseases) {
                handler.addNode(dis);
            }

        }
    }
}
