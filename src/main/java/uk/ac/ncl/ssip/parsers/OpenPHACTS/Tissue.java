/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.parsers.OpenPHACTS;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import uk.ac.ncl.ssip.metadata.SSIPNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Tissue {
    
    
    private CallOpenPHACTS call;
    private Mapping freetext;
    private final String TISSUE_XPATH = "/result/items/item";
    private String target = "http://purl.uniprot.org/uniprot/P55795";

    public Tissue(CallOpenPHACTS call) {
        this.call = call;
    }

    public static void main(String[] args) {
//
//        CallOpenPHACTS call = new CallOpenPHACTS();
//        Tissue c = new Tissue(call);
//        c.requestInfo();

    }

    public Set<SSIPNode> getTissueNodes(String URI) {
        //System.out.println(URI);
       Document doc = null;
        try {
            doc = call.callXMLMethod(getTissuesForProteinRequest());
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        return extractTissuesData(doc);

    }
    
   
    public Set<SSIPNode> extractTissuesData(Document doc) {
        Set<SSIPNode> tissueNodes = new HashSet<SSIPNode>(); 
        SSIPNode tissue  =null;
        
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        NodeList entry = null;
        String href = null;
        String label = null; 

        try {
            entry = (NodeList) xpath.evaluate(TISSUE_XPATH, doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
        for (int i = 0; i < entry.getLength(); i++) {
            System.out.println("---------------");
            try {
                href = (String) xpath.evaluate("tissue//@href", entry.item(i), XPathConstants.STRING);
                label = (String) xpath.evaluate("tissue/label", entry.item(i), XPathConstants.STRING);
             
            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }
            System.out.println("href: "+ href);
            System.out.println("label: "+label);
            
            tissue = new SSIPNode("Tissue", label);
            tissueNodes.add(tissue);

        }
        return tissueNodes;
    }
    
    public String getTissueInformation(){
    
    //need to complete
        return null;
    
    }

    public String getTissuesForProteinRequest() throws UnsupportedEncodingException {
        String urlCall = call.getOpenPHACTSURL() + "/tissue/byProtein?uri=" + URLEncoder.encode(target, "UTF-8") + "&" + call.getAppID() + call.getAppKey() + call.getPageSize() + call.getReturnFormat();
        return urlCall;
    }
    
}
