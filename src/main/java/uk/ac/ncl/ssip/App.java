package uk.ac.ncl.ssip;

import java.io.IOException;
import org.openide.util.Exceptions;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.JVM;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import Redundant.Neo4jGraphHandler;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;
import java.util.Map;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J_multiIndexes;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.parsers.drug.DrugBankXML;
import uk.ac.ncl.ssip.parsers.protein.KeggOrthologyParser;


/**
 *
 *
 */
public class App {

//    GraphHandlerInterface handler = new Neo4J_multiIndexes("D://SSIP-neo4j-subgraph-multi_index-17");
    Neo4J_multiIndexes handler = new Neo4J_multiIndexes("D://SSIP-neo4j-subgraph-multi_index-35");

    public static void main(String[] args) {
        App a = new App();
//        a.testUniProt();
        //     a.testKoParser();
        a.DReNIN();

    }

    /**
     * Test to see if the KO parser works.
     */
    public void testKoParser() {

        //Joe's filepaths
        handler = new Neo4J_multiIndexes("/Users/joemullen/Documents/neotestKEGG");
        String filepath = "/Users/joemullen/Desktop/Datasets/KEGG/ko00001.keg";
        String graphFile2 = "/Users/joemullen/Desktop/kegg_export_trav_subgraph.gexf";

//        //Matts filepaths
//        String filepath = "F://china_dataset/ko00001.keg";//"D://mni_files/ko00001.keg";//
//        String graphFile = "D://Program Files (x86)/Dropbox/SSIP/gephi_export_2.gexf";//"C://Users/Matt2/Documents/Dropbox/SSIP/gephi_export.gexf";//"D://Program Files (x86)/Dropbox/SSIP/gephi_export_2.gexf";
//        String graphFile2 = "D://Program Files (x86)/Dropbox/SSIP/gephi_export_subgraph.gexf";//"C://Users/Matt2/Documents/Dropbox/SSIP/gephi_export_trav_subgraph.gexf";//

        handler.initialiseDatabaseConnection();

        KeggOrthologyParser keggParser = new KeggOrthologyParser(filepath, handler);
        keggParser.parseFile();

//        handler.finaliseDatabaseConnection();
//
//        handler.initialiseDatabaseConnection();
//        handler.createIndex("type: KOlevel1", "type: KOlevel1");

        GephiExporter gexfExportSubgraph = new GephiExporter();

        StepDescription[] steps = new StepDescription[2];
        steps[0] = new StepDescription(2, true);
        steps[0].addRelation(StepDescription.RelTypes.BINDS_TO);
        steps[1] = new StepDescription(1, true);
        steps[1].addRelation(StepDescription.RelTypes.KNOWS);

//        gexfExportSubgraph.export(handler.traversal(new SSIPNode("KOlevel1", "Metabolism"), steps), graphFile2);
        gexfExportSubgraph.export(handler.traversal(new SSIPNode("KOlevel1", "Metabolism"), steps), graphFile2);

        handler.finaliseDatabaseConnection();

    }

    public void DReNIN() {
//
//        //initialise connection
        Neo4J_multiIndexes handler = new Neo4J_multiIndexes("/Users/joemullen/Documents/neotest");

//        handler.createIndex("Small_Molecule", "Small_Molecule");

        handler.initialiseDatabaseConnection();

        String graphFile = "/Users/joemullen/Desktop/gephi_export_trav_subgraph.gexf";
//

        // drugs
//        String f = new String("/Users/joemullen/Desktop/Datasets/DrugBank/drugbank.xml");
//        DrugBankXML db = new DrugBankXML(f, handler);
//        db.streamFile();
//        db.addDrugInteraction();


        //proteins
//        String xmlDB = new String("/Users/joemullen/Desktop/Datasets/UniProtSwissProt/uniprot_sprot.xml");
//        UniProtSwissProtXML uf = new UniProtSwissProtXML(xmlDB, handler);
//        uf.streamFile();

//        handler.finaliseDatabaseConnection();
//
//        handler.initialiseDatabaseConnection();

        GephiExporter gexfExportSubgraph = new GephiExporter();
//

        StepDescription[] steps = new StepDescription[1];
//        steps[0] = new StepDescription(2, true);
//        steps[0].addRelation(StepDescription.RelTypes.INTERACTS_WITH);
        steps[0] = new StepDescription(1, true);
        //steps[0].addRelation(StepDescription.RelTypes.BINDS_TO);
        //steps[0].addRelation(StepDescription.RelTypes.INTERACTS_WITH);

        // export a drugbank drug 
        gexfExportSubgraph.export(handler.traversal(new SSIPNode("Small_Molecule", "DB00203"), steps), graphFile);
        //export a protein


        handler.finaliseDatabaseConnection();


//

//
//        //diseases
//        
//        
//        //side effects
//
//
//
//        //export to Gephi
//        GephiExporter gexfExport = new GephiExporter();
//        gexfExport.export(handler.returnAllNodesMap(), graphFile);
//
//        //finalise transaction
//        handler.finaliseDatabaseConnection();

    }

    /**
     * Test to see that the UniProt parser works, printing out all nodes that
     * are successfully parsed *NOTE* the parser is hard coded to 100 nodes
     */
    public void testUniProt() {
//
//        String filepath = "D:\\uniprot_sprot.dat";
//        UniProtSwissProt uni = new UniProtSwissProt(filepath, handler);
//        try {
//            uni.parseFile();
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }
//
//        GephiExporter gexfExport = new GephiExporter();
//        gexfExport.export(handler.returnAllNodesMap(), "D://nice_graph.gexf");
        //returnAllNodes should be replaced by returnAllNodesMap
//        for (MetaDataInterface st : handler.returnAllNodes()) {
//            System.out.println(st);
//        }

    }
}
