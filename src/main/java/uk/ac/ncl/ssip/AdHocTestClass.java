/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip;

import java.util.ArrayList;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.openide.util.Exceptions;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J_multiIndexes;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;
import uk.ac.ncl.ssip.parsers.protein.KeggOrthologyParser;

/**
 *
 * @author Matt2
 */
public class AdHocTestClass {

    public static void main(String[] args) {

        String version = "0.7";
        String filepath = "D://mni_files/ko00001.keg";//"F://china_dataset/ko00001.keg";//
        String graphFile = "C://Users/Matt2/Documents/Dropbox/SSIP/gephi_export_test.gexf";//"D://Program Files (x86)/Dropbox/SSIP/gephi_export_2.gexf";//
        Neo4J backend = new Neo4J("D://neo4j-databases/test-" + version, "D://performance-" + version);

        //connect to database 
        backend.initialiseDatabaseConnection();
        //create index
        backend.createIndex();

        KeggOrthologyParser keggParser = new KeggOrthologyParser(filepath, backend);
        keggParser.parseFile();

        //query database
        System.out.println("export all code starts here...");
        GephiExporter gexfExportSubgraph = new GephiExporter();
        gexfExportSubgraph.export(backend.returnAllNodesMap(), graphFile);

        //close database again 
        backend.finaliseDatabaseConnection();

//        backend.createIndex("altID");
//        backend.syncIndexes(10);
//        SSIPNode snod = new SSIPNode("test", "Id");
//        snod.addProperty("altID", "Id2");
//        snod.addRelation(new SSIPNode("altID", "Id1"), new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
//        snod.addRelation(new SSIPNode("altID", "Id2"), new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
//        snod.addRelation(new SSIPNode("altID", "Id3"), new SSIPRelationType(SSIPRelationType.RelTypes.KNOWS.toString()));
//        backend.addNode(snod);
//        backend.commitTransaction();
//
//        backend.addNode(new SSIPNode("altID", "Id"));
//        backend.addNode(new SSIPNode("altID", "Id2"));
//        backend.addNode(new SSIPNode("altID", "Id3"));
//        populate databases and index 
//
//        backend.initialiseDatabaseConnection();
//        backend.syncIndexes(5);


    }

}
