/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.mappers.BLAST;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openide.util.Exceptions;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class FASTAFile {

    private String seq;
    private String id;
    private String dir;

    /**
     * Class takes a String sequence and an ID and returns a .FASTA file.
     *
     * @param seq
     * @param id
     */
    
    public static void main (String [] args){
    
        String id = "john";
        String dir = "/Users/joemullen/Desktop/";
        String seq = "sfsdfasjfbiaubgouayrbouybfkjnsdfknasduvbauvybairvnkjdsnvansdfiyabfbkansdkjnasdfbiasubdfakdsjnvapuvniayrbfpirbvkpajndvarfipabrfvpiursdasdasdfdsfsdfsdfsdfs";
        FASTAFile ff = new FASTAFile(seq, id, dir);
        ff.createFile();
    
    }
    
    
    
    public FASTAFile(String seq, String id, String dir) {
        this.seq = seq;
        this.id = id;
        this.dir = dir;
    }

    public void createFile() {
        
        //makes sure seq is in the same format
        seq.trim();
        seq.replace("//s", "");
        
        try {
            BufferedWriter bw = null;
            try {
                bw = new BufferedWriter(new FileWriter(dir+"/"+id + ".fas"));
                //first line- identifier
                bw.append(">" + id + "\n");
                //add every 'split' characters
                int split= 80;
                int len = seq.length();
                for (int i = 0; i < len; i += split) {   
                    bw.append(seq.substring(i, Math.min(len, i + split))+"\n");
                }   
                
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
            
            bw.close();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }

        
    }

    private List<String> getParts(String string, int partitionSize) {
        List<String> parts = new ArrayList<String>();

        return parts;
    }

    public String getSeq() {
        return seq;
    }

    public String getId() {
        return id;
    }

}
