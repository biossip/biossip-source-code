/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.mappers;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class contains methods that call the OpenPHACTS free text query. This
 * ensures that everything we parse utilises the same accessions.
 *
 * @author joemullen
 */
public class OpenPHACTSMapping {

    private String OPENPHACTSURL = "https://beta.openphacts.org/1.4";
    private String APPID = "374a8178";
    private String APPKEY = "35178959e35c09eaeeed7ecbfc91aa10";
    private format RETURNFORMAT = format.xml;
    private metadata RETURNMETADATA = metadata.formats;
    private String ORGANISM = "&assay_organism=Homo+sapiens";
    private String MINEXACTIVITY = "&minEx-activity_value=8300";
    private String SIMTYTPE = similarityType.Tanimoto.type;
    private String THRESHOLD = threshold(0.91);
    private final static Logger LOGGER = Logger.getLogger(OpenPHACTSMapping.class.getName());
    private final String ACCESSION_XPATH = "/result/primaryTopic/result/item//@href";

    public static void main(String[] args) {

        OpenPHACTSMapping am = new OpenPHACTSMapping();
        try {
            String meth = am.freeTextToConcept("sildenafil");
            am.callMethod(meth);
        } catch (UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    public Document callMethod(String request) {
        Document doc = null;
        try {

            HttpClient openPHACTSclient = HttpClientBuilder.create().build();
            HttpGet getMethod = new HttpGet(request);
            LOGGER.info("Executing request: " + "\n" + getMethod.getURI());
            HttpResponse reponseBody = null;
            try {
                reponseBody = openPHACTSclient.execute(getMethod);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
            HttpEntity entity = reponseBody.getEntity();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = null;
            try {
                builder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                Exceptions.printStackTrace(ex);
            }
            doc = builder.parse(entity.getContent());
            getMethod.releaseConnection();
            LOGGER.info("Connection Released");
        } catch (SAXException | IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return doc;

    }

    public String freeTextToConcept(String freeText) throws UnsupportedEncodingException {
        String urlCall = OPENPHACTSURL + "/search/freetext?app_id=" + APPID + "&app_key="+APPKEY + "&q=" + URLEncoder.encode(freeText, "UTF-8") + RETURNFORMAT;
        return urlCall;
      //  https://dev.openphacts.org/docs/1.4/search/freetext?app_id=374a8178&app_key=35178959e35c09eaeeed7ecbfc91aa10&q=c0242350&_format=xml
        //curl -v  -X GET "https://beta.openphacts.org/1.4/search/freetext?app_id=374a8178&app_key=35178959e35c09eaeeed7ecbfc91aa10&q=water&_format=xml"
        //curl -v  -X GET "https://beta.openphacts.org/1.3/search/freetext?app_id=374a8178&app_key=35178959e35c09eaeeed7ecbfc91aa10&q=c0242350&_format=xml"
    }

    private enum format {

        json, tsv, rdf, rdfjson, html, ttl, xml;

        @Override
        public String toString() {
            String f = super.toString();
            return "&_format=" + f;
        }
    }

    private enum similarityType {

        Tanimoto(0), Tversky(1), Euclidian(2);
        String type;

        similarityType(int type) {

            this.type = "&searchOptions.SimilarityType=" + type;

        }
    }

    private enum metadata {

        execution, sites, formats, view, all;
    }

    @Override
    public String toString() {
        String f = super.toString();
        return "&_metadata=" + f;
    }

    private String threshold(double score) {
        String returning = "&searchOptions.Threshold=" + score;
        return returning;

    }
}
