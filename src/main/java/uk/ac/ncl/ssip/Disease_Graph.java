/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip;

import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.parsers.disease.DisGENetFLATFILE;
import uk.ac.ncl.ssip.parsers.disease.DiseaseOntologyOBO;
import uk.ac.ncl.ssip.parsers.drug.DrugBankXML;
import uk.ac.ncl.ssip.parsers.drug.NDFRT;
import uk.ac.ncl.ssip.parsers.disease.OrphanetOntOBO;
import uk.ac.ncl.ssip.parsers.disease.OrphanetRelations;
import uk.ac.ncl.ssip.parsers.drug.SIDERFlatfile;
import uk.ac.ncl.ssip.queryframework.DReNInMining;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Disease_Graph {

    String graphFile = "/Users/joemullen/Desktop/DiseaseOP/disease.gexf";
    Neo4J backend = new Neo4J("/Users/joemullen/Documents/neotest-disease-6-checked");

    public static void main(String[] args) {

        Disease_Graph d = new Disease_Graph();
        //d.CreateDiseaseGraph();
        d.testQuery();

    }

    public void CreateDiseaseGraph() {
//        //need to add:
//        1. drug-> gene relation
//        2. Side effects 
        
        
//      let's use mesh AS WELL of UMLS- seems to be less redundant? 
 //         disease ontology       MSH2006_2005_11_15
 //         orphanet ontology       MESH
 //         then we can map the sider side effects by calling openPHACTS and getting the MESH id from the MEDDRA accession
        //>>>>>>>>>>>>>>>>>> STEP_1 FILES
        String UNIPROTxmlDB = new String("/Users/joemullen/Desktop/Datasets/UniProtSwissProt/uniprot_sprot.xml");
////        >>>>>>>>>>>>>>>>>> STEP_1 START  Genes ONLY
//        backend.setCommitSize(10000);
//        backend.initialiseDatabaseConnection();
//        backend.createIndex();
//        backend.syncIndexes(10);
//        UniProtSwissProtXML uni = new UniProtSwissProtXML(UNIPROTxmlDB, backend, false, true);
//        uni.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
////        //>>>>>>>>>>>>>>>>>> STEP_2 FILES
//        String DBxmlDB = new String("/Users/joemullen/Desktop/Datasets/DrugBank/drugbank.xml");
////        //>>>>>>>>>>>>>>>>>> STEP_2 START
//        backend.initialiseDatabaseConnection();
//        backend.setCommitSize(10000);
//        backend.syncIndexes(10);
//        backend.createIndex("DBID");
//        DrugBankXML db = new DrugBankXML(DBxmlDB, backend, false);
//        db.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
        //>>>>>>>>>>>>>>>>>> STEP_3 FILES
//        String orphanetOnt = "/Users/joemullen/Desktop/Datasets/ORPHANET/orphanet_ordo.obo";
//        //>>>>>>>>>>>>>>>>>> STEP_3 START  Rare Diseases Ontology
//        backend.setCommitSize(10000);
//        backend.initialiseDatabaseConnection();
//        backend.syncIndexes(10);
//        for (int i = 1; i < 20; i++) {
//            backend.createIndex("UMLS_CUI"+i);
//        }
//        OrphanetOntOBO orph = new OrphanetOntOBO(orphanetOnt, backend);
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
//        //>>>>>>>>>>>>>>>>>> STEP_4 FILES
//        String diseaseOnt = "/Users/joemullen/Desktop/Datasets/DiseaseOntology/HumanDOr21.obo";
//        //>>>>>>>>>>>>>>>>>> STEP_4 START  Diseases Ontology 
//        backend.setCommitSize(10000);
//        backend.initialiseDatabaseConnection();
//        backend.syncIndexes(10);
//        DiseaseOntologyOBO diseaseInt = new DiseaseOntologyOBO(diseaseOnt, backend);
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
        //       >>>>>>>>>>>>>>>>>> STEP_5 FILES
//        String orphanetRels = "/Users/joemullen/Desktop/Datasets/ORPHANET/diseaseGeneInteraction.xml";
//        //>>>>>>>>>>>>>>>>>> STEP_5 START  Rare Diseases Interactions
//        backend.setCommitSize(10000);
//        backend.initialiseDatabaseConnection();
//        backend.syncIndexes(10);
//        OrphanetRelations orphRels = new OrphanetRelations(orphanetRels, backend);
//        orphRels.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
//        //>>>>>>>>>>>>>>>>>> STEP_6 FILES
//        String disgeNETRels = "/Users/joemullen/Desktop/Datasets/DiseGeNET/curated_gene_disease_associations.txt";
////        //>>>>>>>>>>>>>>>>>> STEP_6 DisGENET relations
//        backend.setCommitSize(10000);
//        backend.initialiseDatabaseConnection();
//        backend.syncIndexes(10);
//        DisGENetFLATFILE dis = new DisGENetFLATFILE(0.3, disgeNETRels, backend);
//        dis.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
//        //>>>>>>>>>>>>>>>>>> STEP_7 FILES
//        String pharmGKBMapping = "/Users/joemullen/Desktop/Datasets/PharmGKB/drugs/drugs.tsv";
//        //>>>>>>>>>>>>>>>>>> STEP_7 NDFRT
//        backend.setCommitSize(10000);
//        backend.initialiseDatabaseConnection();
//        backend.syncIndexes(10);
//        NDFRT ndfinds = new NDFRT(pharmGKBMapping, backend);
//        ndfinds.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
        //  >>>>>>>>>>>>>>>>>> STEP_8 FILES
//        String pharmGKBMapping = "/Users/joemullen/Desktop/Datasets/PharmGKB/drugs/drugs.tsv";
//        String siderlabs = "/Users/joemullen/Desktop/Datasets/SIDER/label_mapping.tsv";
//        String sidereff = "/Users/joemullen/Desktop/Datasets/SIDER/adverse_effects_raw.tsv";
//        String siderinds = "/Users/joemullen/Desktop/Datasets/SIDER/indications_raw.tsv";    
//        //>>>>>>>>>>>>>>>>>> STEP_8 SIDER (side_effects and indications)
//        backend.setCommitSize(10000);
//        backend.initialiseDatabaseConnection();
//        backend.syncIndexes(10);
//        SIDERFlatfile sf = new SIDERFlatfile(pharmGKBMapping, siderlabs, sidereff, siderinds, backend);
//        sf.run();
//        backend.commitTransaction();
//        backend.finaliseDatabaseConnection();
    }

    public void testQuery() {
//

        String graphFileDBDrugInts = "/Users/joemullen/Desktop/DiseaseOP/sildenafil_DBInts.gexf";
        String graphFileNDFRTInds = "/Users/joemullen/Desktop/DiseaseOP/sildenafil_NDFRT2.gexf";
        String graphFileDISEASES = "/Users/joemullen/Desktop/DiseaseOP/KIF7_RARE_DISEASES.gexf";
        String graphFileDISGENETDISEASES = "/Users/joemullen/Desktop/DiseaseOP/KIF7_DISGEN_DISEASES.gexf";
        String graphFileSIDERTREATS = "/Users/joemullen/Desktop/DiseaseOP/sildenadil_SIDER_INDS.gexf";
        String graphFileSIDERSE = "/Users/joemullen/Desktop/DiseaseOP/sildenadil_SIDER_SIDEEFECTS.gexf";

        //reinitialise connection 
        backend.initialiseDatabaseConnection();

        //resync the indexes
        backend.syncIndexes(10);


        System.out.println("export all code starts here...");
        GephiExporter gexfExportSubgraph = new GephiExporter();
        //ref node reference may be incorrect when reconnecting to database
        DReNInMining dm = new DReNInMining();
        StepDescription[] steps = null;
//        steps = dm.getDrugDrugInteractions();
//        gexfExportSubgraph.export(backend.traversal(new SSIPNode("Small_Molecule", "Sildenafil"), steps), graphFileDBDrugInts);


        steps = dm.getAllNDFRTInds();
        gexfExportSubgraph.export(backend.traversal(new SSIPNode("Small_Molecule", "Sildenafil"), steps), graphFileNDFRTInds);

//
//        steps = dm.getAllNeighbours(1);
//        gexfExportSubgraph.export(backend.findSubGraphMappings(new SSIPNode("UID", "Sildenafil"), steps), graphFileSIDERTREATS);
//
//        steps = dm.getAllSIDERSideEFFECTS();
//        gexfExportSubgraph.export(backend.traversal(new SSIPNode("Small_Molecule", "Sildenafil"), steps), graphFileSIDERSE);
//
//
//        steps = dm.getAllNeighbours(2);
//        gexfExportSubgraph.export(backend.findSubGraphMappings(new SSIPNode("Gene", "KIF7"), steps), graphFileDISEASES);

//        steps = dm.getDISGENETDiseases();
//        gexfExportSubgraph.export(backend.traversal(new SSIPNode("Gene", "KIF7"), steps), graphFileDISGENETDISEASES);



        //query database
        //gexfExportSubgraph.export(backend.returnAllNodesMap(), graphFile);

        //close database again 
        backend.finaliseDatabaseConnectionNoUPDATES();

    }
}
