/*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.ac.ncl.ssip;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.graphdb.schema.Schema;
//import org.neo4j.io.fs.FileUtils;

/*
 * Source: http://docs.neo4j.org/chunked/snapshot/tutorials-java-embedded-new-index.html
 */
public class NeoTast {

    private static final String DB_PATH = "F://neoTast-310";
    GraphDatabaseService graphDb;

    public static void main(final String[] args) throws IOException {
        NeoTast tast = new NeoTast();
        tast.initialiseDbConnection();
        tast.createIndex();
        tast.populateDb();
        tast.finialiseDb();
        
        tast.initialiseDbConnection();
        tast.syncIndex();
        tast.queryDb();
        tast.finialiseDb();

    }

    public void initialiseDbConnection() {

        System.out.println("Starting database ...");
//        FileUtils.deleteRecursively( new File( DB_PATH ) );

        // START SNIPPET: startDb
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
        // END SNIPPET: startDb

    }

    public void createIndex() {
        // START SNIPPET: createIndex
        IndexDefinition indexDefinition;
        try (Transaction tx = graphDb.beginTx()) {
            Schema schema = graphDb.schema();
            indexDefinition = schema.indexFor(DynamicLabel.label("User"))
                    .on("username")
                    .create();
            tx.success();
        }
        // END SNIPPET: createIndex

    }

    public void syncIndex() {
        // START SNIPPET: wait
        try (Transaction tx = graphDb.beginTx()) {
            Schema schema = graphDb.schema();
            schema.awaitIndexesOnline(10, TimeUnit.SECONDS);
//                schema.awaitIndexOnline( indexDefinition, 10, TimeUnit.SECONDS );
        }
        // END SNIPPET: wait
    }

    public void populateDb() {
        // START SNIPPET: addUsers
        try (Transaction tx = graphDb.beginTx()) {
            Label label = DynamicLabel.label("User");

            // Create some users
            for (int id = 0; id < 300; id++) {
                Node userNode = graphDb.createNode(label);
                userNode.setProperty("username", "user" + id + "@neo4j.org");
            }
            System.out.println("Users created");
            tx.success();
        }
        // END SNIPPET: addUsers
    }
    

    public void queryDb() {
        // START SNIPPET: findUsers
        Label label = DynamicLabel.label("User");
        int idToFind = 245;
        String nameToFind = "user" + idToFind + "@neo4j.org";
        try (Transaction tx = graphDb.beginTx()) {
            try (ResourceIterator<Node> users =
                            graphDb.findNodesByLabelAndProperty(label, "username", nameToFind).iterator()) {
                ArrayList<Node> userNodes = new ArrayList<>();
                while (users.hasNext()) {
                    userNodes.add(users.next());
                }

                for (Node node : userNodes) {
                    System.out.println("The username of user " + idToFind + " is " + node.getProperty("username"));
                }
            }
        }
        // END SNIPPET: findUsers
    }

    public void finialiseDb() {
        System.out.println("Shutting down database ...");
        // START SNIPPET: shutdownDb
        graphDb.shutdown();
        // END SNIPPET: shutdownDb
    }
}