/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Redundant;

import java.util.Scanner;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.SSIPNode;
import uk.ac.ncl.ssip.metadata.SSIPRelationType;

/**
 *
 * @author Matt2
 */
public class GoParser {

    public final String TERM_TAG_NAME = "term";
    public final String ID_TAG_NAME = "id";
    public final String NAME_TAG_NAME = "name";
    public final String DEF_TAG_NAME = "def";
    public final String DEFSTR_TAG_NAME = "defstr";
    public final String IS_ROOT_TAG_NAME = "is_root";
    public final String IS_OBSOLETE_TAG_NAME = "is_obsolete";
    public final String COMMENT_TAG_NAME = "comment";
    public final String ALT_ID_TAG_NAME = "alt_id";
    public final String NAMESPACE_TAG_NAME = "namespace";
    public final String ISA_TAG_NAME = "is_a";
    public final String RELATIONSHIP_TAG_NAME = "relationship";
    public final String RELATIONSHIP_TYPE_TAG_NAME = "type";
    public final String RELATIONSHIP_TO_TAG_NAME = "to";
    private String filePath;
    private GraphHandlerInterface graphHandler;
    private Scanner sc;

    public static void main(String[] args) {

        String filePath = "D://Program Files (x86)/Dropbox/old files/go_daily-termdb.obo-xml";//"C://Users/Matt2/Documents/Dropbox/old files/test_go_daily-termdb.obo-xml"; //http://archive.geneontology.org/
        String graphFile = "D://Program Files (x86)/Dropbox/SSIP/gephi_export_3.gexf";//"C://Users/Matt2/Documents/Dropbox/SSIP/gephi_export_test.gexf";//
        Neo4J backend = new Neo4J("D://test-go-2.9_test", "F://performance_test-2.txt");

        backend.initialiseDatabaseConnection();

        //create index
        backend.createIndex();
        for (int i = 1; i < 10; i++) {
            backend.createIndex("altID" + i);
        }
        backend.syncIndexes(10);

        GoParser goimporter = new GoParser(filePath, backend);
        goimporter.parseGO_XML();

        GephiExporter gexfExportSubgraph = new GephiExporter();
        gexfExportSubgraph.export(backend.returnAllNodesMap(), graphFile);

        backend.finaliseDatabaseConnection();

    }

    public GoParser(String filepath, GraphHandlerInterface graphHandler) {
        this.filePath = filepath;
        this.graphHandler = graphHandler;
    }

    public void parseGO_XML() {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler saxHandler = new DefaultHandler() {
                boolean bterm = false;
                boolean bid = false;
                boolean bname = false;
                boolean baltid = false;
                boolean bisa = false;
                SSIPNode node = null;
                int altIDint;
                boolean termswitch = false;

                @Override
                public void startElement(String uri, String localName, String qName,
                        Attributes attributes) throws SAXException {

//                    System.out.println("Start Element :" + qName);
                    if (qName.equalsIgnoreCase(TERM_TAG_NAME)) {
                        bterm = true;
                    }

                    if (qName.equalsIgnoreCase(ID_TAG_NAME)) {
                        bid = true;
                    }

                    if (qName.equalsIgnoreCase(NAME_TAG_NAME)) {
                        bname = true;
                    }

                    if (qName.equalsIgnoreCase(ALT_ID_TAG_NAME)) {
                        baltid = true;
                    }

                    if (qName.equalsIgnoreCase(ISA_TAG_NAME)) {
                        bisa = true;
                    }
                }

                public void endElement(String uri, String localName,
                        String qName) throws SAXException {
                    if (qName.equalsIgnoreCase(TERM_TAG_NAME)) {
                        graphHandler.addNode(node);
                        termswitch=false;
                    }

//                    System.out.println("End Element :" + qName);
                }

                @Override
                public void characters(char ch[], int start, int length) throws SAXException {
                    if (bterm) {
                        String s = new String(ch, start, length);
                        termswitch = true;
//                        System.out.println("Term Name : " + new String(ch, start, length));
                        bterm = false;
                    }
                    if (termswitch) {

                        if (bid) {
                            String s = new String(ch, start, length);
//                        System.out.println("ID Name : " + s);

                            bid = false;
                            node = new SSIPNode("GO", s);
                            altIDint = 1;
                        }

                        if (bname) {
                            if (node != null) {
                                String s = new String(ch, start, length);
                                System.out.println("Name : " + s);
                                System.out.println(node.getId());
                                node.addProperty(NAME_TAG_NAME, s);
                            }
                            bname = false;
                        }

                        if (baltid) {
                            String s = new String(ch, start, length);
//                        System.out.println("Alt ID Name : " + s);
                            baltid = false;
                            altIDint++;
                            node.addProperty("altID" + altIDint, s);
//                            System.out.println(s);
//                        Node altid = checkNode(TERM_TAG_NAME, s);
//                        if (id != null) {
//                            altid.createRelationshipTo(id, RelTypes.GO_ALT_ID);
//                        }
                        }

                        if (bisa) {
                            String s = new String(ch, start, length);
//                        System.out.println("Is a Name : " + s);
                            bisa = false;
                            node.addRelation(new SSIPNode("ID", s),
                                    new SSIPRelationType(SSIPRelationType.RelTypes.IS_A.toString(), 1));
//                            System.out.println(s);
//                        Node isa = checkNode(TERM_TAG_NAME, s);
//                        if (id != null) {
//                            isa.createRelationshipTo(id, RelTypes.GO_ALT_ID);
//                        }
                        }
                    }
                }
            };
            saxParser.parse(filePath, saxHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
