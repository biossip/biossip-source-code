/*
 * Copyright(c) 2014 Matthew Collison. 
 */
package Redundant;

import it.uniroma1.dis.wsngroup.gexf4j.core.Gexf;
import it.uniroma1.dis.wsngroup.gexf4j.core.Graph;
import it.uniroma1.dis.wsngroup.gexf4j.core.Node;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.GexfImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.StaxGraphWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class GephiExporterStatic {

    private static Gexf gexf = new GexfImpl();
    private static Graph graph = gexf.getGraph();
    //map for uid to java bean nodes 
    private static Map<String, MetaDataInterface> jbeanMap = new HashMap<String, MetaDataInterface>();
    //map for uid to geph nodes 
    private static Map<String, Node> gephiNodeMap = new HashMap<String, Node>();

    public static void export(Map<String, MetaDataInterface> jbeanMap, String filename) {

        GephiExporterStatic.jbeanMap = jbeanMap;
        itterateObjects();
        writeGexf(filename);

    }

    public static void exportMetadata(String filename) {
    }

    private static void itterateObjects() {
        //iterate through list to create nodes 
        for (String key : jbeanMap.keySet()) {
            System.out.println(key);
            Node n = graph.createNode(jbeanMap.get(key).getType() + jbeanMap.get(key).getId());
            n.setLabel(jbeanMap.get(key).getId());
            gephiNodeMap.put(jbeanMap.get(key).getType() + jbeanMap.get(key).getId(), n);
        }
        //iterate through to create relations 
        for (String key : jbeanMap.keySet()) {

            Set<MetaDataInterface> rels = jbeanMap.get(key).getRelations().keySet();
            for (MetaDataInterface rel : rels) {
                gephiNodeMap.get(jbeanMap.get(key).getType() + jbeanMap.get(key).getId())
                        .connectTo(gephiNodeMap.get(rel.getType() + rel.getId()));
            }

        }
    }

    private static void writeGexf(String filename) {
        StaxGraphWriter graphWriter = new StaxGraphWriter();
        File f = new File(filename);
        Writer out;
        try {
            out = new FileWriter(f, false);
            graphWriter.writeToStream(gexf, out, "UTF-8");
            System.out.println(f.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
