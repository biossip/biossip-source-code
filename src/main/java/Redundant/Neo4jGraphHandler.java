/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Redundant;

import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.graphdb.schema.Schema;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.impl.util.FileUtils;
import org.neo4j.tooling.GlobalGraphOperations;
import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;

/**
 *
 * @author Matt2
 */
public class Neo4jGraphHandler implements GraphHandlerInterface {

    //graph variables 
    private String DB_PATH = "D://neo4j-GH_app_3";
    private GraphDatabaseService graphDb;
    private Transaction tx;

    //transaction handling variables
    private int commitSize = 1;
    private int counter = 0;

    public Neo4jGraphHandler() {
    }

    public Neo4jGraphHandler(String DB_PATH) {
        this.DB_PATH = DB_PATH;
    }

    public void intialiseTransaction() {
        try {
            graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
            registerShutdownHook(graphDb);
            tx = graphDb.beginTx();
            System.out.println("Sucesfully connected to graph");
            System.out.println("Graph location: " + DB_PATH);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createIndex(String label, String property) {
        try (Transaction schemaTx = graphDb.beginTx()) {
            Schema schema = graphDb.schema();
            IndexDefinition indexDefinition = schema.indexFor(DynamicLabel.label(label))
                    .on(property)
                    .create();
            schemaTx.success();
        }
    }

    @Override
    public void addNode(MetaDataInterface nodeObject) {
        
        Node node = graphDb.createNode();
        node.setProperty(nodeObject.getType(), nodeObject.getId());
        for (MetaDataInterface toNodeObjects : nodeObject.getRelations().keySet()) {

        }
        commitTransaction();
    }

    @Override
    public void commitTransaction() {
        tx.success();
        tx = graphDb.beginTx();
        counter = 0;
    }

    public void finaliseTransaction() {
        commitTransaction();
        tx.close();
    }

    @Override
    public void clearDatabase(String dbname) {
        try {
            FileUtils.deleteRecursively(new File(DB_PATH));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getDatabaseName() {
        return this.DB_PATH;
    }

    private void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    @Override
    public Map<String, MetaDataInterface> returnAllNodesMap() {

        Map<String, MetaDataInterface> dataMap = new HashMap<String, MetaDataInterface>();
        for (IndexDefinition index : graphDb.schema().getIndexes()) {
            for (String keys : index.getPropertyKeys()) {
                System.out.println(keys);
            }
        }
        return dataMap;
    }

    @Override
    public Map<String, MetaDataInterface> traversal(MetaDataInterface startNode, StepDescription... steps) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, MetaDataInterface> traversal(StepDescription... steps) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void initialiseDatabaseConnection() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void finaliseDatabaseConnection() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
